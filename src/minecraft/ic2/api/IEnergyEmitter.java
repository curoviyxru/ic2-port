// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.api;

import net.minecraft.src.TileEntity;

// Referenced classes of package ic2.api:
//            IEnergyTile, Direction

public interface IEnergyEmitter
    extends IEnergyTile
{

    public abstract boolean emitsEnergyTo(TileEntity tileentity, Direction direction);
}
