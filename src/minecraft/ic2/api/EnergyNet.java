// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.api;

import java.lang.reflect.Method;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

// Referenced classes of package ic2.api:
//            IEnergySource

public final class EnergyNet
{

    public static EnergyNet getForWorld(World world)
    {
        try
        {
            return new EnergyNet(Class.forName("ic2.common.EnergyNet").getMethod("getForWorld", new Class[] {
                net.minecraft.src.World.class
            }).invoke(null, new Object[] {
                world
            }));
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    private EnergyNet(Object obj)
    {
        energyNetInstance = obj;
    }

    public void addTileEntity(TileEntity tileentity)
    {
        try
        {
            Class.forName("ic2.common.EnergyNet").getMethod("addTileEntity", new Class[] {
                net.minecraft.src.TileEntity.class
            }).invoke(energyNetInstance, new Object[] {
                tileentity
            });
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public void removeTileEntity(TileEntity tileentity)
    {
        try
        {
            Class.forName("ic2.common.EnergyNet").getMethod("removeTileEntity", new Class[] {
                net.minecraft.src.TileEntity.class
            }).invoke(energyNetInstance, new Object[] {
                tileentity
            });
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public int emitEnergyFrom(IEnergySource ienergysource, int i)
    {
        try
        {
            return ((Integer)Class.forName("ic2.common.EnergyNet").getMethod("emitEnergyFrom", new Class[] {
                ic2.api.IEnergySource.class, Integer.TYPE
            }).invoke(energyNetInstance, new Object[] {
                ienergysource, Integer.valueOf(i)
            })).intValue();
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public long getTotalEnergyConducted(TileEntity tileentity)
    {
        try
        {
            return ((Long)Class.forName("ic2.common.EnergyNet").getMethod("getTotalEnergyConducted", new Class[] {
                net.minecraft.src.TileEntity.class
            }).invoke(energyNetInstance, new Object[] {
                tileentity
            })).longValue();
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    Object energyNetInstance;
}
