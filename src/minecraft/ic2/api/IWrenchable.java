// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.api;

import net.minecraft.src.EntityPlayer;

public interface IWrenchable
{

    public abstract boolean wrenchSetFacing(EntityPlayer entityplayer, int i);

    public abstract void setFacing(short word0);

    public abstract boolean wrenchRemove(EntityPlayer entityplayer);

    public abstract float getWrenchDropRate();
}
