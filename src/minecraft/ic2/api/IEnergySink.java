// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.api;


// Referenced classes of package ic2.api:
//            IEnergyAcceptor, Direction

public interface IEnergySink
    extends IEnergyAcceptor
{

    public abstract boolean demandsEnergy();

    public abstract int injectEnergy(Direction direction, int i);
}
