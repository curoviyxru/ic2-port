// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerBaseGenerator;
import ic2.common.TileEntityGeoGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiGeoGenerator extends GuiContainer
{

    public GuiGeoGenerator(InventoryPlayer inventoryplayer, TileEntityGeoGenerator tileentitygeogenerator)
    {
        super(new ContainerBaseGenerator(inventoryplayer, tileentitygeogenerator));
        tileentity = tileentitygeogenerator;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Geothermal Generator", 52, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIGeoGenerator.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.fuel > 0)
        {
            int l = tileentity.gaugeFuelScaled(14);
            drawTexturedModalRect(j + 66, (k + 36 + 14) - l, 176, 14 - l, 14, l);
        }
        if(tileentity.storage > 0)
        {
            int i1 = tileentity.gaugeStorageScaled(22);
            drawTexturedModalRect(j + 89, k + 39, 177, 19, i1, 6);
        }
    }

    public TileEntityGeoGenerator tileentity;
}
