// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerElectrolyzer;
import ic2.common.TileEntityElectrolyzer;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiElectrolyzer extends GuiContainer
{

    public GuiElectrolyzer(InventoryPlayer inventoryplayer, TileEntityElectrolyzer tileentityelectrolyzer)
    {
        super(new ContainerElectrolyzer(inventoryplayer, tileentityelectrolyzer));
        tileentity = tileentityelectrolyzer;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Electrolyzer", 56, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIElectrolyzer.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.energy > 0)
        {
            int l = tileentity.gaugeEnergyScaled(24);
            drawTexturedModalRect(j + 79, k + 34, 176, 14, l + 1, 16);
        }
    }

    public TileEntityElectrolyzer tileentity;
}
