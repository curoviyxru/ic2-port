// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerElectricBlock;
import ic2.common.TileEntityElectricBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiElectricBlock extends GuiContainer
{

    public GuiElectricBlock(InventoryPlayer inventoryplayer, TileEntityElectricBlock tileentityelectricblock)
    {
        super(new ContainerElectricBlock(inventoryplayer, tileentityelectricblock));
        tileentity = tileentityelectricblock;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString(tileentity.getNameByTier(), 62, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
        fontRenderer.drawString("Power Level:", 79, 25, 0x404040);
        int i = tileentity.energy;
        if(i > tileentity.maxStorage)
        {
            i = tileentity.maxStorage;
        }
        fontRenderer.drawString((new StringBuilder()).append(" ").append(i).toString(), 110, 35, 0x404040);
        fontRenderer.drawString((new StringBuilder()).append("/").append(tileentity.maxStorage).toString(), 110, 45, 0x404040);
        fontRenderer.drawString((new StringBuilder()).append("Out: ").append(tileentity.output).append(" EU/t").toString(), 85, 60, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIElectricBlock.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.energy > 0)
        {
            int l = tileentity.gaugeEnergyScaled(24);
            drawTexturedModalRect(j + 79, k + 34, 176, 14, l + 1, 16);
        }
    }

    public TileEntityElectricBlock tileentity;
}
