// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.EntityIC2Explosive;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class RenderExplosiveBlock extends Render
{

    public RenderExplosiveBlock(String s)
    {
        blockRenderer = new RenderBlocks();
        shadowSize = 0.5F;
        texturePath = s;
    }

    public void func_153_a(EntityIC2Explosive entityic2explosive, double d, double d1, double d2, 
            float f, float f1)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef((float)d, (float)d1, (float)d2);
        if(((float)entityic2explosive.fuse - f1) + 1.0F < 10F)
        {
            float f2 = 1.0F - (((float)entityic2explosive.fuse - f1) + 1.0F) / 10F;
            if(f2 < 0.0F)
            {
                f2 = 0.0F;
            }
            if(f2 > 1.0F)
            {
                f2 = 1.0F;
            }
            f2 *= f2;
            f2 *= f2;
            float f4 = 1.0F + f2 * 0.3F;
            GL11.glScalef(f4, f4, f4);
        }
        float f3 = (1.0F - (((float)entityic2explosive.fuse - f1) + 1.0F) / 100F) * 0.8F;
        loadTexture(texturePath);
        blockRenderer.renderBlockOnInventory(entityic2explosive.renderBlock, 0, entityic2explosive.getEntityBrightness(f1));
        if((entityic2explosive.fuse / 5) % 2 == 0)
        {
            GL11.glDisable(3553 /*GL_TEXTURE_2D*/);
            GL11.glDisable(2896 /*GL_LIGHTING*/);
            GL11.glEnable(3042 /*GL_BLEND*/);
            GL11.glBlendFunc(770, 772);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, f3);
            blockRenderer.renderBlockOnInventory(entityic2explosive.renderBlock, 0, 1.0F);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glDisable(3042 /*GL_BLEND*/);
            GL11.glEnable(2896 /*GL_LIGHTING*/);
            GL11.glEnable(3553 /*GL_TEXTURE_2D*/);
        }
        GL11.glPopMatrix();
    }

    public void doRender(Entity entity, double d, double d1, double d2, 
            float f, float f1)
    {
        func_153_a((EntityIC2Explosive)entity, d, d1, d2, f, f1);
    }

    public RenderBlocks blockRenderer;
    public String texturePath;
}
