// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerMatter;
import ic2.common.TileEntityMatter;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiMatter extends GuiContainer
{

    public GuiMatter(InventoryPlayer inventoryplayer, TileEntityMatter tileentitymatter)
    {
        super(new ContainerMatter(inventoryplayer, tileentitymatter));
        tileentity = tileentitymatter;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Mass Fabricator", 54, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
        fontRenderer.drawString("Progress:", 16, 20, 0x404040);
        fontRenderer.drawString(tileentity.getProgressAsString(), 16, 28, 0x404040);
        if(tileentity.scrap > 0)
        {
            fontRenderer.drawString("Amplifier:", 16, 44, 0x404040);
            fontRenderer.drawString((new StringBuilder()).append("").append(tileentity.scrap).toString(), 16, 56, 0x404040);
        }
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIMatter.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
    }

    public TileEntityMatter tileentity;
}
