// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerSolarGenerator;
import ic2.common.TileEntitySolarGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiSolarGenerator extends GuiContainer
{

    public GuiSolarGenerator(InventoryPlayer inventoryplayer, TileEntitySolarGenerator tileentitysolargenerator)
    {
        super(new ContainerSolarGenerator(inventoryplayer, tileentitysolargenerator));
        tileentity = tileentitysolargenerator;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Solar Panel", 55, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUISolarGenerator.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.sunIsVisible)
        {
            drawTexturedModalRect(j + 80, k + 45, 176, 0, 14, 14);
        }
    }

    public TileEntitySolarGenerator tileentity;
}
