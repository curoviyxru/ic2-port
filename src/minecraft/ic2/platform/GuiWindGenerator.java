// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerWindGenerator;
import ic2.common.TileEntityWindGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiWindGenerator extends GuiContainer
{

    public GuiWindGenerator(InventoryPlayer inventoryplayer, TileEntityWindGenerator tileentitywindgenerator)
    {
        super(new ContainerWindGenerator(inventoryplayer, tileentitywindgenerator));
        tileentity = tileentitywindgenerator;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Wind Mill", 70, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIWindGenerator.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        int l = tileentity.gaugeFuelScaled(14);
        drawTexturedModalRect(j + 80, (k + 45 + 14) - l, 176, 14 - l, 14, l);
        int i1 = tileentity.getOverheatScaled(14);
        if(i1 > 0)
        {
            drawTexturedModalRect(j + 80, (k + 45 + 14) - i1, 176, 28 - i1, 14, i1);
        }
    }

    public TileEntityWindGenerator tileentity;
}
