// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.TileEntityCable;
import net.minecraft.src.*;

public class RenderBlockCable
{

    public RenderBlockCable()
    {
    }

    public static boolean render(RenderBlocks renderblocks, IBlockAccess iblockaccess, int i, int j, int k, Block block, int l)
    {
        TileEntity tileentity = iblockaccess.getBlockTileEntity(i, j, k);
        if(!(tileentity instanceof TileEntityCable))
        {
            return true;
        }
        TileEntityCable tileentitycable = (TileEntityCable)tileentity;
        float f = tileentitycable.getCableThickness();
        float f1 = (1.0F - f) / 2.0F;
        block.setBlockBounds(f1, f1, f1, f1 + f, f1 + f, f1 + f);
        renderblocks.renderStandardBlock(block, i, j, k);
        boolean flag = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i + 1, j, k));
        boolean flag1 = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i - 1, j, k));
        boolean flag2 = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i, j + 1, k));
        boolean flag3 = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i, j - 1, k));
        boolean flag4 = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i, j, k + 1));
        boolean flag5 = tileentitycable.canInteractWith(iblockaccess.getBlockTileEntity(i, j, k - 1));
        if(flag)
        {
            block.setBlockBounds(f1 + f, f1, f1, 1.0F, f1 + f, f1 + f);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        if(flag2)
        {
            block.setBlockBounds(f1, f1 + f, f1, f1 + f, 1.0F, f1 + f);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        if(flag4)
        {
            block.setBlockBounds(f1, f1, f1 + f, f1 + f, f1 + f, 1.0F);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        if(flag1)
        {
            block.setBlockBounds(0.0F, f1, f1, f1, f1 + f, f1 + f);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        if(flag3)
        {
            block.setBlockBounds(f1, 0.0F, f1, f1 + f, f1, f1 + f);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        if(flag5)
        {
            block.setBlockBounds(f1, f1, 0.0F, f1 + f, f1 + f, f1);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        return true;
    }
}
