// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerNuclearReactor;
import ic2.common.TileEntityNuclearReactor;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiNuclearReactor extends GuiContainer
{

    public GuiNuclearReactor(InventoryPlayer inventoryplayer, TileEntityNuclearReactor tileentitynuclearreactor, short word0)
    {
        super(new ContainerNuclearReactor(inventoryplayer, tileentitynuclearreactor, word0));
        tileentity = tileentitynuclearreactor;
        size = word0;
        char c = '\336';
        int i = c - 108;
        ySize = i + 108;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Nuclear Reactor", 55, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture((new StringBuilder()).append("/ic2/sprites/GUI6by").append(size).append(".png").toString());
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
    }

    public TileEntityNuclearReactor tileentity;
    public int size;
}
