// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerMiner;
import ic2.common.TileEntityMiner;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiMiner extends GuiContainer
{

    public GuiMiner(InventoryPlayer inventoryplayer, TileEntityMiner tileentityminer)
    {
        super(new ContainerMiner(inventoryplayer, tileentityminer));
        tileentity = tileentityminer;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Miner", 62, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIMiner.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.energy > 0)
        {
            int l = tileentity.gaugeEnergyScaled(14);
            drawTexturedModalRect(j + 81, (k + 41 + 14) - l, 176, 14 - l, 14, l);
        }
    }

    public TileEntityMiner tileentity;
}
