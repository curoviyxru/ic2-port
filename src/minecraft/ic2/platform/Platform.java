// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.IHasGuiContainer;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.input.Keyboard;

// Referenced classes of package ic2.platform:
//            Ic2

public class Platform
{

    public Platform()
    {
    }

    public static int AddAllFuel(int i, int j)
    {
        return ModLoader.AddAllFuel(i);
    }

    public static File getMinecraftDir()
    {
        return Minecraft.getMinecraftDir();
    }

    public static Chunk getOrLoadChunk(World world, int i, int j)
    {
        return world.getIChunkProvider().provideChunk(i, j);
    }

    public static EntityPlayer getPlayerInstance()
    {
        return ModLoader.getMinecraftInstance().thePlayer;
    }

    public static boolean isRendering()
    {
        return true;
    }

    public static boolean isSimulating()
    {
        return !ModLoader.getMinecraftInstance().theWorld.multiplayerWorld;
    }

    public static boolean launchGUI(EntityPlayer entityplayer, TileEntity tileentity, Integer integer)
    {
        if(integer.intValue() == 0 && (tileentity instanceof IInventory))
        {
            entityplayer.displayGUIChest((IInventory)tileentity);
            return true;
        }
        if((tileentity instanceof IInventory) && (tileentity instanceof IHasGuiContainer))
        {
            ModLoader.OpenGUI(entityplayer, Ic2.getGuiForId(entityplayer, integer.intValue(), tileentity));
            return true;
        } else
        {
            return false;
        }
    }

    public static void log(Level level, String s)
    {
        ModLoader.getLogger().log(level, s);
    }

    public static boolean isBlockOpaqueCube(IBlockAccess iblockaccess, int i, int j, int k)
    {
        return iblockaccess.isBlockOpaqueCube(i, j, k);
    }

    public static void playSoundSp(String s, float f, float f1)
    {
        ModLoader.getMinecraftInstance().theWorld.playSoundAtEntity(ModLoader.getMinecraftInstance().thePlayer, s, f, f1);
    }

    public static void resetPlayerInAirTime(EntityPlayer entityplayer)
    {
    }

    public static void messagePlayer(EntityPlayer entityplayer, String s)
    {
        ModLoader.getMinecraftInstance().ingameGUI.addChatMessage(s);
    }

    public static boolean isKeyDownLaserMode(EntityPlayer entityplayer)
    {
        return Keyboard.isKeyDown(Ic2.keyLaserMode.keyCode);
    }

    public static boolean isKeyDownJetpackHover(EntityPlayer entityplayer)
    {
        return Keyboard.isKeyDown(Ic2.keyJetpackHover.keyCode);
    }

    public static boolean isKeyDownSuitActivate(EntityPlayer entityplayer)
    {
        return Keyboard.isKeyDown(Ic2.keySuitActivate.keyCode);
    }

    public static boolean isKeyDownForward(EntityPlayer entityplayer)
    {
        return Keyboard.isKeyDown(ModLoader.getMinecraftInstance().gameSettings.keyBindForward.keyCode);
    }

    public static String getItemNameIS(ItemStack itemstack)
    {
        return itemstack.getItem().getItemNameIS(itemstack);
    }

    public static void teleportTo(Entity entity, double d, double d1, double d2, float f, 
            float f1)
    {
        entity.setPositionAndRotation(d, d1, d2, f, f1);
    }

    public static String translateBlockName(Block block)
    {
        return block.translateBlockName();
    }

    public static double worldUntranslatedFunction2(World world, Vec3D vec3d, AxisAlignedBB axisalignedbb)
    {
        return (double)world.func_675_a(vec3d, axisalignedbb);
    }

    public static MovingObjectPosition axisalignedbbUntranslatedFunction1(AxisAlignedBB axisalignedbb, Vec3D vec3d, Vec3D vec3d1)
    {
        return axisalignedbb.func_1169_a(vec3d, vec3d1);
    }

    public static List getContainerSlots(Container container)
    {
        return container.slots;
    }

    public static boolean isPlayerOp(EntityPlayer entityplayer)
    {
        return true;
    }

    //public static boolean isPlayerSprinting(EntityPlayer entityplayer)
    //{
    //    return entityplayer.is();
    //}

    public static boolean givePlayerOneFood(EntityPlayer entityplayer)
    {
        /* if(entityplayer.func_35191_at().func_35765_a() < 18)
        {
            entityplayer.func_35191_at().func_35771_a(1, 0.9F);
            return true;
        } else
        {
            return false;
        } */

        if (entityplayer.health <= 18) {
            entityplayer.heal(2);
            return true;
        }
        else return false;
    }

    public static void removePoisonFrom(EntityPlayer entityplayer)
    {
        //entityplayer.damageEntity(Potion.field_35689_u.field_35670_H);
    }
}
