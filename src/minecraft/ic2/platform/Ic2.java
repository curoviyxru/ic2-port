// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import forge.*;
import ic2.common.*;
import java.io.File;
import java.io.PrintStream;
import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

// Referenced classes of package ic2.platform:
//            Platform, NetworkManager, RenderExplosiveBlock, RenderFlyingItem, 
//            RenderCrossed, RenderBlockFence, RenderBlockMiningPipe, RenderBlockLuminator, 
//            RenderBlockCable, GuiIronFurnace, GuiElecFurnace, GuiMacerator, 
//            GuiExtractor, GuiCompressor, GuiCanner, GuiMiner, 
//            GuiPump, GuiElectrolyzer, GuiRecycler, GuiInduction, 
//            GuiMatter, GuiElectricBlock, GuiGeoGenerator, GuiWaterGenerator, 
//            GuiSolarGenerator, GuiWindGenerator, GuiGenerator, GuiTradeOMatOpen, 
//            GuiTradeOMatClosed, GuiNuclearReactor

public abstract class Ic2 extends BaseModMp
{

    public Ic2()
    {
        try
        {
            lang = new Configuration(new File(Platform.getMinecraftDir(), "/config/IC2.lang"));
            lang.load();
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("[IndustrialCraft] Error while trying to access language file!").toString());
            lang = null;
        }
        mod_IC2.initialize();
        ModLoader.SetInGameHook(this, true, true);
        addLocalization("blockMachine.name", "Machine Block");
        addLocalization("blockIronFurnace.name", "Iron Furnace");
        addLocalization("blockElecFurnace.name", "Electric Furnace");
        addLocalization("blockMacerator.name", "Macerator");
        addLocalization("blockExtractor.name", "Extractor");
        addLocalization("blockCompressor.name", "Compressor");
        addLocalization("blockCanner.name", "Canning Machine");
        addLocalization("blockMiner.name", "Miner");
        addLocalization("blockPump.name", "Pump");
        addLocalization("blockMagnetizer.name", "Magnetizer");
        addLocalization("blockElectrolyzer.name", "Electrolyzer");
        addLocalization("blockRecycler.name", "Recycler");
        addLocalization("blockAdvMachine.name", "Advanced Machine Block");
        addLocalization("blockInduction.name", "Induction Furnace");
        addLocalization("blockMatter.name", "Mass Fabricator");
        addLocalization("blockTerra.name", "Terraformer");
        addLocalization("tile.blockOreCopper.name", "Copper Ore");
        addLocalization("tile.blockOreTin.name", "Tin Ore");
        addLocalization("tile.blockOreUran.name", "Uranium Ore");
        addLocalization("blockGenerator.name", "Generator");
        addLocalization("blockGeoGenerator.name", "Geothermal Generator");
        addLocalization("blockWaterGenerator.name", "Water Mill");
        addLocalization("blockSolarGenerator.name", "Solar Panel");
        addLocalization("blockWindGenerator.name", "Wind Mill");
        addLocalization("blockNuclearReactor.name", "Nuclear Reactor");
        addLocalization("tile.blockMiningPipe.name", "Mining Pipe");
        addLocalization("tile.blockMiningTip.name", "Mining Pipe");
        addLocalization("tile.blockRubWood.name", "Rubber Wood");
        addLocalization("tile.blockRubSapling.name", "Rubber Tree Sapling");
        addLocalization("tile.blockITNT.name", "Industrial TNT");
        addLocalization("tile.blockNuke.name", "Nuke");
        addLocalization("tile.blockRubber.name", "Rubber Sheet");
        addLocalization("tile.blockReactorChamber.name", "Reactor Chamber");
        addLocalization("tile.blockFenceIron.name", "Iron Fence");
        addLocalization("tile.blockAlloy.name", "Reinforced Stone");
        addLocalization("tile.blockAlloyGlass.name", "Reinforced Glass");
        addLocalization("blockBatBox.name", "BatBox");
        addLocalization("blockMFE.name", "MFE");
        addLocalization("blockMFSU.name", "MFSU");
        addLocalization("blockTransformerLV.name", "LV-Transformer");
        addLocalization("blockTransformerMV.name", "MV-Transformer");
        addLocalization("blockTransformerHV.name", "HV-Transformer");
        addLocalization("tile.blockLuminator.name", "Luminator");
        addLocalization("blockPersonalChest.name", "Personal Safe");
        addLocalization("blockPersonalTrader.name", "Trade-O-Mat");
        addLocalization("blockMetalCopper.name", "Copper Block");
        addLocalization("blockMetalTin.name", "Tin Block");
        addLocalization("blockMetalBronze.name", "Bronze Block");
        addLocalization("blockMetalUranium.name", "Uranium Block");
        addLocalization("blockTeleporter.name", "Teleporter");
        addLocalization("blockTesla.name", "Tesla Coil");
        addLocalization("tile.blockFoam.name", "Construction Foam");
        addLocalization("tile.blockScaffold.name", "Scaffold");
        addLocalization("tile.blockLuminatorD.name", "Luminator");
        addLocalization("item.itemDustCoal.name", "Coal Dust");
        addLocalization("item.itemDustIron.name", "Iron Dust");
        addLocalization("item.itemDustGold.name", "Gold Dust");
        addLocalization("item.itemDustCopper.name", "Copper Dust");
        addLocalization("item.itemDustTin.name", "Tin Dust");
        addLocalization("item.itemDustBronze.name", "Bronze Dust");
        addLocalization("item.itemDustIronSmall.name", "Small Pile of Iron Dust");
        addLocalization("item.itemIngotAdvIron.name", "Refined Iron");
        addLocalization("item.itemIngotCopper.name", "Copper");
        addLocalization("item.itemIngotTin.name", "Tin");
        addLocalization("item.itemIngotBronze.name", "Bronze");
        addLocalization("item.itemIngotAlloy.name", "Mixed Metal Ingot");
        addLocalization("item.itemIngotUran.name", "Refined Uranium");
        addLocalization("item.itemOreUran.name", "Uranium Ore");
        addLocalization("item.itemBatRE.name", "RE-Battery");
        addLocalization("item.itemBatSU.name", "Single-Use Battery");
        addLocalization("item.itemBatCrystal.name", "Energy Crystal");
        addLocalization("item.itemBatLamaCrystal.name", "Lapotron Crystal");
        addLocalization("item.itemCellEmpty.name", "Empty Cell");
        addLocalization("item.itemCellLava.name", "Lava Cell");
        addLocalization("item.itemToolDrill.name", "Mining Drill");
        addLocalization("item.itemToolDDrill.name", "Diamond Drill");
        addLocalization("item.itemToolChainsaw.name", "Chainsaw");
        addLocalization("item.itemFuelCan.name", "Filled Fuel Can");
        addLocalization("item.itemFuelCanEmpty.name", "(Empty) Fuel Can");
        addLocalization("item.itemCellCoal.name", "H. Coal Cell");
        addLocalization("item.itemCellCoalRef.name", "Coalfuel Cell");
        addLocalization("item.itemCellBio.name", "Bio Cell");
        addLocalization("item.itemCellBioRef.name", "Biofuel Cell");
        addLocalization("item.itemFuelCoalDust.name", "Hydrated Coal Dust");
        addLocalization("item.itemFuelCoalCmpr.name", "H. Coal");
        addLocalization("item.itemFuelPlantBall.name", "Plantball");
        addLocalization("item.itemFuelPlantCmpr.name", "Compressed Plants");
        addLocalization("item.itemTinCan.name", "Tin Can");
        addLocalization("item.itemTinCanFilled.name", "(Filled) Tin Can");
        addLocalization("item.itemScanner.name", "OD Scanner");
        addLocalization("item.itemScannerAdv.name", "OV Scanner");
        addLocalization("item.itemCellWater.name", "Water Cell");
        addLocalization("item.itemHarz.name", "Sticky Resin");
        addLocalization("item.itemRubber.name", "Rubber");
        addLocalization("item.itemDynamite.name", "Dynamite");
        addLocalization("item.itemDynamiteSticky.name", "Sticky Dynamite");
        addLocalization("item.itemRemote.name", "Dynamite-O-Mote");
        addLocalization("item.itemTreetap.name", "Treetap");
        addLocalization("item.itemArmorRubBoots.name", "Rubber Boots");
        addLocalization("item.itemArmorJetpack.name", "Jetpack");
        addLocalization("item.itemArmorJetpackElectric.name", "Electric Jetpack");
        addLocalization("item.itemToolMiningLaser.name", "Mining Laser");
        addLocalization("item.itemCellUran.name", "Uranium Cell");
        addLocalization("item.itemCellCoolant.name", "Coolant Cell");
        addLocalization("item.itemReactorPlating.name", "Integrated Reactor Plating");
        addLocalization("item.itemReactorCooler.name", "Integrated Heat Disperser");
        addLocalization("item.itemCellUranDepleted.name", "Depleted Isotope Cell");
        addLocalization("item.itemCellUranEnriched.name", "Re-Enriched Uranium Cell");
        addLocalization("item.itemCellUranEmpty.name", "Near-depleted Uranium Cell");
        addLocalization("item.itemToolBronzePickaxe.name", "Bronze Pickaxe");
        addLocalization("item.itemToolBronzeAxe.name", "Bronze Axe");
        addLocalization("item.itemToolBronzeSword.name", "Bronze Sword");
        addLocalization("item.itemToolBronzeSpade.name", "Bronze Shovel");
        addLocalization("item.itemToolBronzeHoe.name", "Bronze Hoe");
        addLocalization("item.itemArmorBronzeHelmet.name", "Bronze Helmet");
        addLocalization("item.itemArmorBronzeChestplate.name", "Bronze Chestplate");
        addLocalization("item.itemArmorBronzeLegs.name", "Bronze Legs");
        addLocalization("item.itemArmorBronzeBoots.name", "Bronze Boots");
        addLocalization("item.itemPartCircuit.name", "Electronic Circuit");
        addLocalization("item.itemPartCircuitAdv.name", "Advanced Circuit");
        addLocalization("item.itemPartAlloy.name", "Advanced Alloy");
        addLocalization("item.itemScrap.name", "Scrap");
        addLocalization("item.itemMatter.name", "UU-Matter");
        addLocalization("item.itemCoin.name", "Industrial Credit");
        addLocalization("item.itemDoorAlloy.name", "Reinforced Door");
        addLocalization("itemCable.name", "Copper Cable");
        addLocalization("itemCableO.name", "Uninsulated Copper Cable");
        addLocalization("itemGoldCable.name", "Gold Cable");
        addLocalization("itemGoldCableI.name", "Insulated Gold Cable");
        addLocalization("itemGoldCableII.name", "2xIns. Gold Cable");
        addLocalization("itemIronCable.name", "HV Cable");
        addLocalization("itemIronCableI.name", "Insulated HV Cable");
        addLocalization("itemIronCableII.name", "2xIns. HV Cable");
        addLocalization("itemIronCableIIII.name", "4xIns. HV Cable");
        addLocalization("itemGlassCable.name", "Glass Fibre Cable");
        addLocalization("itemTinCable.name", "Ultra-Low-Current Cable");
        addLocalization("itemDetectorCable.name", "EU-Detector Cable");
        addLocalization("itemSplitterCable.name", "EU-Splitter Cable");
        addLocalization("item.itemToolWrench.name", "Wrench");
        addLocalization("item.itemToolMeter.name", "EU-Reader");
        addLocalization("item.itemCellWaterElectro.name", "Electrolyzed Water Cell");
        addLocalization("item.itemArmorBatpack.name", "BatPack");
        addLocalization("item.itemArmorAlloyChestplate.name", "Composite Vest");
        addLocalization("item.itemArmorNanoHelmet.name", "NanoSuit Helmet");
        addLocalization("item.itemArmorNanoChestplate.name", "NanoSuit Bodyarmor");
        addLocalization("item.itemArmorNanoLegs.name", "NanoSuit Leggings");
        addLocalization("item.itemArmorNanoBoots.name", "NanoSuit Boots");
        addLocalization("item.itemArmorQuantumHelmet.name", "QuantumSuit Helmet");
        addLocalization("item.itemArmorQuantumChestplate.name", "QuantumSuit Bodyarmor");
        addLocalization("item.itemArmorQuantumLegs.name", "QuantumSuit Leggings");
        addLocalization("item.itemArmorQuantumBoots.name", "QuantumSuit Boots");
        addLocalization("item.itemToolPainter.name", "Painter");
        addLocalization("item.itemToolCutter.name", "Insulation Cutter");
        addLocalization("item.itemPartCarbonFibre.name", "Raw Carbon Fibre");
        addLocalization("item.itemPartCarbonMesh.name", "Raw Carbon Mesh");
        addLocalization("item.itemPartCarbonPlate.name", "Carbon Plate");
        addLocalization("item.itemNanoSaber.name", "Nano Saber");
        addLocalization("item.itemPartIridium.name", "Iridium Plate");
        addLocalization("item.itemTFBP.name", "TFBP - Empty");
        addLocalization("item.itemTFBPCultivation.name", "TFBP - Cultivation");
        addLocalization("item.itemTFBPIrrigation.name", "TFBP - Irrigation");
        addLocalization("item.itemTFBPDesertification.name", "TFBP - Desertification");
        addLocalization("item.itemTFBPChilling.name", "TFBP - Chilling");
        addLocalization("item.itemTFBPFlatification.name", "TFBP - Flatification");
        addLocalization("item.itemToolWrenchElectric.name", "Electric Wrench");
        addLocalization("item.itemScrapbox.name", "Scrap Box");
        addLocalization("item.itemPartCoalBall.name", "Coal Ball");
        addLocalization("item.itemPartCoalBlock.name", "Compressed Coal Ball");
        addLocalization("item.itemPartCoalChunk.name", "Coal Chunk");
        addLocalization("item.itemPartIndustrialDiamond.name", "Industrial Diamond");
        addLocalization("item.itemFreq.name", "Frequency Transmitter");
        addLocalization("item.itemDustClay.name", "Clay Dust");
        addLocalization("item.itemPartPellet.name", "CF Pellet");
        addLocalization("item.itemFoamSprayer.name", "CF Sprayer");
        addLocalization("item.itemDustSilver.name", "Silver Dust");
        addLocalization("item.itemArmorCFPack.name", "CF Backpack");
        addLocalization("item.itemOreIridium.name", "Iridium Ore");
        addLocalization("item.itemArmorLappack.name", "Lappack");
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdIronFurnace);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdElecFurnace);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdMacerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdExtractor);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdCompressor);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdCanner);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdMiner);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdPump);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdElectrolyzer);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdRecycler);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdInduction);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdMatter);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdElectricBatBox);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdElectricMFE);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdElectricMFSU);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdGeoGenerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdWaterGenerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdSolarGenerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdWindGenerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdGenerator);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdTradeOMatOpen);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdTradeOMatClosed);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x3);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x4);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x5);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x6);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x7);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x8);
        ModLoaderMp.RegisterGUI(this, mod_IC2.guiIdNuclearReactor6x9);
        ModLoader.RegisterKey(this, keySuitActivate, true);
        ModLoader.RegisterKey(this, keyJetpackHover, true);
        ModLoader.RegisterKey(this, keyLaserMode, true);
        mod_IC2.cableRenderId = ModLoader.getUniqueBlockModelID(this, false);
        mod_IC2.miningPipeRenderId = ModLoader.getUniqueBlockModelID(this, true);
        mod_IC2.fenceRenderId = ModLoader.getUniqueBlockModelID(this, true);
        mod_IC2.luminatorRenderId = ModLoader.getUniqueBlockModelID(this, false);
        ModLoaderMp.RegisterNetClientHandlerEntity(ic2.common.EntityMiningLaser.class, true, 141);
        ModLoaderMp.RegisterNetClientHandlerEntity(ic2.common.EntityDynamite.class, true, 142);
        if(lang != null)
        {
            lang.save();
        }
    }

    public void addLocalization(String s, String s1)
    {
        String s2 = s1;
        if(lang != null)
        {
            s2 = lang.getOrCreateProperty(s, 0, s1).value;
        }
        ModLoader.AddLocalization(s, s2);
    }

    public int AddFuel(int i, int j)
    {
        return mod_IC2.AddFuelCommon(i, j);
    }

    public void HandlePacket(Packet230ModLoader packet230modloader)
    {
        NetworkManager.handlePacket(packet230modloader);
    }

    public void AddRenderer(Map map)
    {
        map.put(ic2.common.EntityIC2Explosive.class, new RenderExplosiveBlock("/ic2/sprites/block_0.png"));
        map.put(ic2.common.EntityDynamite.class, new RenderFlyingItem(62, "/ic2/sprites/item_0.png"));
        map.put(ic2.common.EntityMiningLaser.class, new RenderCrossed("/ic2/sprites/laser.png"));
    }

    public void RenderInvBlock(RenderBlocks renderblocks, Block block, int i, int j)
    {
        if(j == mod_IC2.fenceRenderId)
        {
            RenderBlockFence.renderInv(renderblocks, block, i);
        } else
        if(j == mod_IC2.miningPipeRenderId)
        {
            RenderBlockMiningPipe.renderInv(renderblocks, block, i);
        } else
        if(j == mod_IC2.luminatorRenderId)
        {
            RenderBlockLuminator.renderInv(renderblocks, block, i);
        }
    }

    public boolean RenderWorldBlock(RenderBlocks renderblocks, IBlockAccess iblockaccess, int i, int j, int k, Block block, int l)
    {
        if(block.getRenderType() == mod_IC2.cableRenderId)
        {
            return RenderBlockCable.render(renderblocks, iblockaccess, i, j, k, block, l);
        }
        if(block.getRenderType() == mod_IC2.fenceRenderId)
        {
            return RenderBlockFence.render(renderblocks, iblockaccess, i, j, k, block, l);
        }
        if(block.getRenderType() == mod_IC2.miningPipeRenderId)
        {
            return RenderBlockMiningPipe.render(renderblocks, iblockaccess, i, j, k, block, l);
        }
        if(block.getRenderType() == mod_IC2.luminatorRenderId)
        {
            return RenderBlockLuminator.render(renderblocks, iblockaccess, i, j, k, block, l);
        } else
        {
            return false;
        }
    }

    public GuiScreen HandleGUI(int i)
    {
        net.minecraft.src.EntityPlayerSP entityplayersp = ModLoader.getMinecraftInstance().thePlayer;
        if(entityplayersp == null)
        {
            return null;
        } else
        {
            return getGuiForId(entityplayersp, i, null);
        }
    }

    public static GuiScreen getGuiForId(EntityPlayer entityplayer, int i, TileEntity tileentity)
    {
        if(i == mod_IC2.guiIdIronFurnace)
        {
            return new GuiIronFurnace(entityplayer.inventory, tileentity == null ? new TileEntityIronFurnace() : (TileEntityIronFurnace)tileentity);
        }
        if(i == mod_IC2.guiIdElecFurnace)
        {
            return new GuiElecFurnace(entityplayer.inventory, tileentity == null ? new TileEntityElecFurnace() : (TileEntityElecFurnace)tileentity);
        }
        if(i == mod_IC2.guiIdMacerator)
        {
            return new GuiMacerator(entityplayer.inventory, tileentity == null ? new TileEntityMacerator() : (TileEntityMacerator)tileentity);
        }
        if(i == mod_IC2.guiIdExtractor)
        {
            return new GuiExtractor(entityplayer.inventory, tileentity == null ? new TileEntityExtractor() : (TileEntityExtractor)tileentity);
        }
        if(i == mod_IC2.guiIdCompressor)
        {
            return new GuiCompressor(entityplayer.inventory, tileentity == null ? new TileEntityCompressor() : (TileEntityCompressor)tileentity);
        }
        if(i == mod_IC2.guiIdCanner)
        {
            return new GuiCanner(entityplayer.inventory, tileentity == null ? new TileEntityCanner() : (TileEntityCanner)tileentity);
        }
        if(i == mod_IC2.guiIdMiner)
        {
            return new GuiMiner(entityplayer.inventory, tileentity == null ? new TileEntityMiner() : (TileEntityMiner)tileentity);
        }
        if(i == mod_IC2.guiIdPump)
        {
            return new GuiPump(entityplayer.inventory, tileentity == null ? new TileEntityPump() : (TileEntityPump)tileentity);
        }
        if(i == mod_IC2.guiIdElectrolyzer)
        {
            return new GuiElectrolyzer(entityplayer.inventory, tileentity == null ? new TileEntityElectrolyzer() : (TileEntityElectrolyzer)tileentity);
        }
        if(i == mod_IC2.guiIdRecycler)
        {
            return new GuiRecycler(entityplayer.inventory, tileentity == null ? new TileEntityRecycler() : (TileEntityRecycler)tileentity);
        }
        if(i == mod_IC2.guiIdInduction)
        {
            return new GuiInduction(entityplayer.inventory, tileentity == null ? new TileEntityInduction() : (TileEntityInduction)tileentity);
        }
        if(i == mod_IC2.guiIdMatter)
        {
            return new GuiMatter(entityplayer.inventory, tileentity == null ? new TileEntityMatter() : (TileEntityMatter)tileentity);
        }
        if(i == mod_IC2.guiIdElectricBatBox)
        {
            return new GuiElectricBlock(entityplayer.inventory, tileentity == null ? ((ic2.common.TileEntityElectricBlock) (new TileEntityElectricBatBox())) : ((ic2.common.TileEntityElectricBlock) ((TileEntityElectricBatBox)tileentity)));
        }
        if(i == mod_IC2.guiIdElectricMFE)
        {
            return new GuiElectricBlock(entityplayer.inventory, tileentity == null ? ((ic2.common.TileEntityElectricBlock) (new TileEntityElectricMFE())) : ((ic2.common.TileEntityElectricBlock) ((TileEntityElectricMFE)tileentity)));
        }
        if(i == mod_IC2.guiIdElectricMFSU)
        {
            return new GuiElectricBlock(entityplayer.inventory, tileentity == null ? ((ic2.common.TileEntityElectricBlock) (new TileEntityElectricMFSU())) : ((ic2.common.TileEntityElectricBlock) ((TileEntityElectricMFSU)tileentity)));
        }
        if(i == mod_IC2.guiIdGeoGenerator)
        {
            return new GuiGeoGenerator(entityplayer.inventory, tileentity == null ? new TileEntityGeoGenerator() : (TileEntityGeoGenerator)tileentity);
        }
        if(i == mod_IC2.guiIdWaterGenerator)
        {
            return new GuiWaterGenerator(entityplayer.inventory, tileentity == null ? new TileEntityWaterGenerator() : (TileEntityWaterGenerator)tileentity);
        }
        if(i == mod_IC2.guiIdSolarGenerator)
        {
            return new GuiSolarGenerator(entityplayer.inventory, tileentity == null ? new TileEntitySolarGenerator() : (TileEntitySolarGenerator)tileentity);
        }
        if(i == mod_IC2.guiIdWindGenerator)
        {
            return new GuiWindGenerator(entityplayer.inventory, tileentity == null ? new TileEntityWindGenerator() : (TileEntityWindGenerator)tileentity);
        }
        if(i == mod_IC2.guiIdGenerator)
        {
            return new GuiGenerator(entityplayer.inventory, tileentity == null ? new TileEntityGenerator() : (TileEntityGenerator)tileentity);
        }
        if(i == mod_IC2.guiIdTradeOMatOpen)
        {
            return new GuiTradeOMatOpen(entityplayer.inventory, tileentity == null ? new TileEntityTradeOMat() : (TileEntityTradeOMat)tileentity);
        }
        if(i == mod_IC2.guiIdTradeOMatClosed)
        {
            return new GuiTradeOMatClosed(entityplayer.inventory, tileentity == null ? new TileEntityTradeOMat() : (TileEntityTradeOMat)tileentity);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x3)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)3);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x4)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)4);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x5)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)5);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x6)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)6);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x7)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)7);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x8)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)8);
        }
        if(i == mod_IC2.guiIdNuclearReactor6x9)
        {
            return new GuiNuclearReactor(entityplayer.inventory, tileentity == null ? new TileEntityNuclearReactor() : (TileEntityNuclearReactor)tileentity, (short)9);
        } else
        {
            return null;
        }
    }

    public boolean OnTickInGame(Minecraft minecraft)
    {
        return mod_IC2.OnTickInGame(minecraft.theWorld.playerEntities, minecraft.theWorld);
    }

    public Configuration lang;
    public static KeyBinding keySuitActivate = new KeyBinding("Suit Activate", 29);
    public static KeyBinding keyJetpackHover = new KeyBinding("Hover Mode", 35);
    public static KeyBinding keyLaserMode = new KeyBinding("Laser Mode", 50);

    static 
    {
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_0.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_cable.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_electric.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_generator.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_machine.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_machine2.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/block_personal.png");
        MinecraftForgeClient.preloadTexture("/ic2/sprites/item_0.png");
    }
}
