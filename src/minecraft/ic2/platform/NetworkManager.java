// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.*;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

// Referenced classes of package ic2.platform:
//            Platform

public class NetworkManager
{

    public NetworkManager()
    {
    }

    public static void updateTileEntityField(TileEntity tileentity, String s)
    {
        if(tileentity instanceof INetworkUpdateListener)
        {
            ((INetworkUpdateListener)tileentity).onNetworkUpdate(s);
        }
    }

    public static void initiateTileEntityEvent(TileEntity tileentity, int i, boolean flag)
    {
        if(tileentity instanceof INetworkTileEntityEventListener)
        {
            ((INetworkTileEntityEventListener)tileentity).onNetworkEvent(i);
        }
    }

    public static void initiateItemEvent(EntityPlayer entityplayer, ItemStack itemstack, int i, boolean flag)
    {
        Item item = itemstack.getItem();
        if(item instanceof INetworkItemEventListener)
        {
            ((INetworkItemEventListener)item).onNetworkEvent(itemstack.getItemDamage(), entityplayer, i);
        }
    }

    public static void announceBlockUpdate(World world, int i, int j, int k)
    {
        world.markBlockNeedsUpdate(i, j, k);
    }

    public static void requestInitialTileEntityData(World world, int i, int j, int k)
    {
        Packet230ModLoader packet230modloader = new Packet230ModLoader();
        packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
        packet230modloader.isChunkDataPacket = true;
        packet230modloader.packetType = 0;
        packet230modloader.dataInt = new int[4];
        packet230modloader.dataInt[0] = world.worldProvider.worldType;
        packet230modloader.dataInt[1] = i;
        packet230modloader.dataInt[2] = j;
        packet230modloader.dataInt[3] = k;
        packet230modloader.dataFloat = new float[0];
        packet230modloader.dataString = new String[0];
        ModLoaderMp.SendPacket(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), packet230modloader);
    }

    public static void initiateClientItemEvent(ItemStack itemstack, int i)
    {
        Item item = itemstack.getItem();
        if(Platform.isSimulating())
        {
            if(item instanceof INetworkItemEventListener)
            {
                ((INetworkItemEventListener)item).onNetworkEvent(itemstack.getItemDamage(), Platform.getPlayerInstance(), i);
            }
        } else
        {
            Packet230ModLoader packet230modloader = new Packet230ModLoader();
            packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
            packet230modloader.isChunkDataPacket = true;
            packet230modloader.packetType = 1;
            packet230modloader.dataInt = new int[3];
            packet230modloader.dataInt[0] = itemstack.itemID;
            packet230modloader.dataInt[1] = itemstack.getItemDamage();
            packet230modloader.dataInt[2] = i;
            packet230modloader.dataFloat = new float[0];
            packet230modloader.dataString = new String[0];
            ModLoaderMp.SendPacket(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), packet230modloader);
        }
    }

    public static void handlePacket(Packet230ModLoader packet230modloader)
    {
label0:
        {
            if(packet230modloader.packetType == 0 && packet230modloader.dataInt.length > 0)
            {
                World world = ModLoader.getMinecraftInstance().theWorld;
                if(world.worldProvider.worldType != packet230modloader.dataInt[0])
                {
                    return;
                }
                int i = 0;
                int j = 0;
                for(int k = 1; k + 3 < packet230modloader.dataInt.length; k += 4)
                {
                    TileEntity tileentity1 = world.getBlockTileEntity(packet230modloader.dataInt[k], packet230modloader.dataInt[k + 1], packet230modloader.dataInt[k + 2]);
                    String s = packet230modloader.dataString[j];
                    j++;
                    Field field = null;
                    try
                    {
                        if(tileentity1 != null)
                        {
                            Class class1 = tileentity1.getClass();
                            do
                            {
                                try
                                {
                                    field = class1.getDeclaredField(s);
                                }
                                catch(NoSuchFieldException nosuchfieldexception)
                                {
                                    class1 = class1.getSuperclass();
                                }
                            } while(field == null && class1 != null);
                            if(field == null)
                            {
                                throw new NoSuchFieldException(s);
                            }
                            field.setAccessible(true);
                        }
                        switch(packet230modloader.dataInt[k + 3])
                        {
                        case 0: // '\0'
                            if(tileentity1 != null)
                            {
                                field.setFloat(tileentity1, packet230modloader.dataFloat[i]);
                            }
                            i++;
                            break;

                        case 1: // '\001'
                            if(tileentity1 != null)
                            {
                                field.setInt(tileentity1, packet230modloader.dataInt[k + 4]);
                            }
                            k++;
                            break;

                        case 2: // '\002'
                            if(tileentity1 != null)
                            {
                                field.set(tileentity1, packet230modloader.dataString[j]);
                            }
                            j++;
                            break;

                        case 3: // '\003'
                            if(tileentity1 != null)
                            {
                                field.setBoolean(tileentity1, packet230modloader.dataInt[k + 4] != 0);
                            }
                            k++;
                            break;

                        case 4: // '\004'
                            if(tileentity1 != null)
                            {
                                field.setByte(tileentity1, (byte)packet230modloader.dataInt[k + 4]);
                            }
                            k++;
                            break;

                        case 5: // '\005'
                            if(tileentity1 != null)
                            {
                                field.setShort(tileentity1, (short)packet230modloader.dataInt[k + 4]);
                            }
                            k++;
                            break;

                        default:
                            throw new RuntimeException((new StringBuilder()).append("Invalid field type index: ").append(packet230modloader.dataInt[k + 3]).toString());
                        }
                    }
                    catch(Exception exception)
                    {
                        throw new RuntimeException(exception);
                    }
                    if(tileentity1 instanceof INetworkUpdateListener)
                    {
                        ((INetworkUpdateListener)tileentity1).onNetworkUpdate(s);
                    }
                }

                break label0;
            }
            if(packet230modloader.packetType == 1 && packet230modloader.dataInt.length == 5)
            {
                World world1 = ModLoader.getMinecraftInstance().theWorld;
                if(world1.worldProvider.worldType != packet230modloader.dataInt[0])
                {
                    return;
                }
                TileEntity tileentity = world1.getBlockTileEntity(packet230modloader.dataInt[1], packet230modloader.dataInt[2], packet230modloader.dataInt[3]);
                if(tileentity instanceof INetworkTileEntityEventListener)
                {
                    ((INetworkTileEntityEventListener)tileentity).onNetworkEvent(packet230modloader.dataInt[4]);
                }
                break label0;
            }
            if(packet230modloader.packetType == 2 && packet230modloader.dataInt.length == 3)
            {
                World world2 = ModLoader.getMinecraftInstance().theWorld;
                Iterator iterator = world2.playerEntities.iterator();
                EntityPlayer entityplayer;
                do
                {
                    if(!iterator.hasNext())
                    {
                        break label0;
                    }
                    Object obj = iterator.next();
                    entityplayer = (EntityPlayer)obj;
                } while(!entityplayer.username.equals(packet230modloader.dataString[0]));
                Item item = Item.itemsList[packet230modloader.dataInt[0]];
                if(item instanceof INetworkItemEventListener)
                {
                    ((INetworkItemEventListener)item).onNetworkEvent(packet230modloader.dataInt[1], entityplayer, packet230modloader.dataInt[2]);
                }
            } else
            if(packet230modloader.packetType == 3 && packet230modloader.dataInt.length == 4)
            {
                World world3 = ModLoader.getMinecraftInstance().theWorld;
                if(world3.worldProvider.worldType != packet230modloader.dataInt[0])
                {
                    return;
                }
                world3.markBlockNeedsUpdate(packet230modloader.dataInt[1], packet230modloader.dataInt[2], packet230modloader.dataInt[3]);
            }
        }
    }
}
