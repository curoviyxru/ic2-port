// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerPump;
import ic2.common.TileEntityPump;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiPump extends GuiContainer
{

    public GuiPump(InventoryPlayer inventoryplayer, TileEntityPump tileentitypump)
    {
        super(new ContainerPump(inventoryplayer, tileentitypump));
        tileentity = tileentitypump;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Pump", 63, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIPump.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.energy > 0)
        {
            int l = (tileentity.energy * 14) / 200;
            if(l > 14)
            {
                l = 14;
            }
            drawTexturedModalRect(j + 62, (k + 36 + 14) - l, 176, 14 - l, 14, l);
        }
        int i1 = (tileentity.pumpCharge * 41) / 200;
        if(i1 > 41)
        {
            i1 = 41;
        }
        drawTexturedModalRect(j + 99, (k + 61) - i1, 176, 55, 12, 5);
        if(i1 > 0)
        {
            drawTexturedModalRect(j + 99, (k + 25 + 41) - i1, 176, 14, 12, i1);
        }
        drawTexturedModalRect(j + 98, k + 19, 188, 14, 13, 47);
    }

    public TileEntityPump tileentity;
}
