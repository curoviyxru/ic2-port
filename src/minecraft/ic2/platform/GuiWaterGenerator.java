// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerWaterGenerator;
import ic2.common.TileEntityWaterGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiWaterGenerator extends GuiContainer
{

    public GuiWaterGenerator(InventoryPlayer inventoryplayer, TileEntityWaterGenerator tileentitywatergenerator)
    {
        super(new ContainerWaterGenerator(inventoryplayer, tileentitywatergenerator));
        tileentity = tileentitywatergenerator;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Water Mill", 55, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUIWaterGenerator.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.fuel > 0)
        {
            int l = tileentity.gaugeFuelScaled(14);
            drawTexturedModalRect(j + 80, (k + 36 + 14) - l, 176, 14 - l, 14, l);
        }
    }

    public TileEntityWaterGenerator tileentity;
}
