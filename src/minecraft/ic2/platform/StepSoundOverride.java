// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.*;
import java.lang.reflect.Field;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

// Referenced classes of package ic2.platform:
//            AudioManager

public class StepSoundOverride extends StepSound
{

    public StepSoundOverride(StepSound stepsound)
    {
        super(stepsound.field_1678_a, stepsound.field_1677_b, stepsound.field_1679_c); //TODO OK BUT NOT CHECKED
        parent = null;
        parent = stepsound;
    }

    public float getVolume()
    {
        return parent.getVolume();
    }

    public float getPitch()
    {
        return parent.getPitch();
    }

    public String stepSoundDir()
    {
        return parent.stepSoundDir();
    }

    public String stepSoundDir2()
    {
        if(capture)
        {
            int ai[] = new int[3];
            int i = 0;
            try
            {
                Field afield[] = (net.minecraft.src.PlayerControllerSP.class).getDeclaredFields();
                int j = afield.length;
                int k = 0;
                do
                {
                    if(k >= j)
                    {
                        break;
                    }
                    Field field = afield[k];
                    if(field.getType() == Integer.TYPE)
                    {
                        field.setAccessible(true);
                        ai[i] = field.getInt(ModLoader.getMinecraftInstance().playerController);
                        if(++i == 3)
                        {
                            break;
                        }
                    }
                    k++;
                } while(true);
            }
            catch(Exception exception)
            {
                throw new RuntimeException(exception);
            }
            if(hitSoundOverride != null)
            {
                String s = hitSoundOverride.getHitSoundForBlock(ai[0], ai[1], ai[2]);
                if(s != null)
                {
                    AudioManager.playOnce(new AudioPosition((float)ai[0] + 0.5F, (float)ai[1] + 0.5F, (float)ai[2] + 0.5F), PositionSpec.Center, s, true, AudioManager.defaultVolume);
                }
            }
            capture = false;
            return null;
        } else
        {
            return parent.func_1145_d(); //TODO IDK
        }
    }

    public static boolean capture = false;
    public static IHitSoundOverride hitSoundOverride = null;
    public StepSound parent;

}
