// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerTradeOMatOpen;
import ic2.common.TileEntityTradeOMat;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiTradeOMatOpen extends GuiContainer
{

    public GuiTradeOMatOpen(InventoryPlayer inventoryplayer, TileEntityTradeOMat tileentitytradeomat)
    {
        super(new ContainerTradeOMatOpen(inventoryplayer, tileentitytradeomat));
        tileentity = tileentitytradeomat;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Trade-O-Mat", 56, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
        fontRenderer.drawString("Performed", 112, 20, 0x404040);
        fontRenderer.drawString("Trades:", 112, 28, 0x404040);
        fontRenderer.drawString((new StringBuilder()).append("").append(tileentity.totalTradeCount).toString(), 112, 36, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/ic2/sprites/GUITradeOMatOpen.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
    }

    public TileEntityTradeOMat tileentity;
}
