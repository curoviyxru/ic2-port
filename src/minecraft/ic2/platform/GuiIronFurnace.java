// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.ContainerIronFurnace;
import ic2.common.TileEntityIronFurnace;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class GuiIronFurnace extends GuiContainer
{

    public GuiIronFurnace(InventoryPlayer inventoryplayer, TileEntityIronFurnace tileentityironfurnace)
    {
        super(new ContainerIronFurnace(inventoryplayer, tileentityironfurnace));
        tileentity = tileentityironfurnace;
    }

    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Iron Furnace", 55, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }

    protected void drawGuiContainerBackgroundLayer(float f)
    {
        int i = mc.renderEngine.getTexture("/gui/furnace.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(i);
        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
        if(tileentity.fuel > 0)
        {
            int l = tileentity.gaugeFuelScaled(12);
            drawTexturedModalRect(j + 56, (k + 36 + 12) - l, 176, 12 - l, 14, l + 2);
        }
        int i1 = tileentity.gaugeProgressScaled(24);
        drawTexturedModalRect(j + 79, k + 34, 176, 14, i1 + 1, 16);
    }

    public TileEntityIronFurnace tileentity;
}
