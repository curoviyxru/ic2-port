// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

public class RenderBlockFence
{

    public RenderBlockFence()
    {
    }

    public static boolean render(RenderBlocks renderblocks, IBlockAccess iblockaccess, int i, int j, int k, Block block, int l)
    {
        float f = 0.25F;
        float f1 = (1.0F - f) / 2.0F;
        float f2 = 0.125F;
        float f3 = (1.0F - f2) / 2.0F;
        float f4 = 0.75F;
        float f5 = 0.9375F;
        float f6 = 0.375F;
        float f7 = 0.5625F;
        block.setBlockBounds(f1, 0.0F, f1, f1 + f, 1.0F, f1 + f);
        renderblocks.renderStandardBlock(block, i, j, k);
        int i1 = iblockaccess.getBlockId(i + 1, j, k);
        if(i1 == block.blockID || i1 == Block.fence.blockID)
        {
            block.setBlockBounds(f1 + f, f4, f3, 1.0F + f1, f5, f3 + f2);
            renderblocks.renderStandardBlock(block, i, j, k);
            block.setBlockBounds(f1 + f, f6, f3, 1.0F + f1, f7, f3 + f2);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        i1 = iblockaccess.getBlockId(i, j, k + 1);
        if(i1 == block.blockID || i1 == Block.fence.blockID)
        {
            block.setBlockBounds(f3, f4, f1 + f, f3 + f2, f5, 1.0F + f1);
            renderblocks.renderStandardBlock(block, i, j, k);
            block.setBlockBounds(f3, f6, f1 + f, f3 + f2, f7, 1.0F + f1);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        i1 = iblockaccess.getBlockId(i - 1, j, k);
        if(i1 == Block.fence.blockID)
        {
            block.setBlockBounds(-f1, f4, f3, f1, f5, f3 + f2);
            renderblocks.renderStandardBlock(block, i, j, k);
            block.setBlockBounds(-f1, f6, f3, f1, f7, f3 + f2);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        i1 = iblockaccess.getBlockId(i, j, k - 1);
        if(i1 == Block.fence.blockID)
        {
            block.setBlockBounds(f3, f4, -f1, f3 + f2, f5, f1);
            renderblocks.renderStandardBlock(block, i, j, k);
            block.setBlockBounds(f3, f6, -f1, f3 + f2, f7, f1);
            renderblocks.renderStandardBlock(block, i, j, k);
        }
        block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        return true;
    }

    public static void renderInv(RenderBlocks renderblocks, Block block, int i)
    {
        Tessellator tessellator = Tessellator.instance;
        for(int j = 0; j < 4; j++)
        {
            float f = 0.125F;
            if(j == 0)
            {
                block.setBlockBounds(0.5F - f, 0.0F, 0.0F, 0.5F + f, 1.0F, f * 2.0F);
            }
            if(j == 1)
            {
                block.setBlockBounds(0.5F - f, 0.0F, 1.0F - f * 2.0F, 0.5F + f, 1.0F, 1.0F);
            }
            f = 0.0625F;
            if(j == 2)
            {
                block.setBlockBounds(0.5F - f, 1.0F - f * 3F, -f * 2.0F, 0.5F + f, 1.0F - f, 1.0F + f * 2.0F);
            }
            if(j == 3)
            {
                block.setBlockBounds(0.5F - f, 0.5F - f * 3F, -f * 2.0F, 0.5F + f, 0.5F - f, 1.0F + f * 2.0F);
            }
            GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, -1F, 0.0F);
            renderblocks.renderBottomFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(0));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 1.0F, 0.0F);
            renderblocks.renderTopFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(1));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 0.0F, -1F);
            renderblocks.renderEastFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(2));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 0.0F, 1.0F);
            renderblocks.renderWestFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(3));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(-1F, 0.0F, 0.0F);
            renderblocks.renderNorthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(4));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(1.0F, 0.0F, 0.0F);
            renderblocks.renderSouthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSide(5));
            tessellator.draw();
            GL11.glTranslatef(0.5F, 0.5F, 0.5F);
        }

        block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
    }
}
