// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import forge.Configuration;
import forge.Property;
import ic2.common.AudioPosition;
import ic2.common.PositionSpec;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.IntBuffer;
import java.util.*;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;

// Referenced classes of package ic2.platform:
//            AudioSource

public final class AudioManager
{

    public AudioManager()
    {
    }

    public static void initialize()
    {
        if(mod_IC2.config != null)
        {
            enabled = Boolean.parseBoolean(mod_IC2.config.getOrCreateBooleanProperty("soundsEnabled", 0, enabled).value);
            maxSourceCount = Integer.parseInt(mod_IC2.config.getOrCreateIntProperty("soundSourceLimit", 0, maxSourceCount).value);
            mod_IC2.config.save();
            if(maxSourceCount <= 6)
            {
                enabled = false;
            }
        }
        if(!enabled)
        {
            return;
        }
        int i = 0;
        int j = maxSourceCount + 1;
        try
        {
            AL.create();
        }
        catch(Exception exception) { }
        while(i < j - 1) 
        {
            int k = (i + j) / 2;
            if(testSourceCount(k))
            {
                i = k;
            } else
            {
                j = k;
            }
        }
        AL.destroy();
        maxSourceCount = i;
        if(maxSourceCount < 6)
        {
            enabled = false;
            return;
        } else
        {
            System.out.println((new StringBuilder()).append("Using ").append(maxSourceCount).append(" audio sources.").toString());
            SoundSystemConfig.setNumberStreamingChannels(streamingSourceCount);
            SoundSystemConfig.setNumberNormalChannels(maxSourceCount - streamingSourceCount);
            return;
        }
    }

    public static void onTick()
    {
        if(!enabled || soundSystem == null)
        {
            return;
        }
        if(ticker++ % 8 == 0 && soundSystem != null)
        {
            float f = ModLoader.getMinecraftInstance().gameSettings.soundVolume;
            if(f != masterVolume)
            {
                float f1 = f / masterVolume;
                masterVolume = f;
                for(Iterator iterator = objectToAudioSourceMap.values().iterator(); iterator.hasNext();)
                {
                    List list = (List)iterator.next();
                    Iterator iterator1 = list.iterator();
                    while(iterator1.hasNext()) 
                    {
                        AudioSource audiosource = (AudioSource)iterator1.next();
                        audiosource.setVolume(audiosource.getVolume() * f1);
                    }
                }

            }
        }
    }

    public static AudioSource createSource(Object obj, String s)
    {
        return createSource(obj, PositionSpec.Center, s, false, false, defaultVolume);
    }

    public static AudioSource createSource(Object obj, PositionSpec positionspec, String s, boolean flag, boolean flag1, float f)
    {
        if(!enabled)
        {
            return null;
        }
        if(soundSystem == null)
        {
            getSoundSystem();
        }
        if(soundSystem == null)
        {
            return null;
        }
        String s1 = getSourceName(nextId);
        nextId++;
        AudioSource audiosource = new AudioSource(soundSystem, s1, obj, positionspec, s, flag, flag1, f);
        if(!objectToAudioSourceMap.containsKey(obj))
        {
            objectToAudioSourceMap.put(obj, new LinkedList());
        }
        ((List)objectToAudioSourceMap.get(obj)).add(audiosource);
        return audiosource;
    }

    public static void removeSources(Object obj)
    {
        if(soundSystem == null)
        {
            return;
        }
        if(!objectToAudioSourceMap.containsKey(obj))
        {
            return;
        }
        AudioSource audiosource;
        for(Iterator iterator = ((List)objectToAudioSourceMap.get(obj)).iterator(); iterator.hasNext(); audiosource.remove())
        {
            audiosource = (AudioSource)iterator.next();
        }

        objectToAudioSourceMap.remove(obj);
    }

    public static void playOnce(Object obj, String s)
    {
        playOnce(obj, PositionSpec.Center, s, false, defaultVolume);
    }

    public static void playOnce(Object obj, PositionSpec positionspec, String s, boolean flag, float f)
    {
        if(!enabled)
        {
            return;
        }
        if(soundSystem == null)
        {
            getSoundSystem();
        }
        if(soundSystem == null)
        {
            return;
        }
        AudioPosition audioposition = AudioPosition.getFrom(obj, positionspec);
        URL url = (ic2.platform.AudioSource.class).getClassLoader().getResource((new StringBuilder()).append("ic2/sounds/").append(s).toString());
        if(url == null)
        {
            System.out.println((new StringBuilder()).append("Invalid sound file: ").append(s).toString());
            return;
        } else
        {
            String s1 = soundSystem.quickPlay(flag, url, s, false, audioposition.x, audioposition.y, audioposition.z, 2, fadingDistance * Math.max(f, 1.0F));
            soundSystem.setVolume(s1, masterVolume * Math.min(f, 1.0F));
            return;
        }
    }

    public static float getMasterVolume()
    {
        return masterVolume;
    }

    private static boolean testSourceCount(int i)
    {
        IntBuffer intbuffer = BufferUtils.createIntBuffer(i);
        try
        {
            AL10.alGenSources(intbuffer);
            if(AL10.alGetError() == 0)
            {
                AL10.alDeleteSources(intbuffer);
                return true;
            }
        }
        catch(Exception exception)
        {
            AL10.alGetError();
        }
        catch(UnsatisfiedLinkError unsatisfiedlinkerror) { }
        return false;
    }

    private static void getSoundSystem()
    {
        Field afield[] = (net.minecraft.src.SoundManager.class).getDeclaredFields();
        int i = afield.length;
        int j = 0;
        do
        {
            if(j >= i)
            {
                break;
            }
            Field field = afield[j];
            if(field.getType() == (paulscode.sound.SoundSystem.class))
            {
                field.setAccessible(true);
                try
                {
                    Object obj = field.get(null);
                    if(obj instanceof SoundSystem)
                    {
                        soundSystem = (SoundSystem)obj;
                    }
                }
                catch(Exception exception)
                {
                    exception.printStackTrace();
                    soundSystem = null;
                }
                break;
            }
            j++;
        } while(true);
    }

    private static String getSourceName(int i)
    {
        return (new StringBuilder()).append("asm_snd").append(i).toString();
    }

    public static float defaultVolume = 1.2F;
    public static float fadingDistance = 16F;
    private static boolean enabled = true;
    private static int maxSourceCount = 32;
    private static int streamingSourceCount = 4;
    private static boolean lateInitDone = false;
    private static SoundSystem soundSystem = null;
    private static float masterVolume = 0.5F;
    private static int ticker = 0;
    private static int nextId = 0;
    private static Map objectToAudioSourceMap = new HashMap();

}
