// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.src.BlockGlass;
import net.minecraft.src.Material;

public class BlockTexGlass extends BlockGlass
    implements ITextureProvider
{

    public BlockTexGlass(int i, int j, Material material, boolean flag)
    {
        super(i, j, material, flag);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }
}
