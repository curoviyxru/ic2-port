// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.nio.ByteBuffer;

interface IPersistentItemData
{

    public abstract void loadFromBuffer(short word0, short word1, ByteBuffer bytebuffer);

    public abstract ByteBuffer saveToBuffer();

    public abstract short getVersion();
}
