// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.*;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemArmorNanoSuit

public class ItemArmorBackpack extends ItemArmor
    implements ITextureProvider, ISpecialArmor
{

    public ItemArmorBackpack(int i, int j, int k)
    {
        super(i, 0, k, 1);
        iconIndex = j;
        damageReduceAmount = 0;
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public ArmorProperties getProperties(EntityPlayer entityplayer, int i, int j)
    {
        return ((ItemArmorNanoSuit)mod_IC2.itemArmorNanoHelmet).getProperties(entityplayer, i, j);
    }

    public int damageReduceAmount;
}
