// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import java.util.Random;
import net.minecraft.src.BlockDoor;
import net.minecraft.src.Material;

public class BlockIC2Door extends BlockDoor
    implements ITextureProvider
{

    public BlockIC2Door(int i, int j, int k, Material material)
    {
        super(i, material);
        spriteIndexTop = j;
        spriteIndexBottom = k;
    }

    public BlockIC2Door setItemDropped(int i)
    {
        itemDropped = i;
        return this;
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        if((j & 8) == 8)
        {
            return spriteIndexTop;
        } else
        {
            return spriteIndexBottom;
        }
    }

    public int idDropped(int i, Random random)
    {
        if((i & 8) == 8)
        {
            return 0;
        } else
        {
            return itemDropped;
        }
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }

    public int spriteIndexTop;
    public int spriteIndexBottom;
    public int itemDropped;
}
