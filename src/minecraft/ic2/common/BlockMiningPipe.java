// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockMiningPipe extends BlockTex
{

    public BlockMiningPipe(int i, int j)
    {
        super(i, j, Material.iron);
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        return false;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return mod_IC2.miningPipeRenderId;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.getBoundingBoxFromPool((float)i + 0.375F, (float)j, (float)k + 0.375F, (float)i + 0.625F, (float)j + 1.0F, (float)k + 0.625F);
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.getBoundingBoxFromPool((float)i + 0.375F, (float)j, (float)k + 0.375F, (float)i + 0.625F, (float)j + 1.0F, (float)k + 0.625F);
    }
}
