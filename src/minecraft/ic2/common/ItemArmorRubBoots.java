// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.*;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemArmorNanoSuit

public class ItemArmorRubBoots extends ItemArmor
    implements ITextureProvider, ISpecialArmor
{

    public ItemArmorRubBoots(int i, int j, int k)
    {
        super(i, 0, k, 3);
        iconIndex = j;
        setMaxDamage(64);
        damageReduceAmount = 0;
    }

    public ArmorProperties getProperties(EntityPlayer entityplayer, int i, int j)
    {
        return ((ItemArmorNanoSuit)mod_IC2.itemArmorNanoHelmet).getProperties(entityplayer, i, j);
    }

    public static void absorbFalling(EntityPlayer entityplayer)
    {
        float f = mod_IC2.getFallDistanceOfEntity(entityplayer);
        if(f < 1.0F && fallStorage == 0.0F)
        {
            return;
        }
        if(fallStorage > 0.0F && (entityplayer.isInWater() || entityplayer.isOnLadder()))
        {
            fallStorage = 0.0F;
        }
        if(f >= 1.0F)
        {
            f--;
            mod_IC2.setFallDistanceOfEntity(entityplayer, f);
            fallStorage++;
        }
        if(entityplayer.onGround)
        {
            if(fallStorage < 3F)
            {
                fallStorage = 0.0F;
            } else
            {
                int i = (int)Math.ceil(fallStorage - 3F);
                i = (i + 1) / 2;
                ItemStack itemstack = entityplayer.inventory.armorInventory[0];
                itemstack.damageItem(i, entityplayer);
                if(itemstack.stackSize <= 0)
                {
                    entityplayer.inventory.armorInventory[0] = null;
                }
                if(i >= 4)
                {
                    entityplayer.attackEntityFrom(null, i / 4);
                }
                fallStorage = 0.0F;
            }
        }
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public static void multiplyFall(Entity entity, float f)
    {
        float f1 = mod_IC2.getFallDistanceOfEntity(entity);
        f1 *= f;
        mod_IC2.setFallDistanceOfEntity(entity, f1);
    }

    public int damageReduceAmount;
    public static float fallStorage = 0.0F;

}
