// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ForgeHooks;
import forge.ITextureProvider;
import ic2.platform.ItemToolCommon;
import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            IChargeableItem, ItemArmorBatpack

public abstract class ItemElectricTool extends ItemToolCommon
    implements IChargeableItem, ITextureProvider
{

    public ItemElectricTool(int i, int j, EnumToolMaterial enumtoolmaterial, Block ablock[], int k, int l, int i1)
    {
        super(i, 0, enumtoolmaterial, ablock);
        tier = k;
        ratio = l;
        transfer = i1;
        mineables = ablock;
        iconIndex = j;
    }

    public float getStrVsBlock(ItemStack itemstack, Block block)
    {
        if(itemstack.getItemDamage() + 1 >= itemstack.getMaxDamage())
        {
            return 1.0F;
        }
        if(ForgeHooks.isToolEffective(itemstack, block, 0))
        {
            return efficiencyOnProperMaterial;
        }
        if(canHarvestBlock(block))
        {
            return efficiencyOnProperMaterial;
        } else
        {
            return 1.0F;
        }
    }

    public float getStrVsBlock(ItemStack itemstack, Block block, int i)
    {
        if(itemstack.getItemDamage() + 1 >= itemstack.getMaxDamage())
        {
            return 1.0F;
        }
        if(ForgeHooks.isToolEffective(itemstack, block, i))
        {
            return efficiencyOnProperMaterial;
        }
        if(canHarvestBlock(block))
        {
            return efficiencyOnProperMaterial;
        } else
        {
            return 1.0F;
        }
    }

    public boolean canHarvestBlock(Block block)
    {
        for(int i = 0; i < mineables.length; i++)
        {
            if(mineables[i] == block)
            {
                return true;
            }
        }

        return false;
    }

    public boolean hitEntity(ItemStack itemstack, EntityLiving entityliving, EntityLiving entityliving1)
    {
        return true;
    }

    public boolean onBlockDestroyed(ItemStack itemstack, int i, int j, int k, int l, EntityLiving entityliving)
    {
        return true;
    }

    public static boolean use(ItemStack itemstack, int i, EntityPlayer entityplayer)
    {
        chargeFromBatpack(itemstack, entityplayer);
        if(itemstack.getItemDamage() + i > itemstack.getMaxDamage() - 1)
        {
            return false;
        }
        if(Platform.isSimulating())
        {
            itemstack.setItemDamage(itemstack.getItemDamage() + i);
            chargeFromBatpack(itemstack, entityplayer);
        }
        return true;
    }

    public static void chargeFromBatpack(ItemStack itemstack, EntityPlayer entityplayer)
    {
        if(entityplayer == null || entityplayer.inventory.armorInventory[2] == null || !(entityplayer.inventory.armorInventory[2].getItem() instanceof ItemArmorBatpack))
        {
            return;
        } else
        {
            ((ItemArmorBatpack)entityplayer.inventory.armorInventory[2].getItem()).useBatpackOn(itemstack, entityplayer.inventory.armorInventory[2]);
            return;
        }
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < tier || itemstack.getItemDamage() == 1)
        {
            return 0;
        }
        int k = (itemstack.getItemDamage() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getItemDamage() - i / ratio);
        return i;
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public int tier;
    public int ratio;
    public int transfer;
    public Block mineables[];
}
