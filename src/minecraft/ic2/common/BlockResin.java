// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex, ItemArmorRubBoots

public class BlockResin extends BlockTex
{

    public BlockResin(int i, int j)
    {
        super(i, j, Material.circuits);
        setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return null;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int idDropped(int i, Random random)
    {
        return mod_IC2.itemHarz.shiftedIndex;
    }

    public int quantityDropped(Random random)
    {
        return random.nextInt(5) != 0 ? 1 : 0;
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        int l = world.getBlockId(i, j - 1, k);
        if(l == 0 || !Block.blocksList[l].isOpaqueCube())
        {
            return false;
        } else
        {
            return world.getBlockMaterial(i, j - 1, k).getIsSolid();
        }
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(!canPlaceBlockAt(world, i, j, k))
        {
            dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
            world.setBlockWithNotify(i, j, k, 0);
        }
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        ItemArmorRubBoots.multiplyFall(entity, 0.75F);
        entity.motionX *= 0.60000002384185791D;
        entity.motionY *= 0.85000002384185791D;
        entity.motionZ *= 0.60000002384185791D;
    }
}
