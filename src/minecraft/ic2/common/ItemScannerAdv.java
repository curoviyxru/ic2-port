// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.ItemStack;

// Referenced classes of package ic2.common:
//            ItemScanner

public class ItemScannerAdv extends ItemScanner
{

    public ItemScannerAdv(int i, int j, int k)
    {
        super(i, j, k);
    }

    public int startLayerScan(ItemStack itemstack)
    {
        return use(itemstack, 5, null) ? 4 : 0;
    }
}
