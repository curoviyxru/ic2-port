// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElectricMachine, ContainerElectricMachine

public class TileEntityRecycler extends TileEntityElectricMachine
{

    public TileEntityRecycler()
    {
        super(3, 1, 35, 32);
    }

    public void operate()
    {
        if(!canOperate())
        {
            return;
        }
        inventory[0].stackSize--;
        if(inventory[0].stackSize <= 0)
        {
            inventory[0] = null;
        }
        if(worldObj.rand.nextInt(recycleChance()) == 0)
        {
            if(inventory[2] == null)
            {
                inventory[2] = new ItemStack(mod_IC2.itemScrap);
            } else
            {
                inventory[2].stackSize++;
            }
        }
    }

    public boolean canOperate()
    {
        if(inventory[0] == null)
        {
            return false;
        }
        return inventory[2] == null || inventory[2].itemID == mod_IC2.itemScrap.shiftedIndex && inventory[2].stackSize < mod_IC2.itemScrap.getItemStackLimit();
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return null;
    }

    public String getInvName()
    {
        return "Recycler";
    }

    public static int recycleChance()
    {
        return 8;
    }

    public String getStartSoundFile()
    {
        return "Machines/RecyclerOp.ogg";
    }

    public String getInterruptSoundFile()
    {
        return "Machines/InterruptOne.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricMachine(inventoryplayer, this);
    }

    public float getWrenchDropRate()
    {
        return 0.85F;
    }
}
