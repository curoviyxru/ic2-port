// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.IUseItemFirst;
import ic2.api.IWrenchable;
import ic2.platform.AudioManager;
import ic2.platform.Platform;
import java.util.*;
import java.util.logging.Level;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, TileEntityTerra, PositionSpec, StackUtil

public class ItemToolWrench extends ItemIC2
    implements IUseItemFirst
{

    public ItemToolWrench(int i, int j)
    {
        super(i, j);
        setMaxDamage(160);
        setMaxStackSize(1);
    }

    public boolean canTakeDamage(ItemStack itemstack, int i)
    {
        return true;
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        return false;
    }

    public boolean onItemUseFirst(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(!canTakeDamage(itemstack, 1))
        {
            return false;
        }
        int i1 = world.getBlockId(i, j, k);
        int j1 = world.getBlockMetadata(i, j, k);
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(tileentity instanceof TileEntityTerra)
        {
            TileEntityTerra tileentityterra = (TileEntityTerra)tileentity;
            if(tileentityterra.ejectBlueprint())
            {
                if(Platform.isSimulating())
                {
                    damage(itemstack, 1, entityplayer);
                }
                if(Platform.isRendering())
                {
                    AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/wrench.ogg", true, AudioManager.defaultVolume);
                }
                return true;
            }
        }
        if(tileentity instanceof IWrenchable)
        {
            IWrenchable iwrenchable = (IWrenchable)tileentity;
            if(entityplayer.isSneaking())
            {
                l += (l % 2) * -2 + 1;
            }
            if(iwrenchable.wrenchSetFacing(entityplayer, l))
            {
                if(Platform.isSimulating())
                {
                    iwrenchable.setFacing((short)l);
                    damage(itemstack, 1, entityplayer);
                }
                if(Platform.isRendering())
                {
                    AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/wrench.ogg", true, AudioManager.defaultVolume);
                }
                return true;
            }
            if(canTakeDamage(itemstack, 10) && iwrenchable.wrenchRemove(entityplayer))
            {
                if(Platform.isSimulating())
                {
                    if(!Platform.isRendering() && mod_IC2.enableLoggingWrench)
                    {
                        String s = tileentity.getClass().getName().replace("TileEntity", "");
                        Platform.log(Level.INFO, (new StringBuilder()).append("Player ").append(entityplayer.username).append(" used the wrench to remove the ").append(s).append(" (").append(i1).append("-").append(j1).append(") at ").append(i).append("/").append(j).append("/").append(k).toString());
                    }
                    Block block = Block.blocksList[i1];
                    java.util.ArrayList arraylist = mod_IC2.getDrop(world, block, j1);
                    if(world.rand.nextFloat() <= iwrenchable.getWrenchDropRate())
                    {
                        arraylist.set(0, new ItemStack(i1, 1, j1));
                    }
                    ItemStack itemstack1;
                    for(Iterator iterator = arraylist.iterator(); iterator.hasNext(); StackUtil.dropAsEntity(world, i, j, k, itemstack1))
                    {
                        itemstack1 = (ItemStack)iterator.next();
                    }

                    world.setBlockWithNotify(i, j, k, 0);
                    damage(itemstack, 10, entityplayer);
                }
                if(Platform.isRendering())
                {
                    AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/wrench.ogg", true, AudioManager.defaultVolume);
                }
                return true;
            }
        }
        return false;
    }

    public void damage(ItemStack itemstack, int i, EntityPlayer entityplayer)
    {
        itemstack.damageItem(i, entityplayer);
    }
}
