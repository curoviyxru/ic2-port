// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import forge.MinecraftForge;
import ic2.platform.*;
import java.util.*;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, StackUtil, TileEntityGeoGenerator, 
//            ContainerPump, PositionSpec

public class TileEntityPump extends TileEntityElecMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityPump()
    {
        super(2, 1, 200, 32);
        pumpCharge = 0;
        soundTicker = mod_IC2.random.nextInt(64);
    }

    public String getInvName()
    {
        return "Pump";
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = false;
            if(energy > 0 && !isPumpReady())
            {
                energy--;
                pumpCharge++;
            }
            if(energy <= maxEnergy)
            {
                flag = provideEnergy();
            }
            if(isPumpReady())
            {
                flag = pump();
            }
            if(getActive() == isPumpReady() && energy > 0)
            {
                setActive(!getActive());
            }
            if(flag)
            {
                onInventoryChanged();
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public boolean pump()
    {
        if(!canHarvest())
        {
            return false;
        }
        if(isWaterBelow() || isLavaBelow())
        {
            int i = worldObj.getBlockId(xCoord, yCoord - 1, zCoord);
            worldObj.setBlockWithNotify(xCoord, yCoord - 1, zCoord, 0);
            if(i == Block.waterMoving.blockID)
            {
                i = Block.waterStill.blockID;
            }
            if(i == Block.lavaMoving.blockID)
            {
                i = Block.lavaStill.blockID;
            }
            return pumpThis(i);
        }
        if(inventory[0] != null && inventory[0].itemID == Item.bucketEmpty.shiftedIndex)
        {
            ItemStack itemstack = MinecraftForge.fillCustomBucket(worldObj, xCoord, yCoord - 1, zCoord);
            if(itemstack != null)
            {
                ArrayList arraylist = new ArrayList();
                arraylist.add(itemstack);
                StackUtil.distributeDrop(this, arraylist);
                inventory[0] = null;
                pumpCharge = 0;
                return true;
            }
        }
        return false;
    }

    public boolean isWaterBelow()
    {
        return (worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == Block.waterMoving.blockID || worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == Block.waterStill.blockID) && worldObj.getBlockMetadata(xCoord, yCoord - 1, zCoord) == 0;
    }

    public boolean isLavaBelow()
    {
        return (worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == Block.lavaMoving.blockID || worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == Block.lavaStill.blockID) && worldObj.getBlockMetadata(xCoord, yCoord - 1, zCoord) == 0;
    }

    public boolean pumpThis(int i)
    {
        if(i == Block.lavaStill.blockID && deliverLavaToGeo())
        {
            pumpCharge = 0;
            return true;
        }
        if(inventory[0] != null && inventory[0].itemID == Item.bucketEmpty.shiftedIndex)
        {
            if(i == Block.waterStill.blockID)
            {
                inventory[0].itemID = Item.bucketWater.shiftedIndex;
            }
            if(i == Block.lavaStill.blockID)
            {
                inventory[0].itemID = Item.bucketLava.shiftedIndex;
            }
            ArrayList arraylist = new ArrayList();
            arraylist.add(inventory[0]);
            StackUtil.distributeDrop(this, arraylist);
            inventory[0] = null;
            pumpCharge = 0;
            return true;
        }
        if(inventory[0] != null && inventory[0].itemID == mod_IC2.itemCellEmpty.shiftedIndex)
        {
            ItemStack itemstack = null;
            if(i == Block.waterStill.blockID)
            {
                itemstack = new ItemStack(mod_IC2.itemCellWater);
            }
            if(i == Block.lavaStill.blockID)
            {
                itemstack = new ItemStack(mod_IC2.itemCellLava);
            }
            inventory[0].stackSize--;
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
            ArrayList arraylist1 = new ArrayList();
            arraylist1.add(itemstack);
            StackUtil.distributeDrop(this, arraylist1);
            pumpCharge = 0;
            return true;
        } else
        {
            pumpCharge = 0;
            return putInChestBucket(i);
        }
    }

    public boolean putInChestBucket(int i)
    {
        return putInChestBucket(xCoord, yCoord + 1, zCoord, i) || putInChestBucket(xCoord, yCoord - 1, zCoord, i) || putInChestBucket(xCoord + 1, yCoord, zCoord, i) || putInChestBucket(xCoord - 1, yCoord, zCoord, i) || putInChestBucket(xCoord, yCoord, zCoord + 1, i) || putInChestBucket(xCoord, yCoord, zCoord - 1, i);
    }

    public boolean putInChestBucket(int i, int j, int k, int l)
    {
        if(!(worldObj.getBlockTileEntity(i, j, k) instanceof TileEntityChest))
        {
            return false;
        }
        TileEntityChest tileentitychest = (TileEntityChest)worldObj.getBlockTileEntity(i, j, k);
        for(int i1 = 0; i1 < tileentitychest.getSizeInventory(); i1++)
        {
            if(tileentitychest.getStackInSlot(i1) != null && tileentitychest.getStackInSlot(i1).itemID == Item.bucketEmpty.shiftedIndex)
            {
                if(l == Block.waterStill.blockID)
                {
                    tileentitychest.getStackInSlot(i1).itemID = Item.bucketWater.shiftedIndex;
                }
                if(l == Block.lavaStill.blockID)
                {
                    tileentitychest.getStackInSlot(i1).itemID = Item.bucketLava.shiftedIndex;
                }
                return true;
            }
        }

        return false;
    }

    public void fountain()
    {
        if(worldObj.getWorldTime() % 10L == 0L)
        {
            pumpCharge--;
        }
        int i = 0;
        for(int j = 1; j < 4; j++)
        {
            if(worldObj.getBlockId(xCoord, yCoord + j, zCoord) == 0 || worldObj.getBlockId(xCoord, yCoord + j, zCoord) == Block.waterMoving.blockID)
            {
                i = j;
            }
        }

        if(i != 0)
        {
            worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord + i, zCoord, Block.waterMoving.blockID, 1);
        }
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        pumpCharge = nbttagcompound.getShort("pumpCharge");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("pumpCharge", pumpCharge);
    }

    public boolean isPumpReady()
    {
        return pumpCharge >= 200;
    }

    public boolean canHarvest()
    {
        if(!isPumpReady())
        {
            return false;
        } else
        {
            return inventory[0] != null && (inventory[0].itemID == mod_IC2.itemCellEmpty.shiftedIndex || inventory[0].itemID == Item.bucketEmpty.shiftedIndex) || isBucketInChestAvaible();
        }
    }

    public boolean isBucketInChestAvaible()
    {
        return isBucketInChestAvaible(xCoord, yCoord + 1, zCoord) || isBucketInChestAvaible(xCoord, yCoord - 1, zCoord) || isBucketInChestAvaible(xCoord + 1, yCoord, zCoord) || isBucketInChestAvaible(xCoord - 1, yCoord, zCoord) || isBucketInChestAvaible(xCoord, yCoord, zCoord + 1) || isBucketInChestAvaible(xCoord, yCoord, zCoord - 1);
    }

    public boolean isBucketInChestAvaible(int i, int j, int k)
    {
        if(!(worldObj.getBlockTileEntity(i, j, k) instanceof TileEntityChest))
        {
            return false;
        }
        TileEntityChest tileentitychest = (TileEntityChest)worldObj.getBlockTileEntity(i, j, k);
        for(int l = 0; l < tileentitychest.getSizeInventory(); l++)
        {
            if(tileentitychest.getStackInSlot(l) != null && tileentitychest.getStackInSlot(l).itemID == Item.bucketEmpty.shiftedIndex)
            {
                return true;
            }
        }

        return false;
    }

    public boolean deliverLavaToGeo()
    {
        int i = 3000;
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).distributeLava(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).distributeLava(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).distributeLava(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).distributeLava(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).distributeLava(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityGeoGenerator))
        {
            i = ((TileEntityGeoGenerator)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).distributeLava(i);
        }
        return i < 2980;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerPump(inventoryplayer, this);
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != getActive())
        {
            if(audioSource == null)
            {
                audioSource = AudioManager.createSource(this, PositionSpec.Center, "Machines/PumpOp.ogg", true, false, AudioManager.defaultVolume);
            }
            if(getActive())
            {
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(audioSource != null)
            {
                audioSource.stop();
            }
        }
        super.onNetworkUpdate(s);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public int soundTicker;
    public short pumpCharge;
    private AudioSource audioSource;
}
