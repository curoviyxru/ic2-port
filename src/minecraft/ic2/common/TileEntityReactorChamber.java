// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySource;
import ic2.platform.Platform;
import net.minecraft.src.TileEntity;

// Referenced classes of package ic2.common:
//            EnergyNet, TileEntityNuclearReactor

public class TileEntityReactorChamber extends TileEntity
    implements IEnergySource
{

    public TileEntityReactorChamber()
    {
        addedToEnergyNet = false;
    }

    public void updateEntity()
    {
        if(Platform.isSimulating() && !addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).addTileEntity(this);
            addedToEnergyNet = true;
        }
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public int getMaxEnergyOutput()
    {
        return 240 * TileEntityNuclearReactor.pulsePower();
    }

    public int sendEnergy(int i)
    {
        return EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, i);
    }

    public boolean addedToEnergyNet;
}
