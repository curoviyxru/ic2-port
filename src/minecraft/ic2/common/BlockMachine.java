// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockMultiID, TileEntityIronFurnace, TileEntityElecFurnace, TileEntityMacerator, 
//            TileEntityExtractor, TileEntityCompressor, TileEntityCanner, TileEntityMiner, 
//            TileEntityPump, TileEntityMagnetizer, TileEntityElectrolyzer, TileEntityRecycler, 
//            TileEntityInduction, TileEntityMatter, TileEntityTerra, TileEntityBlock

public class BlockMachine extends BlockMultiID
{

    public BlockMachine(int i)
    {
        super(i, Material.iron);
        setHardness(2.0F);
        setStepSound(soundMetalFootstep);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_machine.png";
    }

    public int idDropped(int i, Random random)
    {
        switch(i)
        {
        default:
            return blockID;
        }
    }

    protected int damageDropped(int i)
    {
        switch(i)
        {
        case 1: // '\001'
            return i;

        case 2: // '\002'
            return i;

        case 9: // '\t'
            return i;

        case 12: // '\f'
            return 12;

        case 13: // '\r'
            return 12;

        case 14: // '\016'
            return 12;

        case 15: // '\017'
            return 12;

        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 10: // '\n'
        case 11: // '\013'
        default:
            return 0;
        }
    }

    public Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        switch(world.getBlockMetadata(i, j, k))
        {
        case 1: // '\001'
            return Integer.valueOf(mod_IC2.guiIdIronFurnace);

        case 2: // '\002'
            return Integer.valueOf(mod_IC2.guiIdElecFurnace);

        case 3: // '\003'
            return Integer.valueOf(mod_IC2.guiIdMacerator);

        case 4: // '\004'
            return Integer.valueOf(mod_IC2.guiIdExtractor);

        case 5: // '\005'
            return Integer.valueOf(mod_IC2.guiIdCompressor);

        case 6: // '\006'
            return Integer.valueOf(mod_IC2.guiIdCanner);

        case 7: // '\007'
            return Integer.valueOf(mod_IC2.guiIdMiner);

        case 8: // '\b'
            return Integer.valueOf(mod_IC2.guiIdPump);

        case 10: // '\n'
            return Integer.valueOf(mod_IC2.guiIdElectrolyzer);

        case 11: // '\013'
            return Integer.valueOf(mod_IC2.guiIdRecycler);

        case 13: // '\r'
            return Integer.valueOf(mod_IC2.guiIdInduction);

        case 14: // '\016'
            return Integer.valueOf(mod_IC2.guiIdMatter);

        case 9: // '\t'
        case 12: // '\f'
        default:
            return null;
        }
    }

    public TileEntityBlock getBlockEntity(int i)
    {
        switch(i)
        {
        case 1: // '\001'
            return new TileEntityIronFurnace();

        case 2: // '\002'
            return new TileEntityElecFurnace();

        case 3: // '\003'
            return new TileEntityMacerator();

        case 4: // '\004'
            return new TileEntityExtractor();

        case 5: // '\005'
            return new TileEntityCompressor();

        case 6: // '\006'
            return new TileEntityCanner();

        case 7: // '\007'
            return new TileEntityMiner();

        case 8: // '\b'
            return new TileEntityPump();

        case 9: // '\t'
            return new TileEntityMagnetizer();

        case 10: // '\n'
            return new TileEntityElectrolyzer();

        case 11: // '\013'
            return new TileEntityRecycler();

        case 13: // '\r'
            return new TileEntityInduction();

        case 14: // '\016'
            return new TileEntityMatter();

        case 15: // '\017'
            return new TileEntityTerra();

        case 12: // '\f'
        default:
            return new TileEntityBlock();
        }
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        if(!Platform.isRendering())
        {
            return;
        }
        int l = world.getBlockMetadata(i, j, k);
        if(l == 1 && isActive(world, i, j, k))
        {
            TileEntity tileentity = world.getBlockTileEntity(i, j, k);
            short word0 = (tileentity instanceof TileEntityBlock) ? ((TileEntityBlock)tileentity).getFacing() : 0;
            float f2 = (float)i + 0.5F;
            float f4 = (float)j + 0.0F + (random.nextFloat() * 6F) / 16F;
            float f5 = (float)k + 0.5F;
            float f7 = 0.52F;
            float f9 = random.nextFloat() * 0.6F - 0.3F;
            switch(word0)
            {
            case 4: // '\004'
                world.spawnParticle("smoke", f2 - f7, f4, f5 + f9, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f2 - f7, f4, f5 + f9, 0.0D, 0.0D, 0.0D);
                break;

            case 5: // '\005'
                world.spawnParticle("smoke", f2 + f7, f4, f5 + f9, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f2 + f7, f4, f5 + f9, 0.0D, 0.0D, 0.0D);
                break;

            case 2: // '\002'
                world.spawnParticle("smoke", f2 + f9, f4, f5 - f7, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f2 + f9, f4, f5 - f7, 0.0D, 0.0D, 0.0D);
                break;

            case 3: // '\003'
                world.spawnParticle("smoke", f2 + f9, f4, f5 + f7, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f2 + f9, f4, f5 + f7, 0.0D, 0.0D, 0.0D);
                break;
            }
        }
        if(l == 3 && isActive(world, i, j, k))
        {
            float f = (float)i + 1.0F;
            float f1 = (float)j + 1.0F;
            float f3 = (float)k + 1.0F;
            for(int i1 = 0; i1 < 4; i1++)
            {
                float f6 = -0.2F - random.nextFloat() * 0.6F;
                float f8 = -0.1F + random.nextFloat() * 0.2F;
                float f10 = -0.2F - random.nextFloat() * 0.6F;
                world.spawnParticle("smoke", f + f6, f1 + f8, f3 + f10, 0.0D, 0.0D, 0.0D);
            }

        }
    }
}
