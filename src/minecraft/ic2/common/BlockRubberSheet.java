// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex, ItemArmorRubBoots

public class BlockRubberSheet extends BlockTex
{

    public BlockRubberSheet(int i, int j)
    {
        super(i, j, Material.cloth);
        setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        return isBlockSupporter(world, i - 1, j, k) || isBlockSupporter(world, i + 1, j, k) || isBlockSupporter(world, i, j, k - 1) || isBlockSupporter(world, i, j, k + 1);
    }

    public boolean isBlockSupporter(World world, int i, int j, int k)
    {
        return world.isBlockOpaqueCube(i, j, k) || world.getBlockId(i, j, k) == blockID;
    }

    public boolean canSupportWeight(World world, int i, int j, int k)
    {
        if(world.getBlockMetadata(i, j, k) == 1)
        {
            return true;
        }
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        int l = i;
        do
        {
            if(world.isBlockOpaqueCube(l, j, k))
            {
                flag1 = true;
                break;
            }
            if(world.getBlockId(l, j, k) != blockID)
            {
                break;
            }
            if(world.isBlockOpaqueCube(l, j - 1, k))
            {
                flag1 = true;
                break;
            }
            l--;
        } while(true);
        l = i;
        do
        {
            if(world.isBlockOpaqueCube(l, j, k))
            {
                flag = true;
                break;
            }
            if(world.getBlockId(l, j, k) != blockID)
            {
                break;
            }
            if(world.isBlockOpaqueCube(l, j - 1, k))
            {
                flag = true;
                break;
            }
            l++;
        } while(true);
        if(flag && flag1)
        {
            world.setBlockMetadata(i, j, k, 1);
            return true;
        }
        l = k;
        do
        {
            if(world.isBlockOpaqueCube(i, j, l))
            {
                flag3 = true;
                break;
            }
            if(world.getBlockId(i, j, l) != blockID)
            {
                break;
            }
            if(world.isBlockOpaqueCube(i, j - 1, l))
            {
                flag3 = true;
                break;
            }
            l--;
        } while(true);
        l = k;
        do
        {
            if(world.isBlockOpaqueCube(i, j, l))
            {
                flag2 = true;
                break;
            }
            if(world.getBlockId(i, j, l) != blockID)
            {
                break;
            }
            if(world.isBlockOpaqueCube(i, j - 1, l))
            {
                flag2 = true;
                break;
            }
            l++;
        } while(true);
        if(flag2 && flag3)
        {
            world.setBlockMetadata(i, j, k, 1);
            return true;
        } else
        {
            return false;
        }
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(world.getBlockMetadata(i, j, k) == 1)
        {
            world.setBlockMetadataWithNotify(i, j, k, 0);
        }
        if(!canPlaceBlockAt(world, i, j, k))
        {
            dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
            world.setBlockWithNotify(i, j, k, 0);
        }
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        if(world.isBlockNormalCube(i, j - 1, k))
        {
            return;
        }
        if((entity instanceof EntityLiving) && !canSupportWeight(world, i, j, k))
        {
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        if(entity.motionY <= -0.40000000596046448D)
        {
            ItemArmorRubBoots.multiplyFall(entity, 0.0F);
            entity.motionX *= 1.1000000238418579D;
            if(entity instanceof EntityLiving)
            {
                if(mod_IC2.getIsJumpingOfEntityLiving((EntityLiving)entity))
                {
                    entity.motionY *= -1.2999999523162842D;
                } else
                if((entity instanceof EntityPlayer) && ((EntityPlayer)entity).isSneaking())
                {
                    entity.motionY *= -0.10000000149011612D;
                } else
                {
                    entity.motionY *= -0.80000001192092896D;
                }
            } else
            {
                entity.motionY *= -0.80000001192092896D;
            }
            entity.motionZ *= 1.1000000238418579D;
        }
    }
}
