// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.ItemBlockCommon;
import net.minecraft.src.ItemStack;

public class ItemGenerator extends ItemBlockCommon
{

    public ItemGenerator(int i)
    {
        super(i);
        setMaxDamage(0);
        setHasSubtypes(true);
    }

    public int getPlacedBlockMetadata(int i)
    {
        return i;
    }

    public String getItemNameIS(ItemStack itemstack)
    {
        int i = itemstack.getItemDamage();
        switch(i)
        {
        case 0: // '\0'
            return "blockGenerator";

        case 1: // '\001'
            return "blockGeoGenerator";

        case 2: // '\002'
            return "blockWaterGenerator";

        case 3: // '\003'
            return "blockSolarGenerator";

        case 4: // '\004'
            return "blockWindGenerator";

        case 5: // '\005'
            return "blockNuclearReactor";
        }
        return null;
    }
}
