// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.src.*;

public class ItemArmorCFPack extends ItemArmor
    implements ITextureProvider
{

    public ItemArmorCFPack(int i, int j, int k)
    {
        super(i, 0, k, 1);
        iconIndex = j;
        setMaxDamage(130);
    }

    public boolean getCFPellet(EntityPlayer entityplayer, ItemStack itemstack)
    {
        if(itemstack.getItemDamage() < itemstack.getMaxDamage() - 1)
        {
            itemstack.setItemDamage(itemstack.getItemDamage() + 1);
            return true;
        } else
        {
            return false;
        }
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }
}
