// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.lang.reflect.Field;

public class RH
{

    public RH()
    {
    }

    public static Object getField(Object obj, String s)
    {
        try
        {
            Field field = obj.getClass().getDeclaredField(s);
            field.setAccessible(true);
            return field.getType().cast(field.get(obj));
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        return null;
    }

    public static void setField(Object obj, String s, Object obj1)
    {
        try
        {
            Field field = obj.getClass().getDeclaredField(s);
            field.setAccessible(true);
            field.set(obj, obj1);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
