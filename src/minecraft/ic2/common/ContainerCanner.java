// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityCanner

public class ContainerCanner extends ContainerIC2
{

    public ContainerCanner(InventoryPlayer inventoryplayer, TileEntityCanner tileentitycanner)
    {
        progress = 0;
        energy = 0;
        tileentity = tileentitycanner;
        addSlot(new Slot(tileentitycanner, 0, 69, 17));
        addSlot(new Slot(tileentitycanner, 1, 30, 45));
        addSlot(new SlotFurnace(inventoryplayer.player, tileentitycanner, 2, 119, 35));
        addSlot(new Slot(tileentitycanner, 3, 69, 53));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(progress != tileentity.progress)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.progress);
            }
            if(energy != tileentity.energy)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.energy & 0xffff);
                icrafting.updateCraftingInventoryInfo(this, 2, tileentity.energy >>> 16);
            }
        }

        progress = tileentity.progress;
        energy = tileentity.energy;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.progress = (short)j;
            break;

        case 1: // '\001'
            tileentity.energy = tileentity.energy & 0xffff0000 | j;
            break;

        case 2: // '\002'
            tileentity.energy = tileentity.energy & 0xffff | j << 16;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 4;
    }

    public int getInput()
    {
        return firstEmptyFrom(0, 2, tileentity);
    }

    public TileEntityCanner tileentity;
    public short progress;
    public int energy;
}
