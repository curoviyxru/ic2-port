// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.ItemBlockCommon;
import net.minecraft.src.ItemStack;

public class ItemMachine extends ItemBlockCommon
{

    public ItemMachine(int i)
    {
        super(i);
        setMaxDamage(0);
        setHasSubtypes(true);
    }

    public int getPlacedBlockMetadata(int i)
    {
        return i;
    }

    public String getItemNameIS(ItemStack itemstack)
    {
        int i = itemstack.getItemDamage();
        switch(i)
        {
        case 0: // '\0'
            return "blockMachine";

        case 1: // '\001'
            return "blockIronFurnace";

        case 2: // '\002'
            return "blockElecFurnace";

        case 3: // '\003'
            return "blockMacerator";

        case 4: // '\004'
            return "blockExtractor";

        case 5: // '\005'
            return "blockCompressor";

        case 6: // '\006'
            return "blockCanner";

        case 7: // '\007'
            return "blockMiner";

        case 8: // '\b'
            return "blockPump";

        case 9: // '\t'
            return "blockMagnetizer";

        case 10: // '\n'
            return "blockElectrolyzer";

        case 11: // '\013'
            return "blockRecycler";

        case 12: // '\f'
            return "blockAdvMachine";

        case 13: // '\r'
            return "blockInduction";

        case 14: // '\016'
            return "blockMatter";

        case 15: // '\017'
            return "blockTerra";
        }
        return null;
    }
}
