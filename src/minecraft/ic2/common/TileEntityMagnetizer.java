// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySink;
import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, EnergyNet

public class TileEntityMagnetizer extends TileEntityBlock
    implements IEnergySink
{

    public TileEntityMagnetizer()
    {
        energy = 0;
        ticker = 0;
        maxEnergy = 100;
        addedToEnergyNet = false;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getShort("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("energy", (short)energy);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            if(ticker-- > 0)
            {
                return;
            }
            boolean flag = false;
            for(int i = yCoord - 1; i > 0 && i >= yCoord - 20 && energy > 0 && worldObj.getBlockId(xCoord, i, zCoord) == mod_IC2.blockFenceIron.blockID; i--)
            {
                int k = 15 - worldObj.getBlockMetadata(xCoord, i, zCoord);
                if(k <= 0)
                {
                    continue;
                }
                flag = true;
                if(k > energy)
                {
                    energy = k;
                }
                worldObj.setBlockMetadata(xCoord, i, zCoord, worldObj.getBlockMetadata(xCoord, i, zCoord) + k);
                energy -= k;
            }

            for(int j = yCoord + 1; j < 128 && j <= yCoord + 20 && energy > 0 && worldObj.getBlockId(xCoord, j, zCoord) == mod_IC2.blockFenceIron.blockID; j++)
            {
                int l = 15 - worldObj.getBlockMetadata(xCoord, j, zCoord);
                if(l <= 0)
                {
                    continue;
                }
                flag = true;
                if(l > energy)
                {
                    energy = l;
                }
                worldObj.setBlockMetadata(xCoord, j, zCoord, worldObj.getBlockMetadata(xCoord, j, zCoord) + l);
                energy -= l;
            }

            if(!flag)
            {
                ticker = 10;
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public boolean demandsEnergy()
    {
        return energy < maxEnergy;
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 32)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        energy += i;
        int j = 0;
        if(energy > maxEnergy)
        {
            j = energy - maxEnergy;
            energy = maxEnergy;
        }
        return j;
    }

    public int energy;
    public int ticker;
    public int maxEnergy;
    public boolean addedToEnergyNet;
}
