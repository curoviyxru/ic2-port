// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySink;
import ic2.platform.Platform;
import java.util.List;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, EnergyNet

public class TileEntityTesla extends TileEntityBlock
    implements IEnergySink
{

    public TileEntityTesla()
    {
        energy = 0;
        ticker = 0;
        maxEnergy = 10000;
        addedToEnergyNet = false;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getShort("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("energy", (short)energy);
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            if(energy < getCost() || !Platform.isSimulating() || !redstoned())
            {
                return;
            }
            int i = energy / getCost();
            energy--;
            if(ticker++ % 32 == 0 && shock(i))
            {
                energy = 0;
            }
        }
    }

    public boolean shock(int i)
    {
        List list = worldObj.getEntitiesWithinAABB(net.minecraft.src.EntityLiving.class, AxisAlignedBB.getBoundingBox(xCoord - 4, yCoord - 4, zCoord - 4, xCoord + 5, yCoord + 5, zCoord + 5));
        for(int j = 0; j < list.size(); j++)
        {
            EntityLiving entityliving = (EntityLiving)list.get(j);
            entityliving.attackEntityFrom(null, i);
            for(int k = 0; k < i; k++)
            {
                worldObj.spawnParticle("reddust", entityliving.posX + (double)worldObj.rand.nextFloat(), entityliving.posY + (double)(worldObj.rand.nextFloat() * 2.0F), entityliving.posZ + (double)worldObj.rand.nextFloat(), 0.0D, 0.0D, 1.0D);
            }

        }

        return list.size() > 0;
    }

    public boolean redstoned()
    {
        return worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord) || worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
    }

    public static int getCost()
    {
        return 400;
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public boolean demandsEnergy()
    {
        return energy < maxEnergy;
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 128)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        energy += i;
        int j = 0;
        if(energy > maxEnergy)
        {
            j = energy - maxEnergy;
            energy = maxEnergy;
        }
        return j;
    }

    public int energy;
    public int ticker;
    public int maxEnergy;
    public boolean addedToEnergyNet;
}
