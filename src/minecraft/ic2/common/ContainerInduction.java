// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityInduction

public class ContainerInduction extends ContainerIC2
{

    public ContainerInduction(InventoryPlayer inventoryplayer, TileEntityInduction tileentityinduction)
    {
        progress = 0;
        energy = 0;
        heat = 0;
        tileentity = tileentityinduction;
        addSlot(new Slot(tileentityinduction, 0, 47, 17));
        addSlot(new Slot(tileentityinduction, 1, 63, 17));
        addSlot(new Slot(tileentityinduction, 2, 56, 53));
        addSlot(new SlotFurnace(inventoryplayer.player, tileentityinduction, 3, 113, 35));
        addSlot(new SlotFurnace(inventoryplayer.player, tileentityinduction, 4, 131, 35));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(progress != tileentity.progress)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.progress);
            }
            if(energy != tileentity.energy)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.energy & 0xffff);
                icrafting.updateCraftingInventoryInfo(this, 2, tileentity.energy >>> 16);
            }
            if(heat != tileentity.heat)
            {
                icrafting.updateCraftingInventoryInfo(this, 3, tileentity.heat);
            }
        }

        progress = tileentity.progress;
        energy = tileentity.energy;
        heat = tileentity.heat;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.progress = (short)j;
            break;

        case 1: // '\001'
            tileentity.energy = tileentity.energy & 0xffff0000 | j;
            break;

        case 2: // '\002'
            tileentity.energy = tileentity.energy & 0xffff | j << 16;
            break;

        case 3: // '\003'
            tileentity.heat = (short)j;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 5;
    }

    public int getInput()
    {
        return tileentity.getStackInSlot(0) == null ? 0 : 1;
    }

    public TileEntityInduction tileentity;
    public short progress;
    public int energy;
    public short heat;
}
