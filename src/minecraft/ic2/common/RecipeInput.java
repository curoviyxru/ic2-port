// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;


public class RecipeInput
{

    public RecipeInput(int i, int j)
    {
        itemId = i;
        itemDamage = j;
    }

    public boolean equals(Object obj)
    {
        if(obj instanceof RecipeInput)
        {
            RecipeInput recipeinput = (RecipeInput)obj;
            return recipeinput.itemId == itemId && recipeinput.itemDamage == itemDamage;
        } else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return itemId * 31 ^ itemDamage;
    }

    private int itemId;
    private int itemDamage;
}
