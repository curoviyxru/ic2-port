// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.BlockContainerCommon;
import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityNuclearReactor, TileEntityReactorChamber

public class BlockReactorChamber extends BlockContainerCommon
    implements ITextureProvider
{

    public BlockReactorChamber(int i)
    {
        super(i, 67, Material.iron);
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        if(i == 0)
        {
            return 16;
        }
        return i != 1 ? 67 : 17;
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(!canPlaceBlockAt(world, i, j, k))
        {
            dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
            world.setBlockWithNotify(i, j, k, 0);
        }
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        int l = 0;
        if(world.getBlockTileEntity(i + 1, j, k) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        if(world.getBlockTileEntity(i - 1, j, k) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        if(world.getBlockTileEntity(i, j + 1, k) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        if(world.getBlockTileEntity(i, j - 1, k) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        if(world.getBlockTileEntity(i, j, k + 1) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        if(world.getBlockTileEntity(i, j, k - 1) instanceof TileEntityNuclearReactor)
        {
            l++;
        }
        return l == 1;
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        TileEntityNuclearReactor tileentitynuclearreactor = getReactorEntity(world, i, j, k);
        if(tileentitynuclearreactor == null)
        {
            onNeighborBlockChange(world, i, j, k, blockID);
            return;
        }
        int l = tileentitynuclearreactor.heat / 1000;
        if(l <= 0)
        {
            return;
        }
        l = world.rand.nextInt(l);
        for(int i1 = 0; i1 < l; i1++)
        {
            world.spawnParticle("smoke", (float)i + random.nextFloat(), (float)j + 0.95F, (float)k + random.nextFloat(), 0.0D, 0.0D, 0.0D);
        }

        l -= world.rand.nextInt(4) + 3;
        for(int j1 = 0; j1 < l; j1++)
        {
            world.spawnParticle("flame", (float)i + random.nextFloat(), (float)j + 1.0F, (float)k + random.nextFloat(), 0.0D, 0.0D, 0.0D);
        }

    }

    public TileEntityNuclearReactor getReactorEntity(World world, int i, int j, int k)
    {
        if(world.getBlockTileEntity(i + 1, j, k) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i + 1, j, k);
        }
        if(world.getBlockTileEntity(i - 1, j, k) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i - 1, j, k);
        }
        if(world.getBlockTileEntity(i, j + 1, k) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i, j + 1, k);
        }
        if(world.getBlockTileEntity(i, j - 1, k) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i, j - 1, k);
        }
        if(world.getBlockTileEntity(i, j, k + 1) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i, j, k + 1);
        }
        if(world.getBlockTileEntity(i, j, k - 1) instanceof TileEntityNuclearReactor)
        {
            return (TileEntityNuclearReactor)world.getBlockTileEntity(i, j, k - 1);
        } else
        {
            return null;
        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.isSneaking())
        {
            return false;
        }
        TileEntityNuclearReactor tileentitynuclearreactor = getReactorEntity(world, i, j, k);
        if(tileentitynuclearreactor == null)
        {
            onNeighborBlockChange(world, i, j, k, blockID);
            return false;
        }
        if(!Platform.isSimulating())
        {
            return true;
        }
        switch(tileentitynuclearreactor.getReactorSize())
        {
        case 3: // '\003'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x3));
            break;

        case 4: // '\004'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x4));
            break;

        case 5: // '\005'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x5));
            break;

        case 6: // '\006'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x6));
            break;

        case 7: // '\007'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x7));
            break;

        case 8: // '\b'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x8));
            break;

        case 9: // '\t'
            Platform.launchGUI(entityplayer, tileentitynuclearreactor, Integer.valueOf(mod_IC2.guiIdNuclearReactor6x9));
            break;
        }
        return true;
    }

    public TileEntity getBlockEntity()
    {
        return new TileEntityReactorChamber();
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }
}
