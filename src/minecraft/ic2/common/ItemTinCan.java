// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2

public class ItemTinCan extends ItemIC2
{

    public ItemTinCan(int i, int j)
    {
        super(i, j);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        entityplayer.heal(2);
        itemstack.stackSize--;
        ItemStack aitemstack[] = entityplayer.inventory.mainInventory;
        int i = -1;
        for(int j = 0; j < aitemstack.length; j++)
        {
            if(i == -1 && aitemstack[j] == null)
            {
                i = j;
            }
            if(aitemstack[j] != null && aitemstack[j].itemID == mod_IC2.itemTinCan.shiftedIndex && aitemstack[j].stackSize < aitemstack[j].getMaxStackSize())
            {
                aitemstack[j].stackSize++;
                return itemstack;
            }
        }

        if(i == -1)
        {
            entityplayer.dropPlayerItem(new ItemStack(mod_IC2.itemTinCan));
        } else
        {
            aitemstack[i] = new ItemStack(mod_IC2.itemTinCan);
            return itemstack;
        }
        return itemstack;
    }
}
