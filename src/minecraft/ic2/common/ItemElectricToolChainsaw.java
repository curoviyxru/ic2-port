// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.*;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemElectricTool, IHitSoundOverride, PositionSpec

public class ItemElectricToolChainsaw extends ItemElectricTool
    implements IHitSoundOverride
{

    public ItemElectricToolChainsaw(int i, int j)
    {
        super(i, j, EnumToolMaterial.IRON, mineableBlocks, 1, 50, 100);
        setMaxDamage(202);
        efficiencyOnProperMaterial = 12F;
        damageVsEntity = 10;
    }

    public boolean onBlockDestroyed(ItemStack itemstack, int i, int j, int k, int l, EntityLiving entityliving)
    {
        use(itemstack, 1, (EntityPlayer)entityliving);
        return true;
    }

    public boolean hitEntity(ItemStack itemstack, EntityLiving entityliving, EntityLiving entityliving1)
    {
        use(itemstack, 2, (EntityPlayer)entityliving1);
        return true;
    }

    public boolean canHarvestBlock(Block block)
    {
        if(block.blockMaterial == Material.wood)
        {
            return true;
        } else
        {
            return super.canHarvestBlock(block);
        }
    }

    public void onUpdate(ItemStack itemstack, World world, Entity entity, int i, boolean flag)
    {
        boolean flag1 = flag && (entity instanceof EntityLiving);
        if(Platform.isRendering())
        {
            if(flag1 && !wasEquipped)
            {
                if(audioSource == null)
                {
                    audioSource = AudioManager.createSource(entity, PositionSpec.Hand, "Tools/Chainsaw/ChainsawIdle.ogg", true, false, AudioManager.defaultVolume);
                }
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(!flag1 && audioSource != null)
            {
                audioSource.stop();
                audioSource.remove();
                audioSource = null;
                if(entity instanceof EntityLiving)
                {
                    AudioManager.playOnce(entity, PositionSpec.Hand, "Tools/Chainsaw/ChainsawStop.ogg", true, AudioManager.defaultVolume);
                }
            } else
            if(audioSource != null)
            {
                audioSource.updatePosition();
            }
            wasEquipped = flag1;
        }
    }

    public String getHitSoundForBlock(int i, int j, int k)
    {
        String as[] = {
            "Tools/Chainsaw/ChainsawUseOne.ogg", "Tools/Chainsaw/ChainsawUseTwo.ogg"
        };
        return as[itemRand.nextInt(as.length)];
    }

    public static Block mineableBlocks[];
    public static boolean wasEquipped = false;
    public static AudioSource audioSource;

    static 
    {
        mineableBlocks = (new Block[] {
            Block.planks, Block.bookShelf, Block.wood, Block.chest, Block.leaves, mod_IC2.blockRubLeaves, Block.web
        });
    }
}
