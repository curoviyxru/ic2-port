// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.IUseItemFirst;
import ic2.api.*;
import ic2.platform.Platform;
import java.text.DecimalFormat;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, ItemData, EnergyNet

public class ItemToolMeter extends ItemIC2
    implements IUseItemFirst
{
    static class ObjData
    {

        int lastMeasuredTileEntityX;
        int lastMeasuredTileEntityY;
        int lastMeasuredTileEntityZ;
        long lastTotalEnergyConducted;
        long lastMeasureTime;

        ObjData()
        {
        }
    }


    public ItemToolMeter(int i, int j)
    {
        super(i, j);
        maxStackSize = 1;
        setMaxDamage(0);
    }

    public boolean onItemUseFirst(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if((tileentity instanceof IEnergySource) || (tileentity instanceof IEnergyConductor) || (tileentity instanceof IEnergySink))
        {
            if(Platform.isSimulating())
            {
                if(itemstack.getItemDamage() == 0)
                {
                    itemstack.setItemDamage(ItemData.getUnusedDamageValue(itemstack.itemID, ic2.common.ItemToolMeter.ObjData.class));
                }
                ObjData objdata = (ObjData)ItemData.get(itemstack, ic2.common.ItemToolMeter.ObjData.class);
                long l1 = EnergyNet.getForWorld(world).getTotalEnergyConducted(tileentity);
                long l2 = world.getWorldTime();
                if(objdata.lastMeasuredTileEntityX != i || objdata.lastMeasuredTileEntityY != j || objdata.lastMeasuredTileEntityZ != k)
                {
                    objdata.lastMeasuredTileEntityX = i;
                    objdata.lastMeasuredTileEntityY = j;
                    objdata.lastMeasuredTileEntityZ = k;
                    Platform.messagePlayer(entityplayer, "Starting new measurement");
                } else
                {
                    long l3 = l2 - objdata.lastMeasureTime;
                    if(l3 < 1L)
                    {
                        l3 = 1L;
                    }
                    double d = (double)(l1 - objdata.lastTotalEnergyConducted) / (double)l3;
                    DecimalFormat decimalformat = new DecimalFormat("0.##");
                    Platform.messagePlayer(entityplayer, (new StringBuilder()).append("Measured power: ").append(decimalformat.format(d)).append(" EU/t (avg. over ").append(l3).append(" ticks)").toString());
                }
                objdata.lastTotalEnergyConducted = l1;
                objdata.lastMeasureTime = l2;
            }
            return true;
        } else
        {
            return false;
        }
    }
}
