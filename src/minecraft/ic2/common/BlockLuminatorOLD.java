// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.BlockContainerCommon;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityLuminatorOLD, TileEntityLuminator

public class BlockLuminatorOLD extends BlockContainerCommon
    implements ITextureProvider
{

    public BlockLuminatorOLD(int i, int j)
    {
        super(i, j, Material.glass);
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public int getRenderBlockPass()
    {
        return 0;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public float getBlockBrightness(IBlockAccess iblockaccess, int i, int j, int k)
    {
        TileEntityLuminatorOLD tileentityluminatorold = (TileEntityLuminatorOLD)iblockaccess.getBlockTileEntity(i, j, k);
        if(tileentityluminatorold == null)
        {
            return super.getBlockBrightness(iblockaccess, i, j, k);
        } else
        {
            return tileentityluminatorold.getLightLevel();
        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        TileEntityLuminatorOLD tileentityluminatorold = (TileEntityLuminatorOLD)world.getBlockTileEntity(i, j, k);
        tileentityluminatorold.switchStrength();
        return true;
    }

    public TileEntity getBlockEntity()
    {
        return new TileEntityLuminator();
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }
}
