// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IHasGuiContainer, TileEntityElectricBlock, ContainerElectrolyzer

public class TileEntityElectrolyzer extends TileEntityMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityElectrolyzer()
    {
        super(2);
        energy = 0;
        mfe = null;
        ticker = randomizer.nextInt(16);
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getShort("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("energy", energy);
    }

    public String getInvName()
    {
        return "Electrolyzer";
    }

    public void updateEntity()
    {
        super.updateEntity();
        boolean flag = false;
        boolean flag1 = false;
        if(ticker++ % 16 == 0)
        {
            mfe = lookForMFE();
        }
        if(mfe == null)
        {
            return;
        }
        if(ticker++ % 32 == 0)
        {
            worldObj.playSoundEffect(xCoord, yCoord, zCoord, "machineElectrolyzerLoop", 1.0F, 1.0F);
        }
        if(shouldDrain() && canDrain())
        {
            flag = drain();
            flag1 = true;
        }
        if(shouldPower() && (canPower() || energy > 0))
        {
            flag = power();
            flag1 = true;
        }
        if(getActive() != flag1)
        {
            setActive(flag1);
            flag = true;
        }
        if(flag)
        {
            onInventoryChanged();
        }
    }

    public boolean shouldDrain()
    {
        return mfe != null && (double)mfe.energy / (double)mfe.maxStorage >= 0.69999999999999996D;
    }

    public boolean shouldPower()
    {
        return mfe != null && (double)mfe.energy / (double)mfe.maxStorage <= 0.29999999999999999D;
    }

    public boolean canDrain()
    {
        return inventory[0] != null && inventory[0].itemID == mod_IC2.itemCellWater.shiftedIndex && (inventory[1] == null || inventory[1].itemID == mod_IC2.itemCellWaterElectro.shiftedIndex && inventory[1].stackSize < inventory[1].getMaxStackSize());
    }

    public boolean canPower()
    {
        return (inventory[0] == null || inventory[0].itemID == mod_IC2.itemCellWater.shiftedIndex && inventory[0].stackSize < inventory[0].getMaxStackSize()) && inventory[1] != null && inventory[1].itemID == mod_IC2.itemCellWaterElectro.shiftedIndex;
    }

    public boolean drain()
    {
        mfe.energy -= processRate();
        energy += processRate();
        if(energy >= 15000)
        {
            energy -= 15000;
            inventory[0].stackSize--;
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
            if(inventory[1] == null)
            {
                inventory[1] = new ItemStack(mod_IC2.itemCellWaterElectro);
            } else
            {
                inventory[1].stackSize++;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean power()
    {
        if(energy > 0)
        {
            int i = processRate();
            if(i > energy)
            {
                i = energy;
            }
            energy -= i;
            mfe.energy += i;
            return false;
        }
        energy += 13500;
        inventory[1].stackSize--;
        if(inventory[1].stackSize <= 0)
        {
            inventory[1] = null;
        }
        if(inventory[0] == null)
        {
            inventory[0] = new ItemStack(mod_IC2.itemCellWater);
        } else
        {
            inventory[0].stackSize++;
        }
        return true;
    }

    public static int processRate()
    {
        return 10;
    }

    public TileEntityElectricBlock lookForMFE()
    {
        if((worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord);
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord);
        }
        if((worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord);
        }
        if((worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord);
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1);
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityElectricBlock) && ((TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).tier > 1)
        {
            return (TileEntityElectricBlock)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1);
        } else
        {
            return null;
        }
    }

    public int gaugeEnergyScaled(int i)
    {
        if(energy <= 0)
        {
            return 0;
        }
        int j = (energy * i) / 15000;
        if(j > i)
        {
            j = i;
        }
        return j;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectrolyzer(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public static Random randomizer = new Random();
    public short energy;
    public TileEntityElectricBlock mfe;
    public int ticker;

}
