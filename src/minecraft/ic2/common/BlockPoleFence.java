// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockPoleFence extends BlockTex
{

    public BlockPoleFence(int i, int j, Material material)
    {
        super(i, j, material);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    public int getRenderType()
    {
        return mod_IC2.fenceRenderId;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        if(blockMaterial == Material.iron && isPole(world, i, j, k))
        {
            return AxisAlignedBB.getBoundingBoxFromPool((float)i + 0.375F, (float)j, (float)k + 0.375F, (float)i + 0.625F, (float)j + 1.0F, (float)k + 0.625F);
        } else
        {
            return AxisAlignedBB.getBoundingBoxFromPool(i, j, k, i + 1, (float)j + 1.5F, k + 1);
        }
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        if(blockMaterial == Material.iron && isPole(world, i, j, k))
        {
            return AxisAlignedBB.getBoundingBoxFromPool((float)i + 0.375F, (float)j, (float)k + 0.375F, (float)i + 0.625F, (float)j + 1.0F, (float)k + 0.625F);
        } else
        {
            return AxisAlignedBB.getBoundingBoxFromPool(i, j, k, i + 1, j + 1, k + 1);
        }
    }

    public boolean isPole(World world, int i, int j, int k)
    {
        return world.getBlockId(i - 1, j, k) != blockID && world.getBlockId(i + 1, j, k) != blockID && world.getBlockId(i, j, k - 1) != blockID && world.getBlockId(i, j, k + 1) != blockID;
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        if(blockMaterial != Material.iron || !isPole(world, i, j, k) || !(entity instanceof EntityPlayer))
        {
            return;
        }
        boolean flag = world.getBlockMetadata(i, j, k) > 0;
        boolean flag1 = false;
        EntityPlayer entityplayer = (EntityPlayer)entity;
        ItemStack itemstack = entityplayer.inventory.armorInventory[0];
        if(itemstack != null)
        {
            int l = itemstack.itemID;
            if(l == Item.bootsSteel.shiftedIndex || l == Item.bootsGold.shiftedIndex || l == Item.bootsChain.shiftedIndex || l == mod_IC2.itemArmorBronzeBoots.shiftedIndex || l == mod_IC2.itemArmorNanoBoots.shiftedIndex || l == mod_IC2.itemArmorQuantumBoots.shiftedIndex)
            {
                flag1 = true;
            }
        }
        if(!flag || !flag1)
        {
            if(entityplayer.isSneaking())
            {
                if(entityplayer.motionY < -0.25D)
                {
                    entityplayer.motionY *= 0.89999997615814209D;
                } else
                {
                    mod_IC2.setFallDistanceOfEntity(entityplayer, 0.0F);
                }
            }
        } else
        {
            world.setBlockMetadata(i, j, k, world.getBlockMetadata(i, j, k) - 1);
            entityplayer.motionY += 0.075000002980232239D;
            if(entityplayer.motionY > 0.0D)
            {
                entityplayer.motionY *= 1.0299999713897705D;
                mod_IC2.setFallDistanceOfEntity(entityplayer, 0.0F);
            }
            if(entityplayer.isSneaking())
            {
                if(entityplayer.motionY > 0.30000001192092896D)
                {
                    entityplayer.motionY = 0.30000001192092896D;
                }
            } else
            if(entityplayer.motionY > 1.5D)
            {
                entityplayer.motionY = 1.5D;
            }
        }
    }
}
