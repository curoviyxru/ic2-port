// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.api.*;
import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IHasGuiContainer, EnergyNet, IChargeableItem, 
//            ItemBattery, ContainerElectricBlock

public abstract class TileEntityElectricBlock extends TileEntityMachine
    implements IEnergySink, IEnergySource, IHasGuiContainer, ISidedInventory
{

    public TileEntityElectricBlock(int i, int j, int k)
    {
        super(2);
        energy = 0;
        addedToEnergyNet = false;
        tier = i;
        output = j;
        maxStorage = k;
    }

    public String getNameByTier()
    {
        switch(tier)
        {
        case 1: // '\001'
            return "BatBox";

        case 2: // '\002'
            return " MFE";

        case 3: // '\003'
            return "MFSU";
        }
        return null;
    }

    public int gaugeEnergyScaled(int i)
    {
        return (energy * i) / maxStorage;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getInteger("energy");
        if(maxStorage > 0x7fffffff)
        {
            energy *= 10;
        }
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        int i = energy;
        if(maxStorage > 0x7fffffff)
        {
            i /= 10;
        }
        nbttagcompound.setInteger("energy", i);
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            boolean flag = false;
            if(energy > 0 && inventory[0] != null && (Item.itemsList[inventory[0].itemID] instanceof IChargeableItem) && inventory[0].getItemDamage() != 1)
            {
                int i = ((IChargeableItem)Item.itemsList[inventory[0].itemID]).giveEnergyTo(inventory[0], energy, tier, false);
                energy -= i;
                flag = i > 0;
            }
            if(demandsEnergy() && inventory[1] != null)
            {
                if(Item.itemsList[inventory[1].itemID] instanceof ItemBattery)
                {
                    int j = ((ItemBattery)Item.itemsList[inventory[1].itemID]).getEnergyFrom(inventory[1], maxStorage - energy, tier);
                    energy += j;
                    flag = j > 0;
                } else
                {
                    int k = inventory[1].itemID;
                    char c = '\0';
                    if(k == Item.redstone.shiftedIndex)
                    {
                        c = '\u01F4';
                    }
                    if(k == mod_IC2.itemBatSU.shiftedIndex)
                    {
                        c = '\u03E8';
                    }
                    if(c > 0 && c <= maxStorage - energy)
                    {
                        inventory[1].stackSize--;
                        if(inventory[1].stackSize <= 0)
                        {
                            inventory[1] = null;
                        }
                        energy += c;
                    }
                }
            }
            boolean flag1 = worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
            if(energy >= output && !flag1 || energy >= maxStorage)
            {
                energy -= output - EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, output);
            }
            setActive(!flag1);
            if(flag)
            {
                onInventoryChanged();
            }
        }
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return !facingMatchesDirection(direction);
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        return facingMatchesDirection(direction);
    }

    public boolean facingMatchesDirection(Direction direction)
    {
        return direction.toSideValue() == getFacing();
    }

    public int getMaxEnergyOutput()
    {
        return output;
    }

    public boolean demandsEnergy()
    {
        return energy < maxStorage;
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > output)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        int j = i;
        if(energy + i >= maxStorage + output)
        {
            j = (maxStorage + output) - energy - 1;
        }
        energy += j;
        return i - j;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricBlock(inventoryplayer, this);
    }

    public boolean wrenchSetFacing(EntityPlayer entityplayer, int i)
    {
        return getFacing() != i;
    }

    public void setFacing(short word0)
    {
        if(addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
        }
        addedToEnergyNet = false;
        super.setFacing(word0);
        EnergyNet.getForWorld(worldObj).addTileEntity(this);
        addedToEnergyNet = true;
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public int tier;
    public int output;
    public int maxStorage;
    public int energy;
    public boolean addedToEnergyNet;
}
