// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;

// Referenced classes of package ic2.common:
//            ItemIC2, IChargeableItem, ItemElectricTool

public abstract class ElectricItem extends ItemIC2
    implements IChargeableItem
{

    public ElectricItem(int i, int j, int k, int l, int i1)
    {
        super(i, j);
        tier = k;
        ratio = l;
        transfer = i1;
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < tier || itemstack.getItemDamage() == 1)
        {
            return 0;
        }
        int k = (itemstack.getItemDamage() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getItemDamage() - i / ratio);
        return i;
    }

    public boolean use(ItemStack itemstack, int i, EntityPlayer entityplayer)
    {
        ItemElectricTool.chargeFromBatpack(itemstack, entityplayer);
        if(itemstack.getItemDamage() + i > itemstack.getMaxDamage() - 1)
        {
            return false;
        }
        if(Platform.isSimulating())
        {
            itemstack.setItemDamage(itemstack.getItemDamage() + i);
            ItemElectricTool.chargeFromBatpack(itemstack, entityplayer);
        }
        return true;
    }

    public int tier;
    public int ratio;
    public int transfer;
}
