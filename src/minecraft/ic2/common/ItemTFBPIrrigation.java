// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemTFBP, TileEntityTerra, BlockRubSapling

public class ItemTFBPIrrigation extends ItemTFBP
{

    public ItemTFBPIrrigation(int i, int j)
    {
        super(i, j);
    }

    public int getConsume()
    {
        return 3000;
    }

    public int getRange()
    {
        return 60;
    }

    public boolean terraform(World world, int i, int j, int k)
    {
        if(world.rand.nextInt(48000) == 0)
        {
            world.getWorldInfo().setIsRaining(true);
            return true;
        }
        int l = TileEntityTerra.getFirstBlockFrom(world, i, j, k + 10);
        if(l == -1)
        {
            return false;
        }
        if(TileEntityTerra.switchGround(world, Block.sand, Block.dirt, i, l, j, true))
        {
            TileEntityTerra.switchGround(world, Block.sand, Block.dirt, i, l, j, true);
            return true;
        }
        int i1 = world.getBlockId(i, l, j);
        if(i1 == Block.tallGrass.blockID)
        {
            return spreadGrass(world, i + 1, l, j) || spreadGrass(world, i - 1, l, j) || spreadGrass(world, i, l, j + 1) || spreadGrass(world, i, l, j - 1);
        }
        if(i1 == Block.sapling.blockID)
        {
            ((BlockSapling)Block.sapling).growTree(world, i, l, j, world.rand);
            return true;
        }
        if(i1 == mod_IC2.blockRubSapling.blockID)
        {
            ((BlockRubSapling)mod_IC2.blockRubSapling).growTree(world, i, l, j, world.rand);
            return true;
        }
        if(i1 == Block.wood.blockID)
        {
            int j1 = world.getBlockMetadata(i, l, j);
            world.setBlockAndMetadataWithNotify(i, l + 1, j, Block.wood.blockID, j1);
            createLeaves(world, i, l + 2, j, j1);
            createLeaves(world, i + 1, l + 1, j, j1);
            createLeaves(world, i - 1, l + 1, j, j1);
            createLeaves(world, i, l + 1, j + 1, j1);
            createLeaves(world, i, l + 1, j - 1, j1);
            return true;
        }
        if(i1 == Block.crops.blockID)
        {
            world.setBlockMetadataWithNotify(i, l, j, 7);
            return true;
        }
        if(i1 == Block.fire.blockID)
        {
            world.setBlockWithNotify(i, l, j, 0);
            return true;
        } else
        {
            return false;
        }
    }

    public void createLeaves(World world, int i, int j, int k, int l)
    {
        if(world.getBlockId(i, j, k) == 0)
        {
            world.setBlockAndMetadataWithNotify(i, j, k, Block.leaves.blockID, l);
        }
    }

    public boolean spreadGrass(World world, int i, int j, int k)
    {
        if(world.rand.nextBoolean())
        {
            return false;
        }
        j = TileEntityTerra.getFirstBlockFrom(world, i, k, j);
        int l = world.getBlockId(i, j, k);
        if(l == Block.dirt.blockID)
        {
            world.setBlockWithNotify(i, j, k, Block.grass.blockID);
            return true;
        }
        if(l == Block.grass.blockID)
        {
            world.setBlockAndMetadataWithNotify(i, j + 1, k, Block.tallGrass.blockID, 1);
            return true;
        } else
        {
            return false;
        }
    }
}
