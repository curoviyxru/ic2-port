// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemElectricTool, ItemArmorNanoSuit

public class ItemNanoSaber extends ItemElectricTool
{

    public ItemNanoSaber(int i, int j, boolean flag)
    {
        super(i, j, EnumToolMaterial.IRON, new Block[] {
            Block.web
        }, 2, 8, 128);
        soundTicker = 0;
        setMaxDamage(5002);
        setMaxStackSize(1);
        active = flag;
    }

    public float getStrVsBlock(ItemStack itemstack, Block block)
    {
        if(active)
        {
            soundTicker++;
            if(soundTicker % 4 == 0)
            {
                Platform.playSoundSp(getRandomSwingSound(), 1.0F, 1.0F);
            }
            return 4F;
        } else
        {
            return 1.0F;
        }
    }

    public boolean hitEntity(ItemStack itemstack, EntityLiving entityliving, EntityLiving entityliving1)
    {
        if(!active)
        {
            return true;
        }
        if(Platform.isSimulating())
        {
            EntityPlayer entityplayer = null;
            if(entityliving1 instanceof EntityPlayer)
            {
                entityplayer = (EntityPlayer)entityliving1;
            }
            if(entityliving instanceof EntityPlayer)
            {
                EntityPlayer entityplayer1 = (EntityPlayer)entityliving;
                for(int i = 0; i < 4; i++)
                {
                    if(entityplayer1.inventory.armorInventory[i] == null || !(entityplayer1.inventory.armorInventory[i].getItem() instanceof ItemArmorNanoSuit))
                    {
                        continue;
                    }
                    entityplayer1.inventory.armorInventory[i].damageItem(30, entityplayer1);
                    if(entityplayer1.inventory.armorInventory[i].stackSize <= 0)
                    {
                        entityplayer1.inventory.armorInventory[i] = null;
                    }
                    drainSaber(itemstack, 2, entityplayer);
                }

            }
            drainSaber(itemstack, 5, entityplayer);
        }
        if(Platform.isRendering())
        {
            Platform.playSoundSp(getRandomSwingSound(), 1.0F, 1.0F);
        }
        return true;
    }

    public String getRandomSwingSound()
    {
        switch(mod_IC2.random.nextInt(3))
        {
        default:
            return "nanosabreSwing";

        case 1: // '\001'
            return "nanosabreSwingOne";

        case 2: // '\002'
            return "nanosabreSwingTwo";
        }
    }

    public boolean onBlockDestroyed(ItemStack itemstack, int i, int j, int k, int l, EntityLiving entityliving)
    {
        EntityPlayer entityplayer = null;
        if(entityliving instanceof EntityPlayer)
        {
            entityplayer = (EntityPlayer)entityliving;
        }
        if(active)
        {
            drainSaber(itemstack, 10, entityplayer);
        }
        return true;
    }

    public int getDamageVsEntity(Entity entity)
    {
        return !active ? 4 : 20;
    }

    public boolean isFull3D()
    {
        return true;
    }

    public boolean canHarvestBlock(Block block)
    {
        return block.blockID == Block.web.blockID;
    }

    public static void drainSaber(ItemStack itemstack, int i, EntityPlayer entityplayer)
    {
        if(!use(itemstack, i, entityplayer))
        {
            itemstack.itemID = mod_IC2.itemNanoSaberOff.shiftedIndex;
            itemstack.setItemDamage(itemstack.getMaxDamage() - 1);
        }
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(active)
        {
            itemstack.itemID = mod_IC2.itemNanoSaberOff.shiftedIndex;
        } else
        if(itemstack.getItemDamage() < itemstack.getMaxDamage() - 1)
        {
            itemstack.itemID = mod_IC2.itemNanoSaber.shiftedIndex;
            world.playSoundAtEntity(entityplayer, "nanosabrePower", 1.0F, 1.0F);
        }
        return itemstack;
    }

    public static void timedLoss(EntityPlayer entityplayer)
    {
        ticker++;
        if(ticker % 16 == 0)
        {
            ItemStack aitemstack[] = entityplayer.inventory.mainInventory;
            if(ticker % 64 == 0)
            {
                for(int i = 9; i < aitemstack.length; i++)
                {
                    if(aitemstack[i] != null && aitemstack[i].itemID == mod_IC2.itemNanoSaber.shiftedIndex)
                    {
                        drainSaber(aitemstack[i], 64, entityplayer);
                    }
                }

            }
            for(int j = 0; j < 9; j++)
            {
                if(aitemstack[j] != null && aitemstack[j].itemID == mod_IC2.itemNanoSaber.shiftedIndex)
                {
                    drainSaber(aitemstack[j], 16, entityplayer);
                }
            }

        }
    }

    public int getIconFromDamage(int i)
    {
        if(active && shinyrand.nextBoolean())
        {
            return iconIndex + 1;
        } else
        {
            return iconIndex;
        }
    }

    public boolean active;
    public int soundTicker;
    public static int ticker = 0;
    public static Random shinyrand = new Random();

}
