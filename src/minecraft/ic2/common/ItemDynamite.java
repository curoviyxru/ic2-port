// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, EntityDynamite

public class ItemDynamite extends ItemIC2
{

    public ItemDynamite(int i, int j, boolean flag)
    {
        super(i, j);
        sticky = flag;
        setMaxStackSize(16);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(sticky)
        {
            return false;
        }
        if(l == 0)
        {
            j--;
        }
        if(l == 1)
        {
            j++;
        }
        if(l == 2)
        {
            k--;
        }
        if(l == 3)
        {
            k++;
        }
        if(l == 4)
        {
            i--;
        }
        if(l == 5)
        {
            i++;
        }
        int i1 = world.getBlockId(i, j, k);
        if(i1 == 0 && mod_IC2.blockDynamite.canPlaceBlockAt(world, i, j, k))
        {
            world.setBlockWithNotify(i, j, k, mod_IC2.blockDynamite.blockID);
            itemstack.stackSize--;
            return true;
        } else
        {
            return true;
        }
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        itemstack.stackSize--;
        world.playSoundAtEntity(entityplayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
        if(Platform.isSimulating())
        {
            world.entityJoinedWorld(new EntityDynamite(world, entityplayer, sticky));
        }
        return itemstack;
    }

    public boolean sticky;
}
