// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityTradeOMat

public class ContainerTradeOMatOpen extends ContainerIC2
{

    public ContainerTradeOMatOpen(InventoryPlayer inventoryplayer, TileEntityTradeOMat tileentitytradeomat)
    {
        totalTradeCount = 0;
        tileentity = tileentitytradeomat;
        addSlot(new Slot(tileentitytradeomat, 0, 24, 17));
        addSlot(new Slot(tileentitytradeomat, 1, 24, 53));
        addSlot(new Slot(tileentitytradeomat, 2, 80, 17));
        addSlot(new Slot(tileentitytradeomat, 3, 80, 53));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(totalTradeCount != tileentity.totalTradeCount)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.totalTradeCount & 0xffff);
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.totalTradeCount >>> 16);
            }
        }

        totalTradeCount = tileentity.totalTradeCount;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.totalTradeCount = tileentity.totalTradeCount & 0xffff0000 | j;
            break;

        case 1: // '\001'
            tileentity.totalTradeCount = tileentity.totalTradeCount & 0xffff | j << 16;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 4;
    }

    public int getInput()
    {
        return 2;
    }

    public TileEntityTradeOMat tileentity;
    public int totalTradeCount;
}
