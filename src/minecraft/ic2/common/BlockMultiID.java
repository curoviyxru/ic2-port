// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.api.EnergyNet;
import ic2.platform.BlockContainerCommon;
import ic2.platform.Platform;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, StackUtil

public abstract class BlockMultiID extends BlockContainerCommon
    implements ITextureProvider
{

    protected BlockMultiID(int i, Material material)
    {
        super(i, material);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        TileEntity tileentity = iblockaccess.getBlockTileEntity(i, j, k);
        short word0 = (tileentity instanceof TileEntityBlock) ? ((TileEntityBlock)tileentity).getFacing() : 0;
        int i1 = iblockaccess.getBlockMetadata(i, j, k);
        if(isActive(iblockaccess, i, j, k))
        {
            return i1 + (sideAndFacingToSpriteOffset[l][word0] + 6) * 16;
        } else
        {
            return i1 + sideAndFacingToSpriteOffset[l][word0] * 16;
        }
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        return j + sideAndFacingToSpriteOffset[i][3] * 16;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.isSneaking())
        {
            return false;
        }
        Integer integer = getGui(world, i, j, k, entityplayer);
        if(integer == null)
        {
            return false;
        }
        if(!Platform.isSimulating())
        {
            return true;
        } else
        {
            return Platform.launchGUI(entityplayer, world.getBlockTileEntity(i, j, k), integer);
        }
    }

    public abstract Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer);

    public ArrayList getBlockDropped(World world, int i, int j, int k, int l)
    {
        ArrayList arraylist = mod_IC2.getDrop(world, blocksList[blockID], l);
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(tileentity instanceof IInventory)
        {
            IInventory iinventory = (IInventory)tileentity;
            for(int i1 = 0; i1 < iinventory.getSizeInventory(); i1++)
            {
                ItemStack itemstack = iinventory.getStackInSlot(i1);
                if(itemstack != null)
                {
                    arraylist.add(itemstack);
                    iinventory.setInventorySlotContents(i1, null);
                }
            }

        }
        return arraylist;
    }

    public TileEntityBlock getBlockEntity()
    {
        return null;
    }

    public abstract TileEntityBlock getBlockEntity(int i);

    public void onBlockAdded(World world, int i, int j, int k)
    {
        TileEntityBlock tileentityblock = getBlockEntity(world.getBlockMetadata(i, j, k));
        world.setBlockTileEntity(i, j, k, tileentityblock);
    }

    //public void onBlockDestroyedByExplosion(World world, int i, int j, int k) {
        //super.onBlockDestroyedByExplosion(world, i, j, k);
        //EnergyNet.getForWorld(world).removeTileEntity(world.getBlockTileEntity(i, j, k);
        //world.removeBlockTileEntity(i, j, k);  //TODO CLEAN
    //}

    //public void onBlockDestroyedByPlayer(World world, int i, int j, int k, int l) {
        //super.onBlockDestroyedByPlayer(world, i, j, k, l);
        //EnergyNet.getForWorld(world).removeTileEntity(world.getBlockTileEntity(i, j, k))
        //world.removeBlockTileEntity(i, j, k);
    //}

    public void onBlockRemoval(World world, int i, int j, int k)
    {
        boolean flag = true;
        for(Iterator iterator = getBlockDropped(world, i, j, k, world.getBlockMetadata(i, j, k)).iterator(); iterator.hasNext();)
        {
            ItemStack itemstack = (ItemStack)iterator.next();
            if(flag)
            {
                flag = false;
            } else
            {
                StackUtil.dropAsEntity(world, i, j, k, itemstack);
            }
        }
        //EnergyNet.getForWorld(world).removeTileEntity(world.getBlockTileEntity(i, j, k));
        world.removeBlockTileEntity(i, j, k); //TODO CLEAN
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        TileEntityBlock tileentityblock = (TileEntityBlock)world.getBlockTileEntity(i, j, k);
        if(entityliving == null)
        {
            tileentityblock.setFacing((short)2);
        } else
        {
            int l = MathHelper.floor_double((double)((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
            switch(l)
            {
            case 0: // '\0'
                tileentityblock.setFacing((short)2);
                break;

            case 1: // '\001'
                tileentityblock.setFacing((short)5);
                break;

            case 2: // '\002'
                tileentityblock.setFacing((short)3);
                break;

            case 3: // '\003'
                tileentityblock.setFacing((short)4);
                break;
            }
        }
    }

    public static boolean isActive(IBlockAccess iblockaccess, int i, int j, int k)
    {
        TileEntity tileentity = iblockaccess.getBlockTileEntity(i, j, k);
        if(tileentity instanceof TileEntityBlock)
        {
            return ((TileEntityBlock)tileentity).getActive();
        } else
        {
            return false;
        }
    }

    public static final int sideAndFacingToSpriteOffset[][] = {
        {
            3, 5, 0, 0, 0, 0
        }, {
            5, 3, 1, 1, 1, 1
        }, {
            2, 2, 3, 2, 5, 4
        }, {
            1, 0, 4, 3, 2, 5
        }, {
            4, 4, 5, 4, 3, 2
        }, {
            0, 1, 2, 5, 4, 3
        }
    };

}
