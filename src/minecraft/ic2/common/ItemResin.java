// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2

public class ItemResin extends ItemIC2
{

    public ItemResin(int i, int j)
    {
        super(i, j);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(l != 1)
        {
            return false;
        }
        j++;
        if(world.getBlockId(i, j, k) != 0 || !mod_IC2.blockHarz.canPlaceBlockAt(world, i, j, k))
        {
            return false;
        } else
        {
            world.setBlockWithNotify(i, j, k, mod_IC2.blockHarz.blockID);
            itemstack.stackSize--;
            return true;
        }
    }
}
