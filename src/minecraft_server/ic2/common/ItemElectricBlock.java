// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.ItemBlockCommon;
import net.minecraft.src.ItemStack;

public class ItemElectricBlock extends ItemBlockCommon
{

    public ItemElectricBlock(int i)
    {
        super(i);
        setMaxDamage(0);
        setHasSubtypes(true);
    }

    public int getPlacedBlockMetadata(int i)
    {
        return i;
    }

    public String getItemNameIS(ItemStack itemstack)
    {
        int i = itemstack.getItemDamage();
        switch(i)
        {
        case 0: // '\0'
            return "blockBatBox";

        case 1: // '\001'
            return "blockMFE";

        case 2: // '\002'
            return "blockMFSU";

        case 3: // '\003'
            return "blockTransformerLV";

        case 4: // '\004'
            return "blockTransformerMV";

        case 5: // '\005'
            return "blockTransformerHV";
        }
        return null;
    }
}
