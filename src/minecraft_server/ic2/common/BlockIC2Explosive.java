// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex, EntityIC2Explosive

public abstract class BlockIC2Explosive extends BlockTex
{

    public BlockIC2Explosive(int i, int j, boolean flag)
    {
        super(i, j, Material.tnt);
        canExplodeByHand = false;
        canExplodeByHand = flag;
    }

    public int getBlockTextureFromSide(int i)
    {
        if(i == 0)
        {
            return blockIndexInTexture;
        }
        if(i == 1)
        {
            return blockIndexInTexture + 1;
        } else
        {
            return blockIndexInTexture + 2;
        }
    }

    public void onBlockAdded(World world, int i, int j, int k)
    {
        super.onBlockAdded(world, i, j, k);
        if(world.isBlockIndirectlyGettingPowered(i, j, k))
        {
            onBlockDestroyedByPlayer(world, i, j, k, 1);
            world.setBlockWithNotify(i, j, k, 0);
        }
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(l > 0 && Block.blocksList[l].canProvidePower() && world.isBlockIndirectlyGettingPowered(i, j, k))
        {
            onBlockDestroyedByPlayer(world, i, j, k, 1);
            world.setBlockWithNotify(i, j, k, 0);
        }
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public void onBlockDestroyedByExplosion(World world, int i, int j, int k)
    {
        EntityIC2Explosive entityic2explosive = getExplosionEntity(world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F);
        entityic2explosive.fuse = world.rand.nextInt(entityic2explosive.fuse / 4) + entityic2explosive.fuse / 8;
        world.entityJoinedWorld(entityic2explosive);
    }

    public void onBlockDestroyedByPlayer(World world, int i, int j, int k, int l)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        if((l & 1) == 0 && !canExplodeByHand)
        {
            dropBlockAsItem_do(world, i, j, k, new ItemStack(blockID, 1, 0));
        } else
        {
            EntityIC2Explosive entityic2explosive = getExplosionEntity(world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F);
            world.entityJoinedWorld(entityic2explosive);
            world.playSoundAtEntity(entityic2explosive, "random.fuse", 1.0F, 1.0F);
        }
    }

    public void onBlockClicked(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.getCurrentEquippedItem() != null && entityplayer.getCurrentEquippedItem().itemID == Item.flintAndSteel.shiftedIndex)
        {
            world.setBlockMetadata(i, j, k, 1);
        }
        super.onBlockClicked(world, i, j, k, entityplayer);
    }

    public abstract EntityIC2Explosive getExplosionEntity(World world, float f, float f1, float f2);

    public boolean canExplodeByHand;
}
