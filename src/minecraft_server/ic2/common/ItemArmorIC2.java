// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.src.ItemArmor;

public class ItemArmorIC2 extends ItemArmor
    implements ITextureProvider
{

    public ItemArmorIC2(int i, int j, int k, int l, int i1, int j1)
    {
        super(i, k, l, i1);
        setIconIndex(j);
        setMaxDamage(maxDamageArray[i1] * j1);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    private static final int damageReduceAmountArray[] = {
        3, 8, 6, 3
    };
    private static final int maxDamageArray[] = {
        11, 16, 15, 13
    };

}
