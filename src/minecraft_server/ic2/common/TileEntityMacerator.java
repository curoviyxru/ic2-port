// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.HashMap;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElectricMachine, RecipeInput, ContainerElectricMachine

public class TileEntityMacerator extends TileEntityElectricMachine
{

    public TileEntityMacerator()
    {
        super(3, 2, 300, 32);
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return (ItemStack)recipes.get(new RecipeInput(itemstack.itemID, itemstack.getItemDamage()));
    }

    public static void initRecipes()
    {
        addRecipe(new RecipeInput(Block.oreIron.blockID, 0), new ItemStack(mod_IC2.itemDustIron, 2));
        addRecipe(new RecipeInput(Block.oreGold.blockID, 0), new ItemStack(mod_IC2.itemDustGold, 2));
        addRecipe(new RecipeInput(Item.coal.shiftedIndex, 0), new ItemStack(mod_IC2.itemDustCoal));
        addRecipe(new RecipeInput(Item.ingotIron.shiftedIndex, 0), new ItemStack(mod_IC2.itemDustIron));
        addRecipe(new RecipeInput(Item.ingotGold.shiftedIndex, 0), new ItemStack(mod_IC2.itemDustGold));
        addRecipe(new RecipeInput(Block.cloth.blockID, 0), new ItemStack(Item.silk));
        addRecipe(new RecipeInput(Block.gravel.blockID, 0), new ItemStack(Item.flint));
        addRecipe(new RecipeInput(Block.stone.blockID, 0), new ItemStack(Block.cobblestone));
        addRecipe(new RecipeInput(Block.cobblestone.blockID, 0), new ItemStack(Block.sand));
        addRecipe(new RecipeInput(Block.sandStone.blockID, 0), new ItemStack(Block.sand));
        addRecipe(new RecipeInput(Block.ice.blockID, 0), new ItemStack(Item.snowball));
        addRecipe(new RecipeInput(Block.blockClay.blockID, 0), new ItemStack(mod_IC2.itemDustClay, 2));
    }

    public static void addRecipe(RecipeInput recipeinput, ItemStack itemstack)
    {
        recipes.put(recipeinput, itemstack);
    }

    public String getInvName()
    {
        return "Macerator";
    }

    public String getStartSoundFile()
    {
        return "Machines/MaceratorOp.ogg";
    }

    public String getInterruptSoundFile()
    {
        return "Machines/InterruptOne.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricMachine(inventoryplayer, this);
    }

    public float getWrenchDropRate()
    {
        return 0.85F;
    }

    public static HashMap recipes = new HashMap();

}
