// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.*;
import java.util.Random;

import ic2.platform.NetworkManager;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, PositionSpec

public class ItemTreetap extends ItemIC2
{

    public ItemTreetap(int i, int j)
    {
        super(i, j);
        setMaxStackSize(1);
        setMaxDamage(16);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(world.getBlockId(i, j, k) != mod_IC2.blockRubWood.blockID)
        {
            return false;
        }
        int i1 = world.getBlockMetadata(i, j, k);
        if(i1 < 2 || i1 % 6 != l)
        {
            return false;
        }
        if(i1 < 6)
        {
            if(Platform.isSimulating())
            {
                world.setBlockMetadataWithNotify(i, j, k, i1 + 6);
                ejectHarz(world, i, j, k, l, world.rand.nextInt(3) + 1);
                world.scheduleBlockUpdate(i, j, k, mod_IC2.blockRubWood.blockID, mod_IC2.blockRubWood.tickRate());
                NetworkManager.announceBlockUpdate(world, i, j, k);
            }
            if(Platform.isRendering())
            {
                AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/Treetap.ogg", true, AudioManager.defaultVolume);
            }
        } else
        {
            if(world.rand.nextInt(5) == 0 && Platform.isSimulating())
            {
                world.setBlockMetadataWithNotify(i, j, k, 1);
                NetworkManager.announceBlockUpdate(world, i, j, k);
            }
            if(world.rand.nextInt(5) == 0)
            {
                if(Platform.isSimulating())
                {
                    ejectHarz(world, i, j, k, l, 1);
                }
                if(Platform.isRendering())
                {
                    AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/Treetap.ogg", true, AudioManager.defaultVolume);
                }
            }
        }
        if(Platform.isSimulating())
        {
            itemstack.damageItem(1, entityplayer);
        }
        return true;
    }

    public void ejectHarz(World world, int i, int j, int k, int l, int i1)
    {
        double d = (double)i + 0.5D;
        double d1 = (double)j + 0.5D;
        double d2 = (double)k + 0.5D;
        if(l == 2)
        {
            d2 -= 0.29999999999999999D;
        } else
        if(l == 5)
        {
            d += 0.29999999999999999D;
        } else
        if(l == 3)
        {
            d2 += 0.29999999999999999D;
        } else
        if(l == 4)
        {
            d -= 0.29999999999999999D;
        }
        for(int j1 = 0; j1 < i1; j1++)
        {
            EntityItem entityitem = new EntityItem(world, d, d1, d2, new ItemStack(mod_IC2.itemHarz));
            entityitem.delayBeforeCanPickup = 10;
            world.entityJoinedWorld(entityitem);
        }

    }
}
