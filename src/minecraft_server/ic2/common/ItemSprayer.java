// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.ArrayList;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, ItemArmorCFPack

public class ItemSprayer extends ItemIC2
{

    public ItemSprayer(int i, int j)
    {
        super(i, j);
        setMaxStackSize(1);
        setMaxDamage(1602);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        ItemStack itemstack1 = entityplayer.inventory.armorInventory[2];
        boolean flag = itemstack1 != null && itemstack1.itemID == mod_IC2.itemArmorCFPack.shiftedIndex && ((ItemArmorCFPack)itemstack1.getItem()).getCFPellet(entityplayer, itemstack1);
        if(!flag && itemstack.getItemDamage() > 1501)
        {
            return false;
        }
        if(world.getBlockId(i, j, k) == mod_IC2.blockScaffold.blockID)
        {
            sprayFoam(world, i, j, k, calculateDirectionsFromPlayer(entityplayer), true);
            if(!flag)
            {
                itemstack.damageItem(100, null);
            }
            return true;
        }
        if(l == 0)
        {
            j--;
        }
        if(l == 1)
        {
            j++;
        }
        if(l == 2)
        {
            k--;
        }
        if(l == 3)
        {
            k++;
        }
        if(l == 4)
        {
            i--;
        }
        if(l == 5)
        {
            i++;
        }
        int i1 = world.getBlockId(i, j, k);
        if(sprayFoam(world, i, j, k, calculateDirectionsFromPlayer(entityplayer), false))
        {
            if(!flag)
            {
                itemstack.damageItem(100, null);
            }
            return true;
        } else
        {
            return false;
        }
    }

    public static boolean[] calculateDirectionsFromPlayer(EntityPlayer entityplayer)
    {
        float f = entityplayer.rotationYaw % 360F;
        float f1 = entityplayer.rotationPitch;
        boolean aflag[] = {
            true, true, true, true, true, true
        };
        if(f1 >= -65F && f1 <= 65F)
        {
            if(f >= 300F && f <= 360F || f >= 0.0F && f <= 60F)
            {
                aflag[2] = false;
            }
            if(f >= 30F && f <= 150F)
            {
                aflag[5] = false;
            }
            if(f >= 120F && f <= 240F)
            {
                aflag[3] = false;
            }
            if(f >= 210F && f <= 330F)
            {
                aflag[4] = false;
            }
        }
        if(f1 <= -40F)
        {
            aflag[0] = false;
        }
        if(f1 >= 40F)
        {
            aflag[1] = false;
        }
        return aflag;
    }

    public boolean sprayFoam(World world, int i, int j, int k, boolean aflag[], boolean flag)
    {
        if(!flag && !mod_IC2.blockFoam.canPlaceBlockAt(world, i, j, k) || flag && world.getBlockId(i, j, k) != mod_IC2.blockScaffold.blockID)
        {
            return false;
        }
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        int l = getSprayMass();
        arraylist.add(new ChunkPosition(i, j, k));
        for(int i1 = 0; i1 < arraylist.size() && l > 0; i1++)
        {
            ChunkPosition chunkposition = (ChunkPosition)arraylist.get(i1);
            if(!flag && mod_IC2.blockFoam.canPlaceBlockAt(world, chunkposition.x, chunkposition.y, chunkposition.z) || flag && world.getBlockId(chunkposition.x, chunkposition.y, chunkposition.z) == mod_IC2.blockScaffold.blockID)
            {
                considerAddingCoord(chunkposition, arraylist1);
                addAdjacentSpacesOnList(chunkposition.x, chunkposition.y, chunkposition.z, arraylist, aflag, flag);
                l--;
            }
        }

        for(int j1 = 0; j1 < arraylist1.size(); j1++)
        {
            if(world.getBlockId(((ChunkPosition)arraylist1.get(j1)).x, ((ChunkPosition)arraylist1.get(j1)).y, ((ChunkPosition)arraylist1.get(j1)).z) == mod_IC2.blockScaffold.blockID)
            {
                mod_IC2.blockScaffold.dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
            }
            world.setBlockWithNotify(((ChunkPosition)arraylist1.get(j1)).x, ((ChunkPosition)arraylist1.get(j1)).y, ((ChunkPosition)arraylist1.get(j1)).z, mod_IC2.blockFoam.blockID);
        }

        return true;
    }

    public void addAdjacentSpacesOnList(int i, int j, int k, ArrayList arraylist, boolean aflag[], boolean flag)
    {
        int ai[] = generateRngSpread(mod_IC2.random);
        for(int l = 0; l < ai.length; l++)
        {
            if(flag || aflag[ai[l]])
            {
                switch(ai[l])
                {
                case 0: // '\0'
                    considerAddingCoord(new ChunkPosition(i, j - 1, k), arraylist);
                    break;

                case 1: // '\001'
                    considerAddingCoord(new ChunkPosition(i, j + 1, k), arraylist);
                    break;

                case 2: // '\002'
                    considerAddingCoord(new ChunkPosition(i, j, k - 1), arraylist);
                    break;

                case 3: // '\003'
                    considerAddingCoord(new ChunkPosition(i, j, k + 1), arraylist);
                    break;

                case 4: // '\004'
                    considerAddingCoord(new ChunkPosition(i - 1, j, k), arraylist);
                    break;

                case 5: // '\005'
                    considerAddingCoord(new ChunkPosition(i + 1, j, k), arraylist);
                    break;
                }
            }
        }

    }

    public void considerAddingCoord(ChunkPosition chunkposition, ArrayList arraylist)
    {
        for(int i = 0; i < arraylist.size(); i++)
        {
            if(((ChunkPosition)arraylist.get(i)).x == chunkposition.x && ((ChunkPosition)arraylist.get(i)).y == chunkposition.y && ((ChunkPosition)arraylist.get(i)).z == chunkposition.z)
            {
                return;
            }
        }

        arraylist.add(chunkposition);
    }

    public int[] generateRngSpread(Random random)
    {
        int ai[] = {
            0, 1, 2, 3, 4, 5
        };
        for(int i = 0; i < 16; i++)
        {
            int j = random.nextInt(6);
            int k = random.nextInt(6);
            int l = ai[j];
            ai[j] = ai[k];
            ai[k] = l;
        }

        return ai;
    }

    public static int getSprayMass()
    {
        return 13;
    }
}
