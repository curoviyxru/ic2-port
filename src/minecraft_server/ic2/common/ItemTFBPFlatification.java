// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.ArrayList;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemTFBP, TileEntityTerra

public class ItemTFBPFlatification extends ItemTFBP
{

    public ItemTFBPFlatification(int i, int j)
    {
        super(i, j);
    }

    public int getConsume()
    {
        return 4000;
    }

    public int getRange()
    {
        return 40;
    }

    public boolean terraform(World world, int i, int j, int k)
    {
        int l = TileEntityTerra.getFirstBlockFrom(world, i, j, k + 20);
        if(l == -1)
        {
            return false;
        }
        if(world.getBlockId(i, l, j) == Block.snow.blockID)
        {
            l--;
        }
        if(l == k)
        {
            return false;
        }
        if(l < k)
        {
            world.setBlockWithNotify(i, l + 1, j, Block.dirt.blockID);
            return true;
        }
        if(canRemove(world.getBlockId(i, l, j)))
        {
            world.setBlockWithNotify(i, l, j, 0);
            return true;
        } else
        {
            return false;
        }
    }

    public boolean canRemove(int i)
    {
        for(int j = 0; j < removeIDs.size(); j++)
        {
            if(((Integer)removeIDs.get(j)).intValue() == i)
            {
                return true;
            }
        }

        return false;
    }

    public static void loadRemoveables()
    {
        removeIDs.add(Integer.valueOf(Block.snow.blockID));
        removeIDs.add(Integer.valueOf(Block.ice.blockID));
        removeIDs.add(Integer.valueOf(Block.grass.blockID));
        removeIDs.add(Integer.valueOf(Block.stone.blockID));
        removeIDs.add(Integer.valueOf(Block.gravel.blockID));
        removeIDs.add(Integer.valueOf(Block.sand.blockID));
        removeIDs.add(Integer.valueOf(Block.dirt.blockID));
        removeIDs.add(Integer.valueOf(mod_IC2.blockRubLeaves.blockID));
        removeIDs.add(Integer.valueOf(mod_IC2.blockRubSapling.blockID));
        removeIDs.add(Integer.valueOf(mod_IC2.blockRubWood.blockID));
        removeIDs.add(Integer.valueOf(Block.leaves.blockID));
        removeIDs.add(Integer.valueOf(Block.wood.blockID));
        removeIDs.add(Integer.valueOf(Block.tallGrass.blockID));
        removeIDs.add(Integer.valueOf(Block.plantRed.blockID));
        removeIDs.add(Integer.valueOf(Block.plantYellow.blockID));
        removeIDs.add(Integer.valueOf(Block.sapling.blockID));
        removeIDs.add(Integer.valueOf(Block.crops.blockID));
        removeIDs.add(Integer.valueOf(Block.mushroomRed.blockID));
        removeIDs.add(Integer.valueOf(Block.mushroomBrown.blockID));
        removeIDs.add(Integer.valueOf(mod_IC2.blockRubSapling.blockID));
        removeIDs.add(Integer.valueOf(Block.pumpkin.blockID));
    }

    public static ArrayList removeIDs = new ArrayList();

}
