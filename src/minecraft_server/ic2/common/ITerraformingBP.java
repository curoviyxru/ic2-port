// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.World;

public interface ITerraformingBP
{

    public abstract int getConsume();

    public abstract int getRange();

    public abstract boolean terraform(World world, int i, int j, int k);
}
