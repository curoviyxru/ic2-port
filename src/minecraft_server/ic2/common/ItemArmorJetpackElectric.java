// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.src.ItemStack;

// Referenced classes of package ic2.common:
//            ItemArmorJetpack, IChargeableItem

public class ItemArmorJetpackElectric extends ItemArmorJetpack
    implements IChargeableItem
{

    public ItemArmorJetpackElectric(int i, int j, int k)
    {
        super(i, j, k);
        tier = 2;
        ratio = 3;
        transfer = 60;
        setMaxDamage(10002);
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < tier || itemstack.getItemDamage() == 1)
        {
            return 0;
        }
        int k = (itemstack.getItemDamage() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getItemDamage() - i / ratio);
        return i;
    }

    public int tier;
    public int ratio;
    public int transfer;
}
