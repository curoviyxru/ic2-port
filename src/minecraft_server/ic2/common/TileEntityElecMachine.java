// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySink;
import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, EnergyNet, ItemBattery

public abstract class TileEntityElecMachine extends TileEntityMachine
    implements IEnergySink
{

    public TileEntityElecMachine(int i, int j, int k, int l)
    {
        super(i);
        energy = 0;
        addedToEnergyNet = false;
        fuelslot = j;
        maxEnergy = k;
        maxInput = l;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getInteger("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setInteger("energy", energy);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating() && !addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).addTileEntity(this);
            addedToEnergyNet = true;
        }
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public boolean provideEnergy()
    {
        if(inventory[fuelslot] != null)
        {
            int i = inventory[fuelslot].itemID;
            Item item = Item.itemsList[i];
            if(item instanceof ItemBattery)
            {
                energy += ((ItemBattery)item).getEnergyFrom(inventory[fuelslot], maxEnergy - energy, 1);
                return true;
            }
            if(i == Item.redstone.shiftedIndex)
            {
                energy += maxEnergy;
                inventory[fuelslot].stackSize--;
                if(inventory[fuelslot].stackSize <= 0)
                {
                    inventory[fuelslot] = null;
                }
                return true;
            }
            if(i == mod_IC2.itemBatSU.shiftedIndex)
            {
                energy += 1000;
                inventory[fuelslot].stackSize--;
                if(inventory[fuelslot].stackSize <= 0)
                {
                    inventory[fuelslot] = null;
                }
                return true;
            }
        }
        return false;
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean demandsEnergy()
    {
        return energy <= maxEnergy - maxInput;
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > maxInput)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        energy += i;
        int j = 0;
        if(energy > maxEnergy)
        {
            j = energy - maxEnergy;
            energy = maxEnergy;
        }
        return j;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public boolean isRedstonePowered()
    {
        return worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
    }

    public int energy;
    public int fuelslot;
    public int maxEnergy;
    public int maxInput;
    public boolean addedToEnergyNet;
}
