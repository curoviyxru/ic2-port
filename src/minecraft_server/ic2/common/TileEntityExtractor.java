// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.HashMap;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElectricMachine, RecipeInput, ContainerElectricMachine

public class TileEntityExtractor extends TileEntityElectricMachine
{

    public TileEntityExtractor()
    {
        super(3, 2, 300, 32);
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return (ItemStack)recipes.get(new RecipeInput(itemstack.itemID, itemstack.getItemDamage()));
    }

    public static void initRecipes()
    {
        addRecipe(new RecipeInput(mod_IC2.blockRubSapling.blockID, 0), new ItemStack(mod_IC2.itemRubber, 1));
        addRecipe(new RecipeInput(mod_IC2.itemHarz.shiftedIndex, 0), new ItemStack(mod_IC2.itemRubber, 3));
        addRecipe(new RecipeInput(mod_IC2.itemCellBio.shiftedIndex, 0), new ItemStack(mod_IC2.itemCellBioRef));
        addRecipe(new RecipeInput(mod_IC2.itemCellCoal.shiftedIndex, 0), new ItemStack(mod_IC2.itemCellCoalRef));
        addRecipe(new RecipeInput(mod_IC2.itemCellWater.shiftedIndex, 0), new ItemStack(mod_IC2.itemCellCoolant));
    }

    public static void addRecipe(RecipeInput recipeinput, ItemStack itemstack)
    {
        recipes.put(recipeinput, itemstack);
    }

    public String getInvName()
    {
        return "Extractor";
    }

    public String getStartSoundFile()
    {
        return "Machines/ExtractorOp.ogg";
    }

    public String getInterruptSoundFile()
    {
        return "Machines/InterruptOne.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricMachine(inventoryplayer, this);
    }

    public float getWrenchDropRate()
    {
        return 0.85F;
    }

    public static HashMap recipes = new HashMap();

}
