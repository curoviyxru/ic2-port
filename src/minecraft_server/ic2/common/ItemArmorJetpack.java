// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.*;
import java.util.HashMap;
import java.util.Map;

import ic2.platform.NetworkManager;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemArmorBackpack, IItemTickListener, INetworkItemEventListener, ItemArmorRubBoots, 
//            PositionSpec

public class ItemArmorJetpack extends ItemArmorBackpack
    implements IItemTickListener, INetworkItemEventListener
{

    public ItemArmorJetpack(int i, int j, int k)
    {
        super(i, j, k);
        setMaxDamage(18002);
    }

    public static boolean useJetpack(EntityPlayer entityplayer)
    {
        ItemStack itemstack = entityplayer.inventory.armorInventory[2];
        if(itemstack.getItemDamage() + 1 >= itemstack.getMaxDamage())
        {
            return false;
        }
        boolean flag = itemstack.itemID != mod_IC2.itemArmorJetpack.shiftedIndex;
        float f = 1.0F;
        float f1 = 0.2F;
        if(flag)
        {
            f = 0.7F;
            f1 = 0.05F;
        }
        if((float)itemstack.getItemDamage() + (float)(itemstack.getMaxDamage() - 1) * f1 >= (float)(itemstack.getMaxDamage() - 1))
        {
            float f2 = itemstack.getMaxDamage() - 1 - itemstack.getItemDamage();
            f *= f2 / ((float)(itemstack.getMaxDamage() - 1) * f1);
        }
        if(Platform.isKeyDownForward(entityplayer))
        {
            float f3 = 0.15F;
            if(getHoverMode(entityplayer))
            {
                f3 = 0.5F;
            }
            if(flag)
            {
                f3 += 0.15F;
            }
            float f4 = f * f3 * 2.0F;
            if(f4 > 0.0F)
            {
                entityplayer.moveFlying(0.0F, 0.4F * f4, 0.02F);
            }
        }
        int i = flag ? 100 : 128;
        double d = entityplayer.posY;
        if(d > (double)(i - 25))
        {
            if(d > (double)i)
            {
                d = i;
            }
            f = (float)((double)f * (((double)i - d) / 25D));
        }
        double d1 = entityplayer.motionY;
        entityplayer.motionY = Math.min(entityplayer.motionY + (double)(f * 0.2F), 0.60000002384185791D);
        if(getHoverMode(entityplayer))
        {
            float f5 = -0.1F;
            if(flag && getJumping(entityplayer))
            {
                f5 = 0.1F;
            }
            if(entityplayer.motionY > (double)f5)
            {
                entityplayer.motionY = f5;
                if(d1 > entityplayer.motionY)
                {
                    entityplayer.motionY = d1;
                }
            }
        }
        int j = 9;
        if(getHoverMode(entityplayer))
        {
            j = 6;
        }
        if(flag)
        {
            j -= 2;
        }
        itemstack.setItemDamage(Math.min(itemstack.getMaxDamage() - 1, itemstack.getItemDamage() + j));
        if(entityplayer.motionY > -0.34999999403953552D)
        {
            ItemArmorRubBoots.multiplyFall(entityplayer, 0.0F);
        }
        entityplayer.distanceWalkedModified = 0.0F;
        Platform.resetPlayerInAirTime(entityplayer);
        return true;
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public void onTick(EntityPlayer entityplayer, ItemStack itemstack)
    {
        boolean flag = false;
        if(getJumping(entityplayer) || getHoverMode(entityplayer) && entityplayer.motionY < -0.34999999403953552D)
        {
            flag = useJetpack(entityplayer);
        }
        if(Platform.isRendering())
        {
            boolean flag1 = mod_IC2.getPlayerIsJumping();
            if(lastJumping != flag1)
            {
                NetworkManager.initiateClientItemEvent(itemstack, flag1 ? 0 : 1);
                if(!Platform.isSimulating())
                {
                    onNetworkEvent(itemstack.getItemDamage(), entityplayer, flag1 ? 0 : 1);
                }
                lastJumping = flag1;
            }
            if(flag1 && Platform.isKeyDownJetpackHover(entityplayer) && toggleTimer == 0)
            {
                toggleTimer = 10;
                currentHoverMode = !currentHoverMode;
                NetworkManager.initiateClientItemEvent(itemstack, currentHoverMode ? 2 : 3);
                if(!Platform.isSimulating())
                {
                    onNetworkEvent(itemstack.getItemDamage(), entityplayer, currentHoverMode ? 2 : 3);
                }
            }
            if(toggleTimer > 0)
            {
                toggleTimer--;
            }
            if(lastJetpackUsed != flag)
            {
                if(flag)
                {
                    if(audioSource == null)
                    {
                        audioSource = AudioManager.createSource(entityplayer, PositionSpec.Backpack, "Tools/Jetpack/JetpackLoop.ogg", true, false, AudioManager.defaultVolume);
                    }
                    if(audioSource != null)
                    {
                        audioSource.play();
                    }
                } else
                if(audioSource != null)
                {
                    audioSource.remove();
                    audioSource = null;
                }
                lastJetpackUsed = flag;
            }
            if(audioSource != null)
            {
                audioSource.updatePosition();
            }
        }
    }

    public void onNetworkEvent(int i, EntityPlayer entityplayer, int j)
    {
        switch(j)
        {
        default:
            break;

        case 0: // '\0'
            playerIsjumping.put(entityplayer, Boolean.valueOf(true));
            break;

        case 1: // '\001'
            playerIsjumping.put(entityplayer, Boolean.valueOf(false));
            break;

        case 2: // '\002'
            hoverMode.put(entityplayer, Boolean.valueOf(true));
            if(Platform.isSimulating())
            {
                Platform.messagePlayer(entityplayer, "Hover Mode enabled.");
            }
            break;

        case 3: // '\003'
            hoverMode.put(entityplayer, Boolean.valueOf(false));
            if(Platform.isSimulating())
            {
                Platform.messagePlayer(entityplayer, "Hover Mode disabled.");
            }
            break;
        }
    }

    private static boolean getJumping(EntityPlayer entityplayer)
    {
        if(!playerIsjumping.containsKey(entityplayer))
        {
            return false;
        } else
        {
            return ((Boolean)playerIsjumping.get(entityplayer)).booleanValue();
        }
    }

    private static boolean getHoverMode(EntityPlayer entityplayer)
    {
        if(!hoverMode.containsKey(entityplayer))
        {
            return false;
        } else
        {
            return ((Boolean)hoverMode.get(entityplayer)).booleanValue();
        }
    }

    public static Map playerIsjumping = new HashMap();
    public static Map hoverMode = new HashMap();
    public static AudioSource audioSource;
    private static int toggleTimer = 0;
    private static boolean lastJetpackUsed = false;
    private static boolean lastJumping = false;
    private static boolean currentHoverMode = false;
    private static final int EventJumpingStart = 0;
    private static final int EventJumpingStop = 1;
    private static final int EventHoverModeEnable = 2;
    private static final int EventHoverModeDisable = 3;

}
