// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, IChargeableItem

public class ItemBattery extends ItemIC2
    implements IChargeableItem
{

    public ItemBattery(int i, int j, int k, int l, boolean flag, int i1)
    {
        super(i, j);
        ratio = k;
        transfer = l;
        rechargeable = flag;
        tier = i1;
        setMaxDamage(10002);
        setMaxStackSize(1);
    }

    public int getEnergyFrom(ItemStack itemstack, int i, int j)
    {
        if(j < tier || itemstack.getItemDamage() == 10001)
        {
            return 0;
        }
        int k = (itemstack.getMaxDamage() - 1 - itemstack.getItemDamage()) * ratio;
        if(i > k)
        {
            i = k;
        }
        if(transfer != 0 && transfer < k)
        {
            k = transfer;
        }
        for(; k % ratio != 0; k--) { }
        itemstack.damageItem(k / ratio, null);
        if(itemstack.itemID == mod_IC2.itemBatRE.shiftedIndex && itemstack.getItemDamage() == 10001)
        {
            itemstack.itemID = mod_IC2.itemBatREDischarged.shiftedIndex;
            itemstack.setItemDamage(0);
        }
        return k;
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(!rechargeable || j < tier || itemstack.getItemDamage() == 1)
        {
            return 0;
        }
        int k = (itemstack.getItemDamage() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getItemDamage() - i / ratio);
        return i;
    }

    public int getIconFromDamage(int i)
    {
        if(i <= 1)
        {
            return iconIndex + 4;
        }
        if(i <= 2501)
        {
            return iconIndex + 3;
        }
        if(i <= 5001)
        {
            return iconIndex + 2;
        }
        if(i <= 7501)
        {
            return iconIndex + 1;
        } else
        {
            return iconIndex;
        }
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(itemstack.itemID != mod_IC2.itemBatRE.shiftedIndex)
        {
            return itemstack;
        }
        if(Platform.isSimulating())
        {
            int i = 10001 - itemstack.getItemDamage();
            for(int j = 0; j < 9; j++)
            {
                ItemStack itemstack1 = entityplayer.inventory.mainInventory[j];
                if(itemstack1 == null || !(Item.itemsList[itemstack1.itemID] instanceof IChargeableItem) || itemstack1 == itemstack)
                {
                    continue;
                }
                i -= ((IChargeableItem)Item.itemsList[itemstack1.itemID]).giveEnergyTo(itemstack1, i, 1, false);
                i -= ((IChargeableItem)Item.itemsList[itemstack1.itemID]).giveEnergyTo(itemstack1, i, 1, false);
                if(i <= 0)
                {
                    break;
                }
            }

            itemstack.setItemDamage(10001 - i);
            if(i <= 0)
            {
                itemstack = new ItemStack(mod_IC2.itemBatREDischarged);
            }
        }
        return itemstack;
    }

    public int ratio;
    public int transfer;
    public boolean rechargeable;
    public int tier;
}
