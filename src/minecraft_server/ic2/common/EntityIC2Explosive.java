// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ExplosionIC2

public class EntityIC2Explosive extends Entity
{

    public EntityIC2Explosive(World world)
    {
        super(world);
        fuse = 80;
        explosivePower = 4F;
        dropRate = 0.3F;
        damageVsEntitys = 1.0F;
        renderBlock = Block.dirt;
        preventEntitySpawning = true;
        setSize(0.98F, 0.98F);
        yOffset = height / 2.0F;
    }

    public EntityIC2Explosive(World world, double d, double d1, double d2, 
            int i, float f, float f1, float f2, Block block)
    {
        this(world);
        setPosition(d, d1, d2);
        float f3 = (float)(Math.random() * 3.1415927410125732D * 2D);
        motionX = -MathHelper.sin((f3 * 3.141593F) / 180F) * 0.02F;
        motionY = 0.20000000298023224D;
        motionZ = -MathHelper.cos((f3 * 3.141593F) / 180F) * 0.02F;
        prevPosX = d;
        prevPosY = d1;
        prevPosZ = d2;
        fuse = i;
        explosivePower = f;
        dropRate = f1;
        damageVsEntitys = f2;
        renderBlock = block;
    }

    protected void entityInit()
    {
    }

    protected boolean canTriggerWalking()
    {
        return false;
    }

    public boolean canBeCollidedWith()
    {
        return !isDead;
    }

    public void onUpdate()
    {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        motionY -= 0.039999999105930328D;
        moveEntity(motionX, motionY, motionZ);
        motionX *= 0.98000001907348633D;
        motionY *= 0.98000001907348633D;
        motionZ *= 0.98000001907348633D;
        if(onGround)
        {
            motionX *= 0.69999998807907104D;
            motionZ *= 0.69999998807907104D;
            motionY *= -0.5D;
        }
        if(fuse-- <= 0)
        {
            if(Platform.isSimulating())
            {
                setEntityDead();
                explode();
            } else
            {
                setEntityDead();
            }
        } else
        {
            worldObj.spawnParticle("smoke", posX, posY + 0.5D, posZ, 0.0D, 0.0D, 0.0D);
        }
    }

    private void explode()
    {
        ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, posX, posY, posZ, explosivePower, dropRate, damageVsEntitys);
        explosionic2.doExplosion();
    }

    protected void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setByte("Fuse", (byte)fuse);
    }

    protected void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        fuse = nbttagcompound.getByte("Fuse");
    }

    public float getShadowSize()
    {
        return 0.0F;
    }

    public int fuse;
    public float explosivePower;
    public float dropRate;
    public float damageVsEntitys;
    public Block renderBlock;
}
