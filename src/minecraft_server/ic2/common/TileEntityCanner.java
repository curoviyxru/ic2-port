// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.platform.*;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, ContainerCanner

public class TileEntityCanner extends TileEntityElecMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityCanner()
    {
        super(4, 1, 631, 32);
        progress = 0;
        fuelQuality = 0;
        energyconsume = 1;
        operationLength = 600;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        fuelQuality = nbttagcompound.getShort("fuelQuality");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("fuelQuality", (short)fuelQuality);
    }

    public int gaugeProgressScaled(int i)
    {
        int j = operationLength;
        if(getMode() == 1 && inventory[0] != null)
        {
            int k = getFoodValue(inventory[0]);
            if(k > 0)
            {
                j = 50 * k;
            }
        }
        if(getMode() == 3)
        {
            j = 50;
        }
        return (progress * i) / j;
    }

    public int gaugeFuelScaled(int i)
    {
        if(energy <= 0)
        {
            return 0;
        }
        int j = (energy * i) / (operationLength * energyconsume);
        if(j > i)
        {
            return i;
        } else
        {
            return j;
        }
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = false;
            boolean flag1 = canOperate();
            if(energy <= energyconsume * operationLength && flag1)
            {
                flag = provideEnergy();
            }
            boolean flag2 = getActive();
            if(flag1 && (getMode() == 1 && progress >= getFoodValue(inventory[0]) * 50 || getMode() == 2 && progress > 0 && progress % 100 == 0 || getMode() == 3 && progress >= 50))
            {
                if(getMode() == 1 || getMode() == 3 || progress >= 600)
                {
                    operate(false);
                    fuelQuality = 0;
                    progress = 0;
                    flag2 = false;
                } else
                {
                    operate(true);
                }
                flag = true;
            }
            if(!flag2 || progress == 0)
            {
                if(flag1)
                {
                    if(energy >= energyconsume)
                    {
                        flag2 = true;
                    }
                } else
                if(getMode() != 2)
                {
                    fuelQuality = 0;
                    progress = 0;
                }
            } else
            if(!flag1 || energy < energyconsume)
            {
                if(!flag1 && getMode() != 2)
                {
                    fuelQuality = 0;
                    progress = 0;
                }
                flag2 = false;
            }
            if(flag2)
            {
                progress++;
                energy -= energyconsume;
            }
            if(flag)
            {
                onInventoryChanged();
            }
            if(flag2 != getActive())
            {
                setActive(flag2);
            }
        }
    }

    public void operate(boolean flag)
    {
        switch(getMode())
        {
        default:
            break;

        case 1: // '\001'
            int i = getFoodValue(inventory[0]);
            inventory[0].stackSize--;
            if(inventory[0].getItem() == Item.bowlSoup && inventory[0].stackSize <= 0)
            {
                inventory[0] = new ItemStack(Item.bowlEmpty);
            }
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
            inventory[3].stackSize -= i;
            if(inventory[3].stackSize <= 0)
            {
                inventory[3] = null;
            }
            if(inventory[2] == null)
            {
                inventory[2] = new ItemStack(mod_IC2.itemTinCanFilled, i);
            } else
            {
                inventory[2].stackSize += i;
            }
            break;

        case 2: // '\002'
            int j = getFuelValue(inventory[0].itemID);
            inventory[0].stackSize--;
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
            fuelQuality += j;
            if(flag)
            {
                break;
            }
            if(inventory[3].itemID == mod_IC2.itemFuelCanEmpty.shiftedIndex)
            {
                inventory[3].stackSize--;
                if(inventory[3].stackSize <= 0)
                {
                    inventory[3] = null;
                }
                inventory[2] = new ItemStack(mod_IC2.itemFuelCan, 1, fuelQuality);
                break;
            }
            int k = inventory[3].getItemDamage();
            k -= fuelQuality;
            if(k < 1)
            {
                k = 1;
            }
            inventory[3] = null;
            inventory[2] = new ItemStack(mod_IC2.itemArmorJetpack, 1, k);
            break;

        case 3: // '\003'
            inventory[0].stackSize--;
            inventory[3].setItemDamage(inventory[3].getItemDamage() - 1);
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
            if(inventory[0] == null || inventory[3].getItemDamage() <= 1)
            {
                inventory[2] = inventory[3];
                inventory[3] = null;
            }
            break;
        }
    }

    public void invalidate()
    {
        if(audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public boolean canOperate()
    {
        if(inventory[0] == null)
        {
            return false;
        }
        switch(getMode())
        {
        default:
            break;

        case 1: // '\001'
            int i = getFoodValue(inventory[0]);
            if(i > 0 && i <= inventory[3].stackSize && (inventory[2] == null || inventory[2].stackSize + i <= inventory[2].getMaxStackSize() && inventory[2].itemID == mod_IC2.itemTinCanFilled.shiftedIndex))
            {
                return true;
            }
            break;

        case 2: // '\002'
            int j = getFuelValue(inventory[0].itemID);
            if(j > 0 && inventory[2] == null)
            {
                return true;
            }
            break;

        case 3: // '\003'
            if(inventory[3].getItemDamage() > 1 && getPelletValue(inventory[0]) > 0 && inventory[2] == null)
            {
                return true;
            }
            break;
        }
        return false;
    }

    public int getMode()
    {
        if(inventory[3] == null)
        {
            return 0;
        }
        if(inventory[3].itemID == mod_IC2.itemTinCan.shiftedIndex)
        {
            return 1;
        }
        if(inventory[3].itemID == mod_IC2.itemFuelCanEmpty.shiftedIndex || inventory[3].itemID == mod_IC2.itemArmorJetpack.shiftedIndex)
        {
            return 2;
        }
        return inventory[3].itemID != mod_IC2.itemArmorCFPack.shiftedIndex ? 0 : 3;
    }

    public String getInvName()
    {
        return "Canning Machine";
    }

    private int getFoodValue(ItemStack itemstack)
    {
        if(itemstack.getItem() instanceof ItemFood)
        {
            ItemFood itemfood = (ItemFood)itemstack.getItem();
            return (int)Math.ceil((double)itemfood.getHealAmount() / 2D);
        } else
        {
            return 0;
        }
    }

    public int getFuelValue(int i)
    {
        if(i == mod_IC2.itemCellCoalRef.shiftedIndex)
        {
            return 2548;
        }
        if(i == mod_IC2.itemCellBioRef.shiftedIndex)
        {
            return 868;
        }
        if(i == Item.redstone.shiftedIndex && fuelQuality > 0)
        {
            return (int)((double)fuelQuality * 0.20000000000000001D);
        }
        if(i == Item.lightStoneDust.shiftedIndex && fuelQuality > 0)
        {
            return (int)((double)fuelQuality * 0.29999999999999999D);
        }
        if(i == Item.gunpowder.shiftedIndex && fuelQuality > 0)
        {
            return (int)((double)fuelQuality * 0.40000000000000002D);
        } else
        {
            return 0;
        }
    }

    public int getPelletValue(ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return 0;
        }
        if(itemstack.itemID != mod_IC2.itemPartPellet.shiftedIndex)
        {
            return 0;
        } else
        {
            return itemstack.stackSize;
        }
    }

    public String getStartSoundFile()
    {
        return null;
    }

    public String getInterruptSoundFile()
    {
        return null;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerCanner(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        byte byte0;
        switch(getFacing())
        {
        case 2: // '\002'
            byte0 = 4;
            break;

        case 3: // '\003'
            byte0 = 5;
            break;

        case 4: // '\004'
            byte0 = 3;
            break;

        case 5: // '\005'
            byte0 = 2;
            break;

        default:
            byte0 = 2;
            break;
        }
        if(i == byte0)
        {
            return 1;
        }
        switch(i)
        {
        case 0: // '\0'
            return 3;

        case 1: // '\001'
            return 0;
        }
        return 2;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public float getWrenchDropRate()
    {
        return 0.85F;
    }

    public short progress;
    public int energyconsume;
    public int operationLength;
    private int fuelQuality;
    public AudioSource audioSource;
}
