// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockFoam extends BlockTex
{

    public BlockFoam(int i, int j, Material material)
    {
        super(i, j, material);
        setTickOnLoad(true);
    }

    public int tickRate()
    {
        return 500;
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return true;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return null;
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        if(world.getBlockLightValue(i, j, k) * 6 >= world.rand.nextInt(1000))
        {
            world.setBlockAndMetadataWithNotify(i, j, k, mod_IC2.blockWall.blockID, 7);
        } else
        {
            world.scheduleBlockUpdate(i, j, k, blockID, tickRate());
        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        ItemStack itemstack = entityplayer.getCurrentEquippedItem();
        if(itemstack != null && itemstack.itemID == Block.sand.blockID)
        {
            world.setBlockAndMetadataWithNotify(i, j, k, mod_IC2.blockWall.blockID, 7);
            itemstack.stackSize--;
            if(itemstack.stackSize <= 0)
            {
                entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem] = null;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        int l = world.getBlockId(i, j, k);
        return l == 0 || l == Block.fire.blockID || world.getBlockMaterial(i, j, k).getIsLiquid();
    }
}
