// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBaseGenerator, TileEntityIronFurnace, ContainerBaseGenerator

public class TileEntityGenerator extends TileEntityBaseGenerator
    implements ISidedInventory
{

    public TileEntityGenerator()
    {
        super(2);
        itemFuelTime = 0;
        production = mod_IC2.energyGeneratorBase;
    }

    public short getMaximumStorage()
    {
        return 4000;
    }

    public int gaugeFuelScaled(int i)
    {
        if(fuel <= 0)
        {
            return 0;
        }
        if(itemFuelTime <= 0)
        {
            itemFuelTime = fuel;
        }
        int j = (fuel * i) / itemFuelTime;
        if(j > i)
        {
            j = i;
        }
        return j;
    }

    public boolean gainFuel()
    {
        if(inventory[1] == null)
        {
            return false;
        }
        if(inventory[1].itemID == Item.bucketLava.shiftedIndex)
        {
            return false;
        }
        int i = TileEntityIronFurnace.getFuelValueFor(inventory[1]) / 4;
        if(i <= 0)
        {
            return false;
        }
        fuel += i;
        itemFuelTime = i;
        if(inventory[1].getItem().hasContainerItem())
        {
            inventory[1] = new ItemStack(inventory[1].getItem().getContainerItem(), 1, 0);
        } else
        {
            inventory[1].stackSize--;
        }
        if(inventory[1].stackSize == 0)
        {
            inventory[1] = null;
        }
        return true;
    }

    public String getInvName()
    {
        return "Generator";
    }

    public boolean isConverting()
    {
        return fuel > 0;
    }

    public String getOperationSoundFile()
    {
        return "Generators/GeneratorLoop.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerBaseGenerator(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public int itemFuelTime;
}
