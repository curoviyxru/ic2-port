// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBaseGenerator, ContainerWaterGenerator

public class TileEntityWaterGenerator extends TileEntityBaseGenerator
    implements ISidedInventory
{

    public TileEntityWaterGenerator()
    {
        super(2);
        initialized = false;
        water = 0;
        microStorage = 0;
        maxWater = 2000;
        production = 2;
        ticker = randomizer.nextInt(tickRate());
    }

    public short getMaximumStorage()
    {
        return 100;
    }

    public int gaugeFuelScaled(int i)
    {
        if(fuel <= 0)
        {
            return 0;
        } else
        {
            return (fuel * i) / maxWater;
        }
    }

    public boolean gainFuel()
    {
        if(inventory[1] != null && maxWater - fuel >= 500 && inventory[1].itemID == Item.bucketWater.shiftedIndex)
        {
            production = 2;
            fuel += 500;
            inventory[1].itemID = Item.bucketEmpty.shiftedIndex;
            return true;
        }
        if(fuel <= 0)
        {
            flowPower();
            production = microStorage / 100;
            microStorage -= production * 100;
            if(production > 0)
            {
                fuel++;
                return true;
            } else
            {
                return false;
            }
        } else
        {
            return false;
        }
    }

    public boolean needsFuel()
    {
        return fuel <= maxWater;
    }

    public void flowPower()
    {
        if(ticker++ % tickRate() == 0 || !initialized)
        {
            updateWaterCount();
            initialized = true;
        }
        water = (water * mod_IC2.energyGeneratorWater) / 100;
        if(water > 0)
        {
            microStorage += water;
        }
    }

    public void updateWaterCount()
    {
        int i = 0;
        for(int j = xCoord - 1; j < xCoord + 2; j++)
        {
            for(int k = yCoord - 1; k < yCoord + 2; k++)
            {
                for(int l = zCoord - 1; l < zCoord + 2; l++)
                {
                    if(worldObj.getBlockId(j, k, l) == Block.waterMoving.blockID || worldObj.getBlockId(j, k, l) == Block.waterStill.blockID)
                    {
                        i++;
                    }
                }

            }

        }

        water = i;
    }

    public String getInvName()
    {
        return "Water Mill";
    }

    public int tickRate()
    {
        return 128;
    }

    public String getOperationSoundFile()
    {
        return "Generators/WatermillLoop.ogg";
    }

    public boolean delayActiveUpdate()
    {
        return true;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerWaterGenerator(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public static Random randomizer = new Random();
    public int ticker;
    public boolean initialized;
    public int water;
    public int microStorage;
    public int maxWater;

}
