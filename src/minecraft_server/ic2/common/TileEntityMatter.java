// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.api.Direction;
import ic2.platform.*;
import java.util.*;

import ic2.platform.NetworkManager;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, ExplosionIC2, ContainerMatter, 
//            PositionSpec

public class TileEntityMatter extends TileEntityElecMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityMatter()
    {
        super(2, 0, 0x10c8e0, 512);
        scrap = 0;
        state = 0;
        prevState = 0;
        soundTicker = mod_IC2.random.nextInt(32);
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        scrap = nbttagcompound.getShort("scrap");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("scrap", scrap);
    }

    public String getInvName()
    {
        return "Mass Fabricator";
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(isRedstonePowered() || energy <= 0)
            {
                setState(0);
                setActive(false);
            } else
            {
                setState(scrap <= 0 ? 1 : 2);
                setActive(true);
                boolean flag = false;
                if(scrap < 1000 && inventory[0] != null && inventory[0].itemID == mod_IC2.itemScrap.shiftedIndex)
                {
                    inventory[0].stackSize--;
                    if(inventory[0].stackSize <= 0)
                    {
                        inventory[0] = null;
                    }
                    scrap += 5000;
                }
                if(energy >= 0xf4240)
                {
                    flag = attemptGeneration();
                }
                if(flag)
                {
                    onInventoryChanged();
                }
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
            audioSourceScrap = null;
        }
        super.invalidate();
    }

    public boolean attemptGeneration()
    {
        if(inventory[1] == null)
        {
            inventory[1] = new ItemStack(mod_IC2.itemMatter);
            energy -= 0xf4240;
            return true;
        }
        if(inventory[1].itemID != mod_IC2.itemMatter.shiftedIndex || inventory[1].stackSize + 1 > inventory[1].getMaxStackSize())
        {
            return false;
        } else
        {
            energy -= 0xf4240;
            inventory[1].stackSize++;
            return true;
        }
    }

    public boolean demandsEnergy()
    {
        if(isRedstonePowered())
        {
            return false;
        } else
        {
            return energy < maxEnergy;
        }
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 512)
        {
            worldObj.setBlockWithNotify(xCoord, yCoord, zCoord, 0);
            ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, xCoord, yCoord, zCoord, 15F, 0.01F, 1.5F);
            explosionic2.doExplosion();
            return 0;
        }
        int j = i;
        if(j > scrap)
        {
            j = scrap;
        }
        scrap -= j;
        energy += i + 5 * j;
        int k = 0;
        if(energy > maxEnergy)
        {
            k = energy - maxEnergy;
            energy = maxEnergy;
        }
        return k;
    }

    public String getProgressAsString()
    {
        int i = energy / 10000;
        if(i > 100)
        {
            i = 100;
        }
        return (new StringBuilder()).append("").append(i).append("%").toString();
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerMatter(inventoryplayer, this);
    }

    private void setState(int i)
    {
        state = i;
        if(prevState != i)
        {
            NetworkManager.updateTileEntityField(this, "state");
        }
        prevState = i;
    }

    public List getNetworkedFields()
    {
        Vector vector = new Vector(1);
        vector.add("state");
        return vector;
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("state") && prevState != state)
        {
            switch(state)
            {
            default:
                break;

            case 0: // '\0'
                if(audioSource != null)
                {
                    audioSource.stop();
                }
                if(audioSourceScrap != null)
                {
                    audioSourceScrap.stop();
                }
                break;

            case 1: // '\001'
                if(audioSource == null)
                {
                    audioSource = AudioManager.createSource(this, PositionSpec.Center, "Generators/MassFabricator/MassFabLoop.ogg", true, false, AudioManager.defaultVolume);
                }
                if(audioSource != null)
                {
                    audioSource.play();
                }
                if(audioSourceScrap != null)
                {
                    audioSourceScrap.stop();
                }
                break;

            case 2: // '\002'
                if(audioSource == null)
                {
                    audioSource = AudioManager.createSource(this, PositionSpec.Center, "Generators/MassFabricator/MassFabLoop.ogg", true, false, AudioManager.defaultVolume);
                }
                if(audioSourceScrap == null)
                {
                    audioSourceScrap = AudioManager.createSource(this, PositionSpec.Center, "Generators/MassFabricator/MassFabScrapSolo.ogg", true, false, AudioManager.defaultVolume);
                }
                if(audioSource != null)
                {
                    audioSource.play();
                }
                if(audioSourceScrap != null)
                {
                    audioSourceScrap.play();
                }
                break;
            }
            prevState = state;
        }
        super.onNetworkUpdate(s);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 0;

        case 1: // '\001'
        default:
            return 1;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public float getWrenchDropRate()
    {
        return 0.7F;
    }

    public int soundTicker;
    public short scrap;
    private final int StateIdle = 0;
    private final int StateRunning = 1;
    private final int StateRunningScrap = 2;
    private int state;
    private int prevState;
    private AudioSource audioSource;
    private AudioSource audioSourceScrap;
}
