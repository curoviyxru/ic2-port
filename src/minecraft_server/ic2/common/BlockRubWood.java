// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockRubWood extends BlockTex
{

    public BlockRubWood(int i)
    {
        super(i, 44, Material.wood);
        setTickOnLoad(true);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getBlockMetadata(i, j, k);
        if(l < 2)
        {
            return 47;
        }
        if(l == i1 % 6)
        {
            return i1 <= 5 ? 45 : 46;
        } else
        {
            return 44;
        }
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        return i >= 2 ? 44 : 47;
    }

    public void dropBlockAsItemWithChance(World world, int i, int j, int k, int l, float f)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        int i1 = quantityDropped(world.rand);
        for(int j1 = 0; j1 < i1; j1++)
        {
            if(world.rand.nextFloat() > f)
            {
                continue;
            }
            int k1 = idDropped(l, world.rand);
            if(k1 > 0)
            {
                dropBlockAsItem_do(world, i, j, k, new ItemStack(k1, 1, 0));
            }
            if(l != 0 && world.rand.nextInt(6) == 0)
            {
                dropBlockAsItem_do(world, i, j, k, new ItemStack(mod_IC2.itemHarz.shiftedIndex, 1, 0));
            }
        }

    }

    public void onBlockRemoval(World world, int i, int j, int k)
    {
        byte byte0 = 4;
        int l = byte0 + 1;
        if(world.checkChunksExist(i - l, j - l, k - l, i + l, j + l, k + l))
        {
            for(int i1 = -byte0; i1 <= byte0; i1++)
            {
                for(int j1 = -byte0; j1 <= byte0; j1++)
                {
                    for(int k1 = -byte0; k1 <= byte0; k1++)
                    {
                        int l1 = world.getBlockId(i + i1, j + j1, k + k1);
                        if(l1 != mod_IC2.blockRubLeaves.blockID)
                        {
                            continue;
                        }
                        int i2 = world.getBlockMetadata(i + i1, j + j1, k + k1);
                        if((i2 & 8) == 0)
                        {
                            world.setBlockMetadata(i + i1, j + j1, k + k1, i2 | 8);
                        }
                    }

                }

            }

        }
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        int l = world.getBlockMetadata(i, j, k);
        if(l < 6)
        {
            return;
        }
        if(random.nextInt(200) == 0)
        {
            world.setBlockMetadataWithNotify(i, j, k, l % 6);
        } else
        {
            world.scheduleBlockUpdate(i, j, k, blockID, tickRate());
        }
    }

    public int tickRate()
    {
        return 100;
    }

    public int getMobilityFlag()
    {
        return 2;
    }
}
