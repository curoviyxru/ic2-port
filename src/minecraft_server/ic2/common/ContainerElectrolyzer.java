// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityElectrolyzer

public class ContainerElectrolyzer extends ContainerIC2
{

    public ContainerElectrolyzer(InventoryPlayer inventoryplayer, TileEntityElectrolyzer tileentityelectrolyzer)
    {
        energy = 0;
        tileentity = tileentityelectrolyzer;
        addSlot(new Slot(tileentity, 0, 53, 35));
        addSlot(new Slot(tileentity, 1, 112, 35));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(energy != tileentity.energy)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.energy);
            }
        }

        energy = tileentity.energy;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.energy = (short)j;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 2;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityElectrolyzer tileentity;
    public short energy;
}
