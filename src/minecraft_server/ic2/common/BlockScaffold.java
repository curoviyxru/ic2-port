// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockScaffold extends BlockTex
{

    public BlockScaffold(int i)
    {
        super(i, 116, Material.wood);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getBlockMetadata(i, j, k);
        if(i1 == reinforcedStrength)
        {
            return blockIndexInTexture + 2;
        }
        if(l < 2)
        {
            return blockIndexInTexture + 1;
        } else
        {
            return blockIndexInTexture;
        }
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        if(i < 2)
        {
            return blockIndexInTexture + 1;
        } else
        {
            return blockIndexInTexture;
        }
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        float f = 0.0625F;
        return AxisAlignedBB.getBoundingBoxFromPool((float)i + f, (float)j, (float)k + f, ((float)i + 1.0F) - f, (float)j + 1.0F, ((float)k + 1.0F) - f);
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        if(entity instanceof EntityPlayer)
        {
            EntityPlayer entityplayer = (EntityPlayer)entity;
            mod_IC2.setFallDistanceOfEntity(entityplayer, 0.0F);
            if(entityplayer.motionY < -0.14999999999999999D)
            {
                entityplayer.motionY = -0.14999999999999999D;
            }
            if(Platform.isKeyDownForward(entityplayer) && entityplayer.motionY < 0.20000000000000001D)
            {
                entityplayer.motionY = 0.20000000000000001D;
            }
        }
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.getBoundingBoxFromPool(i, j, k, i + 1, j + 1, k + 1);
    }

    public ArrayList getBlockDropped(World world, int i, int j, int k, int l)
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add(new ItemStack(this, 1));
        if(l == reinforcedStrength)
        {
            arraylist.add(new ItemStack(Item.stick, 2));
        }
        return arraylist;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.isSneaking())
        {
            return false;
        }
        ItemStack itemstack = entityplayer.inventory.getCurrentItem();
        if(itemstack == null || itemstack.itemID != Item.stick.shiftedIndex || itemstack.stackSize < 2)
        {
            return false;
        }
        if(world.getBlockMetadata(i, j, k) == reinforcedStrength || !isPillar(world, i, j, k))
        {
            return false;
        }
        itemstack.stackSize -= 2;
        if(entityplayer.getCurrentEquippedItem().stackSize <= 0)
        {
            entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem] = null;
        }
        world.setBlockMetadataWithNotify(i, j, k, reinforcedStrength);
        world.markBlocksDirty(i, j, k, i, j, k);
        return true;
    }

    public void onBlockClicked(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.getCurrentEquippedItem() != null && entityplayer.getCurrentEquippedItem().itemID == blockID)
        {
            for(; world.getBlockId(i, j, k) == blockID; j++) { }
            if(canPlaceBlockAt(world, i, j, k))
            {
                world.setBlockWithNotify(i, j, k, blockID);
                onBlockPlaced(world, i, j, k, 0);
                entityplayer.getCurrentEquippedItem().stackSize--;
                if(entityplayer.getCurrentEquippedItem().stackSize <= 0)
                {
                    entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem] = null;
                }
            }
        }
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        if(getStrengthFrom(world, i, j, k) <= -1)
        {
            return false;
        } else
        {
            return super.canPlaceBlockAt(world, i, j, k);
        }
    }

    public boolean isPillar(World world, int i, int j, int k)
    {
        for(; world.getBlockId(i, j, k) == blockID; j--) { }
        return world.isBlockNormalCube(i, j, k);
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        updateSupportStatus(world, i, j, k);
    }

    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        updateTick(world, i, j, k, null);
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        int l = world.getBlockMetadata(i, j, k);
        if(l >= reinforcedStrength)
        {
            if(isPillar(world, i, j, k))
            {
                l = reinforcedStrength;
            } else
            {
                l = getStrengthFrom(world, i, j, k);
                dropBlockAsItem_do(world, i, j, k, new ItemStack(Item.stick, 2));
            }
        } else
        {
            l = getStrengthFrom(world, i, j, k);
        }
        if(l <= -1)
        {
            world.setBlockWithNotify(i, j, k, 0);
            dropBlockAsItem_do(world, i, j, k, new ItemStack(this));
        } else
        if(l != world.getBlockMetadata(i, j, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, l);
            world.markBlocksDirty(i, j, k, i, j, k);
        }
    }

    public int getStrengthFrom(World world, int i, int j, int k)
    {
        int l = 0;
        if(isPillar(world, i, j - 1, k))
        {
            l = standardStrength + 1;
        }
        l = compareStrengthTo(world, i, j + 1, k, l);
        l = compareStrengthTo(world, i, j - 1, k, l);
        l = compareStrengthTo(world, i + 1, j, k, l);
        l = compareStrengthTo(world, i - 1, j, k, l);
        l = compareStrengthTo(world, i, j, k + 1, l);
        l = compareStrengthTo(world, i, j, k - 1, l);
        return l - 1;
    }

    public int compareStrengthTo(World world, int i, int j, int k, int l)
    {
        int i1 = 0;
        if(world.getBlockId(i, j, k) == blockID)
        {
            i1 = world.getBlockMetadata(i, j, k);
            if(i1 > reinforcedStrength)
            {
                i1 = reinforcedStrength;
            }
        }
        if(i1 > l)
        {
            return i1;
        } else
        {
            return l;
        }
    }

    public void updateSupportStatus(World world, int i, int j, int k)
    {
        world.scheduleBlockUpdate(i, j, k, blockID, tickDelay);
    }

    public static int standardStrength = 2;
    public static int reinforcedStrength = 5;
    public static int tickDelay = 1;

}
