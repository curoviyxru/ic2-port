// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySource;
import ic2.platform.*;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IHasGuiContainer, EnergyNet, IChargeableItem, 
//            PositionSpec

public abstract class TileEntityBaseGenerator extends TileEntityMachine
    implements IEnergySource, IHasGuiContainer
{

    public TileEntityBaseGenerator(int i)
    {
        super(i);
        fuel = 0;
        storage = 0;
        production = 1;
        activityMeter = 0;
        addedToEnergyNet = false;
        ticksSinceLastActiveUpdate = random.nextInt(256);
    }

    public abstract short getMaximumStorage();

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        fuel = nbttagcompound.getShort("fuel");
        storage = nbttagcompound.getShort("storage");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("fuel", fuel);
        nbttagcompound.setShort("storage", storage);
    }

    public int gaugeStorageScaled(int i)
    {
        return (storage * i) / maxStorage;
    }

    public abstract int gaugeFuelScaled(int i);

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            boolean flag = isConverting();
            boolean flag1 = false;
            boolean flag2 = false;
            if(needsFuel())
            {
                flag1 = gainFuel();
            }
            if(flag)
            {
                if(!getActive())
                {
                    flag1 = true;
                }
                flag2 = true;
                storage += production;
                fuel--;
            } else
            {
                flag2 = false;
            }
            if(storage > 0)
            {
                if(inventory[0] != null && (Item.itemsList[inventory[0].itemID] instanceof IChargeableItem) && inventory[0].getItemDamage() != 1)
                {
                    int i = ((IChargeableItem)Item.itemsList[inventory[0].itemID]).giveEnergyTo(inventory[0], storage, 1, false);
                    storage -= i;
                    if(i > 0)
                    {
                        flag1 = true;
                    }
                } else
                {
                    int j = production;
                    if(j > storage)
                    {
                        j = storage;
                    }
                    if(j > 0)
                    {
                        storage += sendEnergy(j) - j;
                    }
                }
            }
            if(storage > maxStorage)
            {
                storage = maxStorage;
            }
            if(flag1)
            {
                onInventoryChanged();
            }
            if(!delayActiveUpdate())
            {
                setActive(flag2);
            } else
            {
                if(ticksSinceLastActiveUpdate % 256 == 0)
                {
                    setActive(activityMeter > 0);
                    activityMeter = 0;
                }
                if(flag2)
                {
                    activityMeter++;
                } else
                {
                    activityMeter--;
                }
                ticksSinceLastActiveUpdate++;
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public boolean isConverting()
    {
        return fuel > 0 && storage + production <= maxStorage;
    }

    public boolean needsFuel()
    {
        return fuel <= 0 && storage + production <= maxStorage;
    }

    public abstract boolean gainFuel();

    public int sendEnergy(int i)
    {
        return EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, i);
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public int getMaxEnergyOutput()
    {
        return production;
    }

    public abstract String getInvName();

    public String getOperationSoundFile()
    {
        return null;
    }

    public boolean delayActiveUpdate()
    {
        return false;
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != getActive())
        {
            if(audioSource == null && getOperationSoundFile() != null)
            {
                audioSource = AudioManager.createSource(this, PositionSpec.Center, getOperationSoundFile(), true, false, AudioManager.defaultVolume);
            }
            if(getActive())
            {
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(audioSource != null)
            {
                audioSource.stop();
            }
        }
        super.onNetworkUpdate(s);
    }

    public float getWrenchDropRate()
    {
        return 0.9F;
    }

    public static Random random = new Random();
    public short fuel;
    public short storage;
    public final short maxStorage = getMaximumStorage();
    public int production;
    public int ticksSinceLastActiveUpdate;
    public int activityMeter;
    public boolean addedToEnergyNet;
    public AudioSource audioSource;

}
