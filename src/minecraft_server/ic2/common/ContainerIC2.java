// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.List;
import net.minecraft.src.*;

public abstract class ContainerIC2 extends Container
{

    public ContainerIC2()
    {
    }

    public abstract int guiInventorySize();

    public abstract int getInput();

    public int firstEmptyFrom(int i, int j, IInventory iinventory)
    {
        for(int k = i; k <= j; k++)
        {
            if(iinventory.getStackInSlot(k) == null)
            {
                return k;
            }
        }

        return -1;
    }

    public final ItemStack getStackInSlot(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)Platform.getContainerSlots(this).get(i);
        if(slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if(i < guiInventorySize())
            {
                func_28125_a(itemstack1, guiInventorySize(), guiInventorySize() + 36, false);
            } else
            if(i >= guiInventorySize() && i < guiInventorySize() + 27)
            {
                int j = getInput();
                if(j == -1 || j >= guiInventorySize())
                {
                    func_28125_a(itemstack1, guiInventorySize() + 27, guiInventorySize() + 36, false);
                } else
                {
                    func_28125_a(itemstack1, j, j + 1, false);
                }
            } else
            if(i >= guiInventorySize() + 27 && i < guiInventorySize() + 36)
            {
                func_28125_a(itemstack1, guiInventorySize(), guiInventorySize() + 27, false);
            }
            if(itemstack1.stackSize == 0)
            {
                slot.putStack(null);
            } else
            {
                slot.onSlotChanged();
            }
            if(itemstack1.stackSize != itemstack.stackSize)
            {
                slot.onPickupFromSlot(itemstack1);
            } else
            {
                return null;
            }
        }
        return itemstack;
    }

    public abstract boolean isUsableByPlayer(EntityPlayer entityplayer);

    public abstract void updateProgressBar(int i, int j);

    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return isUsableByPlayer(entityplayer);
    }
}
