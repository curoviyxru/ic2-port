// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            TileEntityBaseGenerator, ItemArmorBatpack, StackUtil, ContainerWindGenerator

public class TileEntityWindGenerator extends TileEntityBaseGenerator
    implements ISidedInventory
{

    public TileEntityWindGenerator()
    {
        super(1);
        subproduction = 0.0D;
        substorage = 0.0D;
        initialized = false;
        ticker = randomizer.nextInt(tickRate());
        production = 4;
    }

    public short getMaximumStorage()
    {
        return 100;
    }

    public int gaugeFuelScaled(int i)
    {
        double d = subproduction / 3D;
        int j = (int)(d * (double)i);
        if(j < 0)
        {
            return 0;
        }
        if(j > i)
        {
            return i;
        } else
        {
            return j;
        }
    }

    public int getOverheatScaled(int i)
    {
        double d = (subproduction - 5D) / 5D;
        if(subproduction <= 5D)
        {
            return 0;
        }
        if(subproduction >= 10D)
        {
            return i;
        } else
        {
            return (int)(d * (double)i);
        }
    }

    public boolean gainFuel()
    {
        ticker++;
        if(ticker % tickRate() == 0 || !initialized)
        {
            if(ticker % (8 * tickRate()) != 0 || !initialized)
            {
                updateObscuratedBlockCount();
                initialized = true;
            }
            subproduction = (double)(mod_IC2.windStrength * (yCoord - obscuratedBlockCount)) / 750D;
            if(subproduction <= 0.0D)
            {
                return false;
            }
            if(worldObj.getWorldInfo().getIsThundering())
            {
                subproduction *= 1.5D;
            } else
            if(worldObj.getWorldInfo().getIsRaining())
            {
                subproduction *= 1.2D;
            }
            if(subproduction > 5D && (double)worldObj.rand.nextInt(5000) <= subproduction - 5D)
            {
                subproduction = 0.0D;
                worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, mod_IC2.blockGenerator.blockID, 0);
                for(int i = worldObj.rand.nextInt(5); i > 0; i--)
                {
                    StackUtil.dropAsEntity(worldObj, xCoord, yCoord, zCoord, new ItemStack(ItemArmorBatpack.ingotIron));
                }

                return false;
            }
            subproduction *= mod_IC2.energyGeneratorWind;
            subproduction /= 100D;
        }
        substorage += subproduction;
        production = (short)(int)substorage;
        substorage -= production;
        if(production > 0)
        {
            fuel++;
            return true;
        } else
        {
            return false;
        }
    }

    public void updateObscuratedBlockCount()
    {
        obscuratedBlockCount = -1;
        for(int i = -4; i < 5; i++)
        {
            for(int j = -2; j < 5; j++)
            {
                for(int k = -4; k < 5; k++)
                {
                    if(worldObj.getBlockId(i + xCoord, j + yCoord, k + zCoord) != 0)
                    {
                        obscuratedBlockCount++;
                    }
                }

            }

        }

    }

    public boolean needsFuel()
    {
        return true;
    }

    public String getInvName()
    {
        return "Wind Mill";
    }

    public int tickRate()
    {
        return 128;
    }

    public String getOperationSoundFile()
    {
        return "Generators/WindGenLoop.ogg";
    }

    public boolean delayActiveUpdate()
    {
        return true;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerWindGenerator(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        return 0;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public static Random randomizer = new Random();
    public double subproduction;
    public double substorage;
    public int ticker;
    public boolean initialized;
    public int obscuratedBlockCount;

}
