// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.*;
import ic2.platform.Platform;
import java.io.PrintStream;
import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            EnergyNet, ExplosionIC2

public class TileEntityLuminatorOLD extends TileEntity
    implements IEnergySink, IEnergyConductor
{

    public TileEntityLuminatorOLD()
    {
        energy = 0;
        mode = 0;
        powered = false;
        ticker = 0;
        addedToEnergyNet = false;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getShort("energy");
        mode = nbttagcompound.getShort("mode");
        powered = nbttagcompound.getBoolean("powered");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("energy", (short)energy);
        nbttagcompound.setShort("mode", (short)mode);
        nbttagcompound.setBoolean("poweredy", powered);
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public void updateEntity()
    {
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            ticker++;
            if(ticker % 20 == 0)
            {
                if(ticker % 160 == 0)
                {
                    System.out.println((new StringBuilder()).append("Consume for Mode: ").append(mode).toString());
                    byte byte0 = 5;
                    switch(mode)
                    {
                    case 1: // '\001'
                        byte0 = 10;
                        // fall through

                    case 2: // '\002'
                        byte0 = 40;
                        break;
                    }
                    if(byte0 > energy)
                    {
                        energy = 0;
                        powered = false;
                        System.out.println("Out of energy");
                    } else
                    {
                        System.out.println("Energized");
                        energy -= byte0;
                        powered = true;
                    }
                    updateLightning();
                }
                if(powered)
                {
                    burnMobs();
                }
            }
        }
    }

    public float getLightLevel()
    {
        if(powered)
        {
            System.out.println("get powered");
        }
        System.out.println("get unpowered");
        return 0.9375F;
    }

    public void switchStrength()
    {
        mode = (mode + 1) % 3;
        updateLightning();
    }

    public void updateLightning()
    {
        System.out.println("Update Lightning");
        worldObj.scheduleLightingUpdate(EnumSkyBlock.Sky, xCoord, yCoord, zCoord, xCoord, yCoord, zCoord);
        worldObj.scheduleLightingUpdate(EnumSkyBlock.Block, xCoord, yCoord, zCoord, xCoord, yCoord, zCoord);
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public double getConductionLoss()
    {
        return 0.0D;
    }

    public int getInsulationEnergyAbsorption()
    {
        return 32;
    }

    public int getInsulationBreakdownEnergy()
    {
        return 33;
    }

    public int getConductorBreakdownEnergy()
    {
        return 33;
    }

    public void removeInsulation()
    {
        System.out.println("REmove Insulation");
        poof();
    }

    public void removeConductor()
    {
        System.out.println("REmove Confuctor");
        poof();
    }

    public boolean demandsEnergy()
    {
        return energy < getMaxEnergy();
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 32)
        {
            System.out.println("Injecting > 32");
            poof();
            return 0;
        }
        energy += i;
        int j = 0;
        if(energy > getMaxEnergy())
        {
            j = energy - getMaxEnergy();
            energy = getMaxEnergy();
        }
        return j;
    }

    public int getMaxEnergy()
    {
        switch(mode)
        {
        case 1: // '\001'
            return 20;

        case 2: // '\002'
            return 80;
        }
        return 10;
    }

    public void poof()
    {
        worldObj.setBlockWithNotify(xCoord, yCoord, zCoord, 0);
        ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, 0.5D + (double)xCoord, 0.5D + (double)yCoord, 0.5D + (double)zCoord, 0.5F, 0.85F, 2.0F);
        explosionic2.doExplosion();
    }

    public void burnMobs()
    {
        int i = xCoord;
        int j = yCoord;
        int k = zCoord;
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        boolean flag4 = false;
        boolean flag5 = false;
        if(worldObj.getBlockId(i + 1, j, k) == 0 || worldObj.getBlockId(i + 1, j, k) == Block.glass.blockID || worldObj.getBlockId(i + 1, j, k) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag = true;
        }
        if(worldObj.getBlockId(i - 1, j, k) == 0 || worldObj.getBlockId(i - 1, j, k) == Block.glass.blockID || worldObj.getBlockId(i - 1, j, k) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag1 = true;
        }
        if(worldObj.getBlockId(i, j + 1, k) == 0 || worldObj.getBlockId(i, j + 1, k) == Block.glass.blockID || worldObj.getBlockId(i, j + 1, k) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag2 = true;
        }
        if(worldObj.getBlockId(i, j - 1, k) == 0 || worldObj.getBlockId(i, j - 1, k) == Block.glass.blockID || worldObj.getBlockId(i, j - 1, k) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag3 = true;
        }
        if(worldObj.getBlockId(i, j, k + 1) == 0 || worldObj.getBlockId(i, j, k + 1) == Block.glass.blockID || worldObj.getBlockId(i, j, k + 1) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag4 = true;
        }
        if(worldObj.getBlockId(i, j, k - 1) == 0 || worldObj.getBlockId(i, j, k - 1) == Block.glass.blockID || worldObj.getBlockId(i, j, k - 1) == mod_IC2.blockAlloyGlass.blockID)
        {
            flag5 = true;
        }
        int l = 0;
        int i1 = 0;
        int j1 = 0;
        int k1 = 0;
        int l1 = 0;
        int i2 = 0;
        if(flag)
        {
            l = 3;
        } else
        if(flag2 || flag3 || flag4 || flag5)
        {
            l = 1;
        }
        if(flag1)
        {
            i1 = 3;
        } else
        if(flag2 || flag3 || flag4 || flag5)
        {
            i1 = 1;
        }
        if(flag2)
        {
            j1 = 3;
        } else
        if(flag || flag1 || flag4 || flag5)
        {
            j1 = 1;
        }
        if(flag3)
        {
            k1 = 3;
        } else
        if(flag || flag1 || flag4 || flag5)
        {
            k1 = 1;
        }
        if(flag4)
        {
            l1 = 3;
        } else
        if(flag2 || flag3 || flag || flag1)
        {
            l1 = 1;
        }
        if(flag5)
        {
            i2 = 3;
        } else
        if(flag2 || flag3 || flag || flag1)
        {
            i2 = 1;
        }
        i1 = i - i1;
        k1 = j - k1;
        i2 = k - i2;
        l = i + l;
        j1 = j + j1;
        l1 = k + l1;
        AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
        List list = worldObj.getEntitiesWithinAABBExcludingEntity(null, axisalignedbb.addCoord(i, j, k).expand(3D, 3D, 3D));
        for(int j2 = 0; j2 < list.size(); j2++)
        {
            Entity entity = (Entity)list.get(j2);
            if(!(entity instanceof EntityMob))
            {
                continue;
            }
            double d = entity.posX;
            double d1 = entity.posY;
            double d2 = entity.posZ;
            if(d < (double)i1 || d > (double)(l + 1) || d1 < (double)k1 || d1 > (double)(j1 + 2) || d2 < (double)i2 || d2 > (double)(l1 + 1))
            {
                continue;
            }
            int k2 = entity.fire;
            if(k2 >= 100)
            {
                continue;
            }
            if((k2 += 30) >= 100)
            {
                k2 = 100;
            }
            entity.fire = k2;
        }

    }

    public int energy;
    public int mode;
    public boolean powered;
    public int ticker;
    public boolean addedToEnergyNet;
}
