// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntitySolarGenerator

public class ContainerSolarGenerator extends ContainerIC2
{

    public ContainerSolarGenerator(InventoryPlayer inventoryplayer, TileEntitySolarGenerator tileentitysolargenerator)
    {
        sunIsVisible = false;
        tileentity = tileentitysolargenerator;
        addSlot(new Slot(tileentitysolargenerator, 0, 80, 26));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(sunIsVisible != tileentity.sunIsVisible)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.sunIsVisible ? 1 : 0);
            }
        }

        sunIsVisible = tileentity.sunIsVisible;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.sunIsVisible = j != 0;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 1;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntitySolarGenerator tileentity;
    public boolean sunIsVisible;
}
