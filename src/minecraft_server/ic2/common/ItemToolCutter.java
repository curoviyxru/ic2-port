// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.AudioManager;
import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ItemIC2, TileEntityCable, AudioPosition, PositionSpec

public class ItemToolCutter extends ItemIC2
{

    public ItemToolCutter(int i, int j)
    {
        super(i, j);
        setMaxDamage(512);
        setMaxStackSize(1);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(tileentity instanceof TileEntityCable)
        {
            TileEntityCable tileentitycable = (TileEntityCable)tileentity;
            if(tileentitycable.tryAddInsulation())
            {
                if(entityplayer.inventory.consumeInventoryItem(mod_IC2.itemRubber.shiftedIndex))
                {
                    if(Platform.isSimulating())
                    {
                        itemstack.damageItem(1, null);
                    }
                    return true;
                }
                tileentitycable.tryRemoveInsulation();
            }
        }
        return false;
    }

    public static void cutInsulationFrom(ItemStack itemstack, World world, int i, int j, int k)
    {
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(tileentity instanceof TileEntityCable)
        {
            TileEntityCable tileentitycable = (TileEntityCable)tileentity;
            if(tileentitycable.tryRemoveInsulation())
            {
                if(Platform.isSimulating())
                {
                    double d = (double)world.rand.nextFloat() * 0.69999999999999996D + 0.14999999999999999D;
                    double d1 = (double)world.rand.nextFloat() * 0.69999999999999996D + 0.14999999999999999D;
                    double d2 = (double)world.rand.nextFloat() * 0.69999999999999996D + 0.14999999999999999D;
                    EntityItem entityitem = new EntityItem(world, (double)i + d, (double)j + d1, (double)k + d2, new ItemStack(mod_IC2.itemRubber));
                    entityitem.delayBeforeCanPickup = 10;
                    world.entityJoinedWorld(entityitem);
                    itemstack.damageItem(3, null);
                }
                if(Platform.isRendering())
                {
                    AudioManager.playOnce(new AudioPosition((float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F), PositionSpec.Center, "Tools/InsulationCutters.ogg", true, AudioManager.defaultVolume);
                }
            }
        }
    }
}
