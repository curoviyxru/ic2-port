// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.IPaintableBlock;
import java.util.Random;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            BlockTex

public class BlockWall extends BlockTex
    implements IPaintableBlock
{

    public BlockWall(int i, int j, Material material)
    {
        super(i, j, material);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getBlockMetadata(i, j, k);
        return blockIndexInTexture + i1;
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        return blockIndexInTexture + j;
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public boolean colorBlock(World world, int i, int j, int k, int l)
    {
        if(l != world.getBlockMetadata(i, j, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, l);
            return true;
        } else
        {
            return false;
        }
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        return false;
    }
}
