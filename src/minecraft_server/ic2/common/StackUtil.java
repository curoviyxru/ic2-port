// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import java.util.*;
import net.minecraft.src.*;

public class StackUtil
{

    public StackUtil()
    {
    }

    public static void distributeDrop(TileEntity tileentity, List list)
    {
        Direction adirection[] = Direction.values();
        int i = adirection.length;
label0:
        for(int j = 0; j < i; j++)
        {
            Direction direction = adirection[j];
            if(list.isEmpty())
            {
                break;
            }
            TileEntity tileentity1 = direction.applyToTileEntity(tileentity);
            if(!(tileentity1 instanceof IInventory))
            {
                continue;
            }
            Object obj = (IInventory)tileentity1;
            if(((IInventory) (obj)).getSizeInventory() < 18)
            {
                continue;
            }
            if(tileentity1 instanceof TileEntityChest)
            {
                Direction adirection1[] = Direction.values();
                int k = adirection1.length;
                for(int l = 0; l < k; l++)
                {
                    Direction direction1 = adirection1[l];
                    if(direction1 == Direction.YN || direction1 == Direction.YP)
                    {
                        continue;
                    }
                    TileEntity tileentity2 = direction1.applyToTileEntity(tileentity1);
                    if(!(tileentity2 instanceof TileEntityChest))
                    {
                        continue;
                    }
                    obj = new InventoryLargeChest("", ((IInventory) (obj)), (IInventory)tileentity2);
                    break;
                }

            }
            Iterator iterator1 = list.iterator();
            do
            {
                ItemStack itemstack1;
                do
                {
                    do
                    {
                        if(!iterator1.hasNext())
                        {
                            continue label0;
                        }
                        itemstack1 = (ItemStack)iterator1.next();
                    } while(itemstack1 == null);
                    putInInventory(((IInventory) (obj)), itemstack1);
                } while(itemstack1.stackSize != 0);
                iterator1.remove();
            } while(true);
        }

        ItemStack itemstack;
        for(Iterator iterator = list.iterator(); iterator.hasNext(); dropAsEntity(tileentity.worldObj, tileentity.xCoord, tileentity.yCoord, tileentity.zCoord, itemstack))
        {
            itemstack = (ItemStack)iterator.next();
        }

        list.clear();
    }

    public static void getFromInventory(IInventory iinventory, ItemStack itemstack)
    {
        int i = itemstack.stackSize;
        itemstack.stackSize = 0;
        for(int j = 0; j < iinventory.getSizeInventory(); j++)
        {
            ItemStack itemstack1 = iinventory.getStackInSlot(j);
            if(itemstack1 == null || !itemstack1.isItemEqual(itemstack))
            {
                continue;
            }
            int k = Math.min(i, itemstack1.stackSize);
            i -= k;
            itemstack1.stackSize -= k;
            itemstack.stackSize += k;
            if(itemstack1.stackSize == 0)
            {
                iinventory.setInventorySlotContents(j, null);
            }
            if(i == 0)
            {
                return;
            }
        }

    }

    public static void putInInventory(IInventory iinventory, ItemStack itemstack)
    {
        for(int i = 0; i < iinventory.getSizeInventory(); i++)
        {
            ItemStack itemstack1 = iinventory.getStackInSlot(i);
            if(itemstack1 == null || !itemstack1.isItemEqual(itemstack))
            {
                continue;
            }
            int k = Math.min(itemstack.stackSize, itemstack1.getMaxStackSize() - itemstack1.stackSize);
            itemstack1.stackSize += k;
            itemstack.stackSize -= k;
            if(itemstack.stackSize == 0)
            {
                return;
            }
        }

        for(int j = 0; j < iinventory.getSizeInventory(); j++)
        {
            ItemStack itemstack2 = iinventory.getStackInSlot(j);
            if(itemstack2 != null)
            {
                continue;
            }
            int l = Math.min(itemstack.stackSize, itemstack.getMaxStackSize());
            iinventory.setInventorySlotContents(j, new ItemStack(itemstack.itemID, l, itemstack.getItemDamage()));
            itemstack.stackSize -= l;
            if(itemstack.stackSize == 0)
            {
                return;
            }
        }

    }

    public static void dropAsEntity(World world, int i, int j, int k, ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return;
        } else
        {
            double d = 0.69999999999999996D;
            double d1 = (double)world.rand.nextFloat() * d + (1.0D - d) * 0.5D;
            double d2 = (double)world.rand.nextFloat() * d + (1.0D - d) * 0.5D;
            double d3 = (double)world.rand.nextFloat() * d + (1.0D - d) * 0.5D;
            EntityItem entityitem = new EntityItem(world, (double)i + d1, (double)j + d2, (double)k + d3, itemstack);
            entityitem.delayBeforeCanPickup = 10;
            world.entityJoinedWorld(entityitem);
            return;
        }
    }
}
