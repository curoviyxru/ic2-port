// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityWindGenerator

public class ContainerWindGenerator extends ContainerIC2
{

    public ContainerWindGenerator(InventoryPlayer inventoryplayer, TileEntityWindGenerator tileentitywindgenerator)
    {
        storage = 0;
        fuel = 0;
        tileentity = tileentitywindgenerator;
        addSlot(new Slot(tileentitywindgenerator, 0, 80, 26));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(storage != tileentity.storage)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.storage);
            }
            if(fuel != tileentity.fuel)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.fuel);
            }
        }

        storage = tileentity.storage;
        fuel = tileentity.fuel;
    }

    public void updateProgressBar(int i, int j)
    {
        if(i == 0)
        {
            tileentity.storage = (short)j;
        }
        if(i == 1)
        {
            tileentity.fuel = (short)j;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 1;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityWindGenerator tileentity;
    public short storage;
    public short fuel;
}
