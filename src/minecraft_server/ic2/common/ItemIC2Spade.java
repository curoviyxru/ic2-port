// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.src.EnumToolMaterial;
import net.minecraft.src.ItemSpade;

public class ItemIC2Spade extends ItemSpade
    implements ITextureProvider
{

    public ItemIC2Spade(int i, int j, EnumToolMaterial enumtoolmaterial, float f)
    {
        super(i, enumtoolmaterial);
        efficiencyOnProperMaterial = f;
        setIconIndex(j);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public float efficiencyOnProperMaterial;
}
