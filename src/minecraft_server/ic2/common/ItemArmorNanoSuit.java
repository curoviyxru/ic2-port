// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.*;
import net.minecraft.src.*;

// Referenced classes of package ic2.common:
//            IChargeableItem, ItemArmorQuantumSuit, ItemArmorBackpack, ItemArmorRubBoots

public class ItemArmorNanoSuit extends ItemArmor
    implements ITextureProvider, ISpecialArmor, IChargeableItem
{

    public ItemArmorNanoSuit(int i, int j, int k, int l)
    {
        super(i, 0, k, l);
        iconIndex = j;
        setMaxDamage(252);
        tier = 2;
        ratio = 160;
        transfer = 160;
    }

    public ArmorProperties getProperties(EntityPlayer entityplayer, int i, int j)
    {
        if(i != j)
        {
            return new ArmorProperties(0, false);
        }
        int k = j;
        ItemStack aitemstack[] = entityplayer.inventory.armorInventory;
        double d = 0.0D;
        if(aitemstack[2] != null && (aitemstack[2].getItem() instanceof ItemArmorQuantumSuit) && aitemstack[2].getItemDamage() < aitemstack[2].getMaxDamage() - 1)
        {
            k *= 2;
            k /= 3;
        }
        for(int l = 0; l < 4; l++)
        {
            if(aitemstack[l] == null || !(aitemstack[l].getItem() instanceof ItemArmor))
            {
                continue;
            }
            ItemArmor itemarmor = (ItemArmor)aitemstack[l].getItem();
            if(itemarmor instanceof ItemArmorNanoSuit)
            {
                if(aitemstack[l].getItemDamage() + k >= aitemstack[l].getMaxDamage() - 1)
                {
                    d++;
                    aitemstack[l].setItemDamage(aitemstack[l].getMaxDamage() - 1);
                } else
                {
                    d += itemarmor.damageReduceAmount + 1;
                    aitemstack[l].damageItem(k, null);
                }
                if(itemarmor instanceof ItemArmorQuantumSuit)
                {
                    d++;
                }
                continue;
            }
            if(!(aitemstack[l].getItem() instanceof ItemArmorBackpack) && !(aitemstack[l].getItem() instanceof ItemArmorRubBoots) && aitemstack[l].getItem().getMaxDamage() > 0)
            {
                d += (((double)aitemstack[l].getMaxDamage() - (double)aitemstack[l].getItemDamage()) / (double)aitemstack[l].getMaxDamage()) * (double)itemarmor.damageReduceAmount;
                aitemstack[l].damageItem(k, null);
            }
        }

        d /= 25D;
        if(d > 1.0D)
        {
            d = 1.0D;
        }
        return new ArmorProperties((int)(d * (double)j), false);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < tier || itemstack.getItemDamage() == 1)
        {
            return 0;
        }
        int k = (itemstack.getItemDamage() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getItemDamage() - i / ratio);
        return i;
    }

    public int tier;
    public int ratio;
    public int transfer;
}
