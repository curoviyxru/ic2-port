// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.INetworkItemEventListener;
import ic2.common.TileEntityBlock;
import java.lang.reflect.Field;
import java.util.*;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;

public class NetworkManager
{
    static class TileEntityField
    {

        public boolean equals(Object obj)
        {
            if(obj instanceof TileEntityField)
            {
                TileEntityField tileentityfield = (TileEntityField)obj;
                return tileentityfield.te == te && tileentityfield.field.equals(field);
            } else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return te.hashCode() * 31 ^ field.hashCode();
        }

        TileEntity te;
        String field;
        EntityPlayerMP target;

        TileEntityField(TileEntity tileentity, String s)
        {
            target = null;
            te = tileentity;
            field = s;
        }

        TileEntityField(TileEntity tileentity, String s, EntityPlayerMP entityplayermp)
        {
            target = null;
            te = tileentity;
            field = s;
            target = entityplayermp;
        }
    }


    public NetworkManager()
    {
    }

    public static void onTick()
    {
        if(--ticksLeftToUpdate == 0)
        {
            if(!fieldsToUpdateSet.isEmpty())
            {
                sendUpdatePacket();
            }
            ticksLeftToUpdate = 2;
        }
    }

    public static void updateTileEntityField(TileEntity tileentity, String s)
    {
        fieldsToUpdateSet.add(new TileEntityField(tileentity, s));
        if(fieldsToUpdateSet.size() > 10000)
        {
            sendUpdatePacket();
        }
    }

    public static void initiateTileEntityEvent(TileEntity tileentity, int i, boolean flag)
    {
        int j = flag ? 400 : ModLoader.getMinecraftServerInstance().configManager.getMaxTrackingDistance();
        World world = tileentity.worldObj;
        Iterator iterator = world.playerEntities.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayerMP entityplayermp = (EntityPlayerMP)obj;
            int k = tileentity.xCoord - (int)entityplayermp.posX;
            int l = tileentity.zCoord - (int)entityplayermp.posZ;
            int i1;
            if(flag)
            {
                i1 = k * k + l * l;
            } else
            {
                i1 = Math.max(Math.abs(k), Math.abs(l));
            }
            if(i1 <= j)
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
                packet230modloader.isChunkDataPacket = true;
                packet230modloader.packetType = 1;
                packet230modloader.dataInt = new int[5];
                packet230modloader.dataInt[0] = world.worldProvider.worldType;
                packet230modloader.dataInt[1] = tileentity.xCoord;
                packet230modloader.dataInt[2] = tileentity.yCoord;
                packet230modloader.dataInt[3] = tileentity.zCoord;
                packet230modloader.dataInt[4] = i;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[0];
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void initiateItemEvent(EntityPlayer entityplayer, ItemStack itemstack, int i, boolean flag)
    {
        int j = flag ? 400 : ModLoader.getMinecraftServerInstance().configManager.getMaxTrackingDistance();
        Iterator iterator = entityplayer.worldObj.playerEntities.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayerMP entityplayermp = (EntityPlayerMP)obj;
            int k = (int)entityplayer.posX - (int)entityplayermp.posX;
            int l = (int)entityplayer.posZ - (int)entityplayermp.posZ;
            int i1;
            if(flag)
            {
                i1 = k * k + l * l;
            } else
            {
                i1 = Math.max(Math.abs(k), Math.abs(l));
            }
            if(i1 <= j)
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
                packet230modloader.isChunkDataPacket = true;
                packet230modloader.packetType = 2;
                packet230modloader.dataInt = new int[3];
                packet230modloader.dataInt[0] = itemstack.itemID;
                packet230modloader.dataInt[1] = itemstack.getItemDamage();
                packet230modloader.dataInt[2] = i;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[1];
                packet230modloader.dataString[0] = entityplayer.username;
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void announceBlockUpdate(World world, int i, int j, int k)
    {
        Iterator iterator = world.playerEntities.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayerMP entityplayermp = (EntityPlayerMP)obj;
            int l = Math.min(Math.abs(i - (int)entityplayermp.posX), Math.abs(k - (int)entityplayermp.posZ));
            if(l <= ModLoader.getMinecraftServerInstance().configManager.getMaxTrackingDistance())
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
                packet230modloader.isChunkDataPacket = true;
                packet230modloader.packetType = 3;
                packet230modloader.dataInt = new int[4];
                packet230modloader.dataInt[0] = world.worldProvider.worldType;
                packet230modloader.dataInt[1] = i;
                packet230modloader.dataInt[2] = j;
                packet230modloader.dataInt[3] = k;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[0];
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void requestInitialTileEntityData(World world, int i, int j, int k)
    {
    }

    public static void initiateClientItemEvent(ItemStack itemstack, int i)
    {
    }

    public static void handlePacket(Packet230ModLoader packet230modloader, EntityPlayerMP entityplayermp)
    {
        if(packet230modloader.packetType == 0 && packet230modloader.dataInt.length == 4)
        {
            net.minecraft.src.WorldServer aworldserver[] = ModLoader.getMinecraftServerInstance().worldMngr;
            int i = aworldserver.length;
            int j = 0;
            do
            {
                if(j >= i)
                {
                    break;
                }
                net.minecraft.src.WorldServer worldserver = aworldserver[j];
                if(packet230modloader.dataInt[0] == ((World) (worldserver)).worldProvider.worldType)
                {
                    TileEntity tileentity = worldserver.getBlockTileEntity(packet230modloader.dataInt[1], packet230modloader.dataInt[2], packet230modloader.dataInt[3]);
                    if(tileentity instanceof TileEntityBlock)
                    {
                        Iterator iterator = ((TileEntityBlock)tileentity).getNetworkedFields().iterator();
                        do
                        {
                            if(!iterator.hasNext())
                            {
                                break;
                            }
                            String s = (String)iterator.next();
                            fieldsToUpdateSet.add(new TileEntityField(tileentity, s, entityplayermp));
                            if(fieldsToUpdateSet.size() > 10000)
                            {
                                sendUpdatePacket();
                            }
                        } while(true);
                    }
                    break;
                }
                j++;
            } while(true);
        } else
        if(packet230modloader.packetType == 1 && packet230modloader.dataInt.length == 3)
        {
            Item item = Item.itemsList[packet230modloader.dataInt[0]];
            if(item instanceof INetworkItemEventListener)
            {
                ((INetworkItemEventListener)item).onNetworkEvent(packet230modloader.dataInt[1], entityplayermp, packet230modloader.dataInt[2]);
            }
        }
    }

    private static void sendUpdatePacket()
    {
        net.minecraft.src.WorldServer aworldserver[] = ModLoader.getMinecraftServerInstance().worldMngr;
        int i = aworldserver.length;
label0:
        for(int j = 0; j < i; j++)
        {
            net.minecraft.src.WorldServer worldserver = aworldserver[j];
            Iterator iterator = ((World) (worldserver)).playerEntities.iterator();
            do
            {
                if(!iterator.hasNext())
                {
                    continue label0;
                }
                Object obj = iterator.next();
                EntityPlayerMP entityplayermp = (EntityPlayerMP)obj;
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class).getId();
                packet230modloader.isChunkDataPacket = true;
                packet230modloader.packetType = 0;
                Vector vector = new Vector();
                Vector vector1 = new Vector();
                Vector vector2 = new Vector();
                vector1.add(Integer.valueOf(((World) (worldserver)).worldProvider.worldType));
                Iterator iterator1 = fieldsToUpdateSet.iterator();
                do
                {
                    if(!iterator1.hasNext())
                    {
                        break;
                    }
                    TileEntityField tileentityfield = (TileEntityField)iterator1.next();
                    if(!tileentityfield.te.isInvalid() && tileentityfield.te.worldObj == worldserver && (tileentityfield.target == null || tileentityfield.target == entityplayermp))
                    {
                        int l = Math.min(Math.abs(tileentityfield.te.xCoord - (int)entityplayermp.posX), Math.abs(tileentityfield.te.zCoord - (int)entityplayermp.posZ));
                        if(l <= ModLoader.getMinecraftServerInstance().configManager.getMaxTrackingDistance())
                        {
                            vector1.add(Integer.valueOf(tileentityfield.te.xCoord));
                            vector1.add(Integer.valueOf(tileentityfield.te.yCoord));
                            vector1.add(Integer.valueOf(tileentityfield.te.zCoord));
                            vector2.add(tileentityfield.field);
                            Field field = null;
                            try
                            {
                                Class class1 = tileentityfield.te.getClass();
                                do
                                {
                                    try
                                    {
                                        field = class1.getDeclaredField(tileentityfield.field);
                                    }
                                    catch(NoSuchFieldException nosuchfieldexception)
                                    {
                                        class1 = class1.getSuperclass();
                                    }
                                } while(field == null && class1 != null);
                                if(field == null)
                                {
                                    throw new NoSuchFieldException(tileentityfield.field);
                                }
                                field.setAccessible(true);
                                Class class2 = field.getType();
                                if(class2 == Float.TYPE)
                                {
                                    vector1.add(Integer.valueOf(0));
                                    vector.add(Float.valueOf(field.getFloat(tileentityfield.te)));
                                } else
                                if(class2 == Integer.TYPE)
                                {
                                    vector1.add(Integer.valueOf(1));
                                    vector1.add(Integer.valueOf(field.getInt(tileentityfield.te)));
                                } else
                                if(class2 == (java.lang.String.class))
                                {
                                    vector1.add(Integer.valueOf(2));
                                    vector2.add((String)field.get(tileentityfield.te));
                                } else
                                if(class2 == Boolean.TYPE)
                                {
                                    vector1.add(Integer.valueOf(3));
                                    vector1.add(Integer.valueOf(field.getBoolean(tileentityfield.te) ? 1 : 0));
                                } else
                                if(class2 == Byte.TYPE)
                                {
                                    vector1.add(Integer.valueOf(4));
                                    vector1.add(Integer.valueOf(field.getByte(tileentityfield.te)));
                                } else
                                if(class2 == Short.TYPE)
                                {
                                    vector1.add(Integer.valueOf(5));
                                    vector1.add(Integer.valueOf(field.getShort(tileentityfield.te)));
                                } else
                                {
                                    throw new RuntimeException((new StringBuilder()).append("Invalid field type: ").append(field.getType()).toString());
                                }
                            }
                            catch(Exception exception)
                            {
                                throw new RuntimeException(exception);
                            }
                        }
                    }
                } while(true);
                if(vector1.size() > 1)
                {
                    int ai[] = new int[vector1.size()];
                    int k = 0;
                    for(Iterator iterator2 = vector1.iterator(); iterator2.hasNext();)
                    {
                        Integer integer = (Integer)iterator2.next();
                        ai[k++] = integer.intValue();
                    }

                    float af[] = new float[vector.size()];
                    k = 0;
                    for(Iterator iterator3 = vector.iterator(); iterator3.hasNext();)
                    {
                        Float float1 = (Float)iterator3.next();
                        af[k++] = float1.floatValue();
                    }

                    packet230modloader.dataInt = ai;
                    packet230modloader.dataFloat = af;
                    packet230modloader.dataString = (String[])vector2.toArray(new String[0]);
                    ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.src.mod_IC2.class), entityplayermp, packet230modloader);
                }
            } while(true);
        }

        fieldsToUpdateSet.clear();
    }

    private static final int updatePeriod = 2;
    private static Set fieldsToUpdateSet = new HashSet();
    private static int ticksLeftToUpdate = 2;

}
