// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.IHasGuiContainer;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;

public class Platform
{

    public Platform()
    {
    }

    public static int AddAllFuel(int i, int j)
    {
        return ModLoader.AddAllFuel(i);
    }

    public static File getMinecraftDir()
    {
        return new File(".");
    }

    public static Chunk getOrLoadChunk(World world, int i, int j)
    {
        boolean flag = world.worldChunkLoadOverride;
        world.worldChunkLoadOverride = true;
        Chunk chunk = world.getChunkProvider().provideChunk(i, j);
        world.worldChunkLoadOverride = flag;
        return chunk;
    }

    public static EntityPlayer getPlayerInstance()
    {
        return null;
    }

    public static boolean isRendering()
    {
        return false;
    }

    public static boolean isSimulating()
    {
        return true;
    }

    public static boolean launchGUI(EntityPlayer entityplayer, TileEntity tileentity, Integer integer)
    {
        if(integer.intValue() == 0 && (tileentity instanceof IInventory))
        {
            entityplayer.displayGUIChest((IInventory)tileentity);
            return true;
        }
        if((tileentity instanceof IInventory) && (tileentity instanceof IHasGuiContainer))
        {
            ModLoader.OpenGUI(entityplayer, integer.intValue(), (IInventory)tileentity, ((IHasGuiContainer)tileentity).getGuiContainer(entityplayer.inventory));
            return true;
        } else
        {
            return false;
        }
    }

    public static void log(Level level, String s)
    {
        NetServerHandler.logger.log(level, s);
    }

    public static boolean isBlockOpaqueCube(IBlockAccess iblockaccess, int i, int j, int k)
    {
        return false;
    }

    public static void playSoundSp(String s, float f, float f1)
    {
    }

    public static void resetPlayerInAirTime(EntityPlayer entityplayer)
    {
        if(!(entityplayer instanceof EntityPlayerMP))
        {
            return;
        }
        NetServerHandler netserverhandler = ((EntityPlayerMP)entityplayer).playerNetServerHandler;
        try
        {
            ModLoader.setPrivateValue(net.minecraft.src.NetServerHandler.class, netserverhandler, "g", new Integer(0));
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }

    public static void messagePlayer(EntityPlayer entityplayer, String s)
    {
        if(!(entityplayer instanceof EntityPlayerMP))
        {
            return;
        } else
        {
            ((EntityPlayerMP)entityplayer).playerNetServerHandler.sendPacket(new Packet3Chat(s));
            return;
        }
    }

    public static boolean isKeyDownLaserMode(EntityPlayer entityplayer)
    {
        return false;
    }

    public static boolean isKeyDownJetpackHover(EntityPlayer entityplayer)
    {
        return false;
    }

    public static boolean isKeyDownSuitActivate(EntityPlayer entityplayer)
    {
        return false;
    }

    public static boolean isKeyDownForward(EntityPlayer entityplayer)
    {
        return false;
    }

    public static String getItemNameIS(ItemStack itemstack)
    {
        return null;
    }

    public static void teleportTo(Entity entity, double d, double d1, double d2, float f, 
            float f1)
    {
        if(entity instanceof EntityPlayerMP)
        {
            EntityPlayerMP entityplayermp = (EntityPlayerMP)entity;
            entityplayermp.playerNetServerHandler.teleportTo(d, d1, d2, f, f1);
            int i = (entityplayermp.mcServer.configManager.getMaxTrackingDistance() + 16) / 16;
            int j = (int)entityplayermp.posX >> 4;
            int k = (int)entityplayermp.posZ >> 4;
            int l = (int)entityplayermp.field_9155_d >> 4;
            int i1 = (int)entityplayermp.field_9154_e >> 4;
            if(!isChunkPairWithinDistance(j, k, l, i1, i))
            {
                ic2_ServerTeleportHelper.playerInstanceAddPlayer(entityplayermp, j, k);
            }
            int j1 = j;
            int k1 = k;
            int l1 = 1;
            int i2 = 1;
            label0:
            do
            {
                for(int k2 = 0; k2 < i2; k2++)
                {
                    k1 += l1;
                    if(!isChunkPairWithinDistance(j1, k1, l, i1, i))
                    {
                        ic2_ServerTeleportHelper.playerInstanceAddPlayer(entityplayermp, j1, k1);
                    }
                    if(k1 - k == i)
                    {
                        break label0;
                    }
                }

                for(int l2 = 0; l2 < i2; l2++)
                {
                    j1 += l1;
                    if(!isChunkPairWithinDistance(j1, k1, l, i1, i))
                    {
                        ic2_ServerTeleportHelper.playerInstanceAddPlayer(entityplayermp, j1, k1);
                    }
                }

                l1 = -l1;
                i2++;
            } while(true);
            for(int j2 = l - i; j2 <= l + i; j2++)
            {
                for(int i3 = i1 - i; i3 <= i1 + i; i3++)
                {
                    if(!isChunkPairWithinDistance(j2, i3, j, k, i))
                    {
                        ic2_ServerTeleportHelper.playerInstanceRemovePlayer(entityplayermp, j2, i3);
                    }
                }

            }

            entityplayermp.field_9155_d = entityplayermp.posX;
            entityplayermp.field_9154_e = entityplayermp.posZ;
        } else
        {
            entity.setPositionAndRotation(d, d1, d2, f, f1);
        }
    }

    public static String translateBlockName(Block block)
    {
        return null;
    }

    public static double worldUntranslatedFunction2(World world, Vec3D vec3d, AxisAlignedBB axisalignedbb)
    {
        return (double)world.func_494_a(vec3d, axisalignedbb);
    }

    public static MovingObjectPosition axisalignedbbUntranslatedFunction1(AxisAlignedBB axisalignedbb, Vec3D vec3d, Vec3D vec3d1)
    {
        return axisalignedbb.func_706_a(vec3d, vec3d1);
    }

    public static List getContainerSlots(Container container)
    {
        return container.inventorySlots;
    }

    public static boolean isPlayerOp(EntityPlayer entityplayer)
    {
        return ModLoader.getMinecraftServerInstance().configManager.isOp(entityplayer.username);
    }

    //public static boolean isPlayerSprinting(EntityPlayer entityplayer)
    //{
    //    return entityplayer.func_35149_at();
    //}

    public static boolean givePlayerOneFood(EntityPlayer entityplayer)
    {
        /* if(entityplayer.func_35207_V().func_35585_a() < 18)
        {
            entityplayer.func_35207_V().func_35590_a(1, 0.9F);
            return true;
        } else
        {
            return false;
        } */
        if (entityplayer.health <= 18) {
            entityplayer.heal(2);
            return true;
        }
        else return false;
    }

    public static void removePoisonFrom(EntityPlayer entityplayer)
    {
    }

    private static boolean isChunkPairWithinDistance(int i, int j, int k, int l, int i1)
    {
        return Math.abs(i - k) <= i1 && Math.abs(j - l) <= i1;
    }
}
